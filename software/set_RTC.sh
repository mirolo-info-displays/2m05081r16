#! /bin/bash
 
# RTC setup script for mirolo displays

if [ $# != 2 ]; then
    echo "Usage: set_RTC.sh [DEVICE] [UTC+HOURS] | Example: set_RTC.sh /dev/ttyUSB0 2"
    exit 1
fi

#device setup
echo "Setting up Serial Baud Rate"
stty -F $1 speed 115200 cs8 -cstopb -parenb raw 115200
sleep 1

#read from device
echo "Connecting to Serial"
cat < $1 &
PIDCAT=$!
sleep 4

#reset device
echo "Rebooting Device for RTC Maintenance"
echo "system:setclock" > $1
sleep 4

#set clock
echo "Setting Device RTC"
date "+%s" -d "+ $2 hour" > $1
#date "+%s" -d "07/27/2019 13:30:00 + $2 hour" > $1
sleep 3

#print date and exit
kill $PIDCAT
date
echo "OK"

