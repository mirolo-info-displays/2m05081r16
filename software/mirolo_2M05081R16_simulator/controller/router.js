//Imports
const express = require("express");
const router = express.Router();

//Imports
const miroloController = require("./mirolo");

//Routes
router.use("/api", miroloController.router);

//Exports
module.exports = {
    router
}