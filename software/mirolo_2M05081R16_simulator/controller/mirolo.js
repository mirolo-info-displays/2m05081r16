//Packages
const express = require("express");
const router = express.Router({mergeParams: true});
const randomInt = require('random-int');
const ip = require("ip");
const mac = require('getmac');


//Imports
const mirolo = require("../model/mirolo");

//The display Instance
let display;
mac.getMac(function (err, macAddress) {
    if (err) macAddress = "20-dc-93-aa-bb-cc";
    display = new mirolo.MiroloDisplay(ip.address(), macAddress);
});


//Routes
router.get("/line0", getLine0);
router.patch("/line0", patchLine0);
router.post("/line0", postLine0);

router.get("/line1", getLine1);
router.patch("/line1", patchLine1);
router.post("/line1", postLine1);

router.get("/ring0", getRing0);
router.post("/ring0", postRing0);

router.get("/brightness", getBrightness);
router.patch("/brightness", patchBrightness);
router.post("/brightness", postBrightness);

router.get("/info", getInfo);

router.get("/ldr", getLdr);

router.get("/name", getName);
router.post("/name", postName);

router.get("/apply", getApply);
router.post("/apply", postApply);

router.get("/power", getPower);
router.post("/power", postPower);

router.post("/revert", postRevert);


//Line0
function getLine0(req, res) {

    res.status(200).send(responseLine0());
}

function patchLine0(req, res) {

    let changed = false;
    if (req.body.hasOwnProperty("message")) {
        display.pending.message0 = req.body.message;
        changed = true;
    }
    if (req.body.hasOwnProperty("prefix")) {
        display.pending.prefix0 = req.body.prefix;
        changed = true;
    }
    if (req.body.hasOwnProperty("speed")) {
        display.pending.scrolldelay0 = req.body.speed;
        changed = true;
    }

    if (changed === true) res.status(200).send(responseLine0());
    else {
        res.status(400).send({
            status: 400,
            data: {}
        });
    }
}

function postLine0(req, res) {

    let changed = false;
    if (req.body.hasOwnProperty("message")) {
        display.pending.message0 = req.body.message;
        display.active.message0 = req.body.message;
        changed = true;
    }
    if (req.body.hasOwnProperty("prefix")) {
        display.pending.prefix0 = req.body.prefix;
        display.active.prefix0 = req.body.prefix;
        changed = true;
    }
    if (req.body.hasOwnProperty("speed")) {
        display.pending.scrolldelay0 = req.body.speed;
        display.active.scrolldelay0 = req.body.speed;
        changed = true;
    }

    if (changed === true) res.status(200).send(responseLine0());
    else {
        res.status(400).send({
            status: 400,
            data: {}
        });
    }

    display.printConsole();
}

function responseLine0() {
    return {
        status: 200,
        data: {
            pending: {
                message: display.pending.message0,
                messageok: display.isPrintableASCII(display.pending.message0),
                prefix: display.pending.prefix0,
                prefixok: display.isPrefixOk(5, display.pending.prefix0, display.pending.message0),
                speed: display.pending.scrolldelay0,
                scrolling: display.isScrolling(5, display.pending.prefix0, display.pending.message0),
                roundtriptime: display.isScrolling(5, display.pending.prefix0, display.pending.message0) ? (display.pending.message0.length + 5) * 4 * display.pending.scrolldelay0 : 0 //medium character width is 4 dots
            },
            active: {
                message: display.active.message0,
                messageok: display.isPrintableASCII(display.active.message0),
                prefix: display.active.prefix0,
                prefixok: display.isPrefixOk(5, display.active.prefix0, display.active.message0),
                speed: display.active.scrolldelay0,
                scrolling: display.isScrollingActive0(),
                roundtriptime: display.isScrolling(5, display.active.prefix0, display.active.message0) ? (display.active.message0.length + 5) * 4 * display.active.scrolldelay0 : 0 //medium character width is 4 dots

            }
        }
    }
}


//Line1
function getLine1(req, res) {

    res.status(200).send(responseLine1());
}

function patchLine1(req, res) {

    let changed = false;
    if (req.body.hasOwnProperty("message")) {
        display.pending.message1 = req.body.message;
        changed = true;
    }
    if (req.body.hasOwnProperty("prefix")) {
        display.pending.prefix1 = req.body.prefix;
        changed = true;
    }
    if (req.body.hasOwnProperty("speed")) {
        display.pending.scrolldelay1 = req.body.speed;
        changed = true;
    }

    if (changed === true) res.status(200).send(responseLine1());
    else {
        res.status(400).send({
            status: 400,
            data: {}
        });
    }
}

function postLine1(req, res) {

    let changed = false;
    if (req.body.hasOwnProperty("message")) {
        display.pending.message1 = req.body.message;
        display.active.message1 = req.body.message;
        changed = true;
    }
    if (req.body.hasOwnProperty("prefix")) {
        display.pending.prefix1 = req.body.prefix;
        display.active.prefix1 = req.body.prefix;
        changed = true;
    }
    if (req.body.hasOwnProperty("speed")) {
        display.pending.scrolldelay1 = req.body.speed;
        display.active.scrolldelay1 = req.body.speed;
        changed = true;
    }

    if (changed === true) res.status(200).send(responseLine1());
    else {
        res.status(400).send({
            status: 400,
            data: {}
        });
    }

    display.printConsole();
}

function responseLine1() {
    return {
        status: 200,
        data: {
            pending: {
                message: display.pending.message1,
                messageok: display.isPrintableASCII(display.pending.message1),
                prefix: display.pending.prefix1,
                prefixok: display.isPrefixOk(5, display.pending.prefix1, display.pending.message1),
                speed: display.pending.scrolldelay1,
                scrolling: display.isScrolling(8, display.pending.prefix1, display.pending.message1),
                roundtriptime: display.isScrolling(8, display.pending.prefix1, display.pending.message1) ? (display.pending.message1.length + 8) * 4 * display.pending.scrolldelay1 : 0 //medium character width is 4 dots
            },
            active: {
                message: display.active.message1,
                messageok: display.isPrintableASCII(display.active.message1),
                prefix: display.active.prefix1,
                prefixok: display.isPrefixOk(5, display.active.prefix1, display.active.message1),
                speed: display.active.scrolldelay1,
                scrolling: display.isScrollingActive1(),
                roundtriptime: display.isScrolling(8, display.active.prefix1, display.active.message1) ? (display.active.message1.length + 8) * 4 * display.active.scrolldelay1 : 0 //medium character width is 4 dots

            }
        }
    }
}


//Ring0
function getRing0(req, res) {

    res.status(200).send(responseRing0());
}

function postRing0(req, res) {

    let changed = false;
    if (req.body.hasOwnProperty("loop")) {
        display.loop = req.body.loop;
        changed = true;
    }
    if (req.body.hasOwnProperty("frames")) {
        display.animation = [];
        let frameCount = 0;
        for (frame in req.body.frames) {
            //create frames
            newFrame = new mirolo.AnimationFrame();

            if (frame.hasOwnProperty("colors") && frame.hasOwnProperty("time") && frame.time > 20 && frame.time <= 255) {
                for (color in colors) {
                    if (color.get(0) === '#' && color.length === 7) newFrame.colors.push(color);
                }

                //fill with #000000 Black
                for (let i = newFrame.colors.length; i < 16; ++i) {
                    newFrame.colors.push("#000000");
                }

                //set frame display time
                newFrame.time = frame.time;

                if (++frameCount >= 20) break;
            }


            //add frame to animation sequence
            display.animation.push(newFrame);
        }
        changed = true;
    }

    if (changed === true) res.status(200).send(responseRing0());
    else {
        res.status(400).send({
            status: 400,
            data: {}
        });
    }

    display.printConsole();
}

function responseRing0() {
    return {
        status: 200,
        data: {
            framelength: display.animation.length,
            loop: display.loop
        }
    }
}

//Brightness
function getBrightness(req, res) {

    res.status(200).send(responseBrightness());
}

function patchBrightness(req, res) {

    let changed = false;
    if (req.body.hasOwnProperty("brightness") && req.body.brightness <= 16) {
        display.pending.brightness = req.body.brightness;
        changed = true;
    }

    if (changed === true) res.status(200).send(responseBrightness());
    else {
        res.status(400).send({
            status: 400,
            data: {}
        });
    }
}

function postBrightness(req, res) {

    let changed = false;
    if (req.body.hasOwnProperty("brightness") && req.body.brightness <= 16) {
        display.pending.brightness = req.body.brightness;
        display.active.brightness = req.body.brightness;
        changed = true;
    }

    if (changed === true) res.status(200).send(responseBrightness());
    else {
        res.status(400).send({
            status: 400,
            data: {}
        });
    }

    display.printConsole();
}

function responseBrightness() {
    return {
        status: 200,
        data: {
            pending: {
                brightness: display.pending.brightness
            },
            active: {
                brightness: display.active.brightness
            }
        }
    }
}


//Info
function getInfo(req, res) {

    res.status(200).send({
        status: 200,
        data: {
            hardware: display.HW,
            processor: display.CPU,
            serialnumber: display.SNR,
            firmware: display.FW,
            ip: display.IP,
            mac: display.MAC
        }
    });
}


//LDR
function getLdr(req, res) {

    res.status(200).send({
        status: 200,
        data: {
            ldr: randomInt(1023),
        }
    });
}


//Name
function getName(req, res) {

    res.status(200).send(responseName());
}

function postName(req, res) {

    let changed = false;
    if (req.body.hasOwnProperty("name") && req.body.name.length > 0) {
        display.Name = req.body.name;
        changed = true;
    }

    if (changed === true) res.status(200).send(responseName());
    else {
        res.status(400).send({
            status: 400,
            data: {}
        });
    }

    display.printConsole();
}

function responseName() {
    return {
        status: 200,
        data: {
            name: display.Name
        }
    }
}


//Apply
function getApply(req, res) {

    res.status(200).send({
        status: 200,
        data: {
            apply: display.active === display.pending
        }
    });
}

function postApply(req, res) {

    Object.assign(display.active, display.pending);

    res.status(200).send({
        status: 200,
        data: {
            apply: true
        }
    });

    display.printConsole();
}


//Power
function getPower(req, res) {

    res.status(200).send({
        status: 200,
        data: {
            enable: display.pwr
        }
    });
}

function postPower(req, res) {

    if (req.body.hasOwnProperty("enable")) {
        display.pwr = req.body.enable;
        changed = true;
    }

    if (changed === true) res.status(200).send({
        status: 200,
        data: {
            enable: display.pwr
        }
    });
    else {
        res.status(400).send({
            status: 400,
            data: {}
        });
    }

    display.printConsole();
}


//Revert
function postRevert(req, res) {

    Object.assign(display.pending, display.active);

    res.status(200).send({
        status: 200,
        data: {
            revert: true
        }
    });
}


//Routes
module.exports = {
    router
}