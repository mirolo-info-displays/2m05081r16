

class MiroloDisplay {


    constructor(ip, mac) {

        this.count = 1;

        this.active = new DisplayState();
        this.pending = new DisplayState();

        this.animation = [];

        this.loop = true;
        this.pwr = true;

        this.Name = "Mirolo Simulator";
        this.HW = "mirolo 2M05081R16";
        this.CPU = "Atmega1284p@20MHz";
        //create pseudo unique number from mac
        this.SNR = "2017.10." + (9000 + parseInt("0x" + mac.substring(15, 17)));
        this.FW = "Jun  3 1970 | 19:20:21";
        this.MAC = mac;
        this.IP = ip;

    }

    getCurrentTime(){
        const date = new Date();
        let result =  date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
        result += ":";
        result += date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        result += ":";
        result += date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
        return result;
    }

    printConsole(){
        console.log("\nDisplay @" + this.IP + " powered " + (this.pwr ? "ON" : "OFF") + ": \"" + this.Name + "\" Currently Showing Screen " + this.count++ + " (" + this.getCurrentTime() + "):");
        console.log("Line 0 (Prefix: " + this.active.prefix0 + " ,Speed: " + this.active.scrolldelay0 + "ms" + (this.isScrollingActive0() ? " ,Scrolling" : " ,Static") + "): \"" + this.active.message0 + "\"");
        console.log("Line 1 (Prefix: " + this.active.prefix1 + " ,Speed: " + this.active.scrolldelay0 + "ms" + (this.isScrollingActive1() ? " ,Scrolling" : " ,Static") + "): \"" + this.active.message1 + "\"");
        console.log("Brightness: " + (this.active.brightness < 16 ? this.active.brightness : "auto") + "  |  Animation: " + this.animation.length + " active Frames " + (this.loop ? "(loop)" : "(single run)") + "\n");
    }

    isScrolling(linesize, prefix, message) {
        if (message.length > linesize || (prefix > 0 && message.length > 0 && prefix < message.length)) return true;
        else return false;
    }

    isPrefixOk(linesize, prefix, message) {
        if(prefix <= (linesize * 2 / 3) && prefix < message.length) return true;
        else return false;
    }

    isScrollingActive0(){
        return this.isScrolling(5, this.active.prefix0, this.active.message0);
    }

    isScrollingActive1(){
        return this.isScrolling(8, this.active.prefix1, this.active.message1);
    }


    //check for printable ASCII. See https://stackoverflow.com/questions/14313183/javascript-regex-how-do-i-check-if-the-string-is-ascii-only
    isPrintableASCII(string){
        return /^[\x20-\x7F]*$/.test(string);
    }
}

class DisplayState {

    constructor() {

        this.message0 = "";   //line0
        this.prefix0 = 0;
        this.scrolldelay0 = 75;

        this.message1 = "";   //line1
        this.prefix1 = 0;
        this.scrolldelay1 = 75;

        this.brightness = 5;
    }
}

class AnimationFrame {

    constructor() {

        this.colors = ["#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000"];
        this.time = 500;
    }
}


module.exports = {
    MiroloDisplay : MiroloDisplay,
    AnimationFrame : AnimationFrame
}