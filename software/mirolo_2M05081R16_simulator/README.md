# mirolo - 2M05081R16 API Simulator
Simple backend for simulating a single mirolo 2M05081R16 display without the need for dedicated hardware.
This might be useful for developing and testing applications that access the display's REST JSON API.

## Setup

### Software
* [node.js](https://nodejs.org/en/)
* [npm](https://www.npmjs.com/)

### Installation
To install any dependencies simply execute the following command from the directory where **package.json** is located.

	npm install

After that you can start the server with the following command. You'll need root access to start the server on port 80.

	sudo npm start

The API is accessible at http://localhost:80/api/ or http://[localIP]:80/api/



## Usage
For information on the API itself please have a look at [/Documentation/REST API](../Documentation/REST API)
and the provided postman collection [www.getpostman.com](https://www.getpostman.com/).

## Change log
**Version 1.0 (05-05-2019)**
* first public version
