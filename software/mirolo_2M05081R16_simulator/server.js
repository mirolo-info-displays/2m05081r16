//Packages
const express = require("express");
const bodyParser = require("body-parser");
const router = require("./controller/router");
const ip = require("ip");


//Constants

//Initialize Express
const app = express();


var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE, PATCH');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
}

//Middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.text());
app.use(allowCrossDomain);

//Routes
app.use("/", router.router);

//Start Server
let server;
try {
    console.log("Server listening on " + ip.address() + ":80");
    app.listen(80);
}
catch (e) {
    console.log(e);
    console.log("Failed to start server. Are you root?");
}




