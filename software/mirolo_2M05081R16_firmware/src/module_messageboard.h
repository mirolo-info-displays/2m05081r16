//@(#) module_messageboard.h

/* Module for displaying preconfigured Text Messages that can be selected using the IR Remote Control. This is intended as
  a general manually operated display to show messages.
  A DS1307 Module has to be connected to the I²C Bus for time management.

  Edit the "screens" struct below to your needs and have a look at the remote control
  documentation for information on button assignment and codes.
*/

#ifndef MODULE_MESSAGEBOARD_H
#define MODULE_MESSAGEBOARD_H

// INFO: set system time via serial command: 
//   stty -F /dev/ttyUSB0 115200
//   echo "system:setclock" > /dev/ttyUSB0 && sleep 5 && date '+%s' -d '+ 2 hour' > /dev/ttyUSB0
// DTR (auto reset) has to be disconnected!

#include <time.h>
#include <Wire.h>
#include <DS1307RTC.h>
#include "remote.h"

//function declaration
void updateEEPROM();

const uint16_t max_msg_length = 50;
const uint32_t colors[] = {RED, GREEN, BLUE, WHITE, CYAN, MAGENTA, DARKORANGE};

struct st_screen {
  char Message0[max_msg_length];  //white
  uint8_t Prefix0;
  char Message1[max_msg_length];  //red
  uint8_t Prefix1;
  uint32_t Color;     //0x000000 - 0xffffff. use 0x100001 to set to global default color
  char Animation[20]; // on, off, blink (1Hz), loading, pulse. use "" to set to global default animation
};

#if EVENT == ef
struct st_screen screens[] = {
  //basic
  {"hh:mm:ss", 0, "EF27 Black Magic", 5, 0x100001, ""},
  {"Laden...", 0, "", 0, 0xffffff, "loading"},
  {"hh:mm:ss", 0, "Counter OPEN", 7, GREEN, ""},
  {"hh:mm:ss", 0, "Counter CLOSED", 7, RED, ""},

  //art show specific
  {"hh:mm:ss", 0, "Art Show", 0, 0x100001, ""},
  {"hh:mm:ss", 0, "Charity Check-In", 0, 0x100001, ""},
  {"hh:mm:ss", 0, "Art Show Check-In", 0, 0x100001, ""},
  {"hh:mm:ss", 0, "Art Show - Bidder Registration", 0, 0x100001, ""},
  {"hh:mm:ss", 0, "Art Show - Bidder Registration (Closes 18:00)", 0, 0x100001, ""},
  {"hh:mm:ss", 0, "Art Sale Pick-Up", 0, 0x100001, ""},
  {"hh:mm:ss", 0, "Unsold Art Check-Out", 0, 0x100001, ""},
  {"Art Show", 0, "hh:mm:ss", 0, 0x100001, ""}
};
#else
struct st_screen screens[] = {
  //basic
  {"hh:mm:ss", 0, "Test Message", 5, 0x100001, ""},
  {"Test", 0, "", 0, 0xffffff, "loading"},
  {"hh:mm:ss", 0, "Counter OPEN", 7, GREEN, ""},
  {"hh:mm:ss", 0, "Counter CLOSED", 7, RED, ""}
};
#endif


class schedule
{

  private:
    enum mode : uint8_t {manual = 0, external, MODEEND};

    uint8_t currentScreenIndex = 0;
    uint8_t lastScreenIndex = 0;
    uint8_t nextScreenIndex = 0;
    uint32_t eventTimer = 0;
    uint16_t eventDelay = 0;

    uint32_t defaultColor = 0;
    char defaultAnimation[20] = {'o', 'f', 'f', '\0'};

    mode Mode = manual;

    bool clearScreen = false;
    bool forceUpdateDisplay = false;


    void setLine0(const char * message, const uint8_t &prefix, const uint8_t &scrolldelay) {
      memset(mirolo.DSP.message0, 0, max_bufsize);
      strcpy(mirolo.DSP.message0, message);
      if (prefix == 0)strcpy_P(mirolo.DSP.prefix0, PSTR("off"));
      else ltoa(prefix, mirolo.DSP.prefix0, 10);
      mirolo.DSP.scrolldelay0 = scrolldelay;
      mirolo.DSP.apply_changes |= LINE0;
    }

    void setLine1(const char * message, const uint8_t &prefix, const uint8_t &scrolldelay) {
      memset(mirolo.DSP.message1, 0, max_bufsize);
      strcpy(mirolo.DSP.message1, message);
      if (prefix == 0)strcpy_P(mirolo.DSP.prefix1, PSTR("off"));
      else ltoa(prefix, mirolo.DSP.prefix1, 10);
      mirolo.DSP.scrolldelay1 = scrolldelay;
      mirolo.DSP.apply_changes |= LINE1;
    }

    char * buildClock(char * clockstring, const uint8_t &h, const uint8_t &m, const uint8_t &s = 60) { //build clock in hh:mm:ss or hh:mm format
      char * dest = clockstring;
      char tmp[3] = {'\0'};
      ltoa(h, tmp, 10);
      if (h < 10) {
        memcpy_P(dest++, PSTR("0"), 1);
        memcpy(dest++, tmp, 1);
      }
      else {
        memcpy(dest, tmp, 2);
        dest += 2;
      }

      memcpy_P(dest++, PSTR(":"), 1);
      ltoa(m, tmp, 10);
      if (m < 10) {
        memcpy_P(dest++, PSTR("0"), 1);
        memcpy(dest++, tmp, 1);
      }
      else {
        memcpy(dest, tmp, 2);
        dest += 2;
      }

      if (s < 60) {
        memcpy_P(dest++, PSTR(":"), 1);
        ltoa(s, tmp, 10);
        if (s < 10) {
          memcpy_P(dest++, PSTR("0"), 1);
          memcpy(dest++, tmp, 1);
        }
        else {
          memcpy(dest, tmp, 2);
          dest += 2;
        }
      }

      return dest;
    }


    void setRingColor(const uint32_t & color) {
      mirolo.DSP.color = color;
      mirolo.DSP.apply_changes |= ANIM;
    }

    void setRingAnimation_P(const char * animation) {
      strcpy_P(mirolo.DSP.animation, animation);
      mirolo.DSP.apply_changes |= ANIM;
    }

    void setRingAnimation(const char * animation) {
      strcpy(mirolo.DSP.animation, animation);
      mirolo.DSP.apply_changes |= ANIM;
    }

    void cycleAB() {
      nextScreenIndex = lastScreenIndex;
      eventDelay = 0;
    }

    void cycleAnimationType() {
      static uint8_t index = 0;
      switch (index++) {
        case 0:
          setRingAnimation_P(PSTR("on"));
          strcpy_P(defaultAnimation, PSTR("on"));
          break;
        case 1:
          setRingAnimation_P(PSTR("blink (1Hz)"));
          strcpy_P(defaultAnimation, PSTR("blink (1Hz)"));
          break;
        case 2:
          setRingAnimation_P(PSTR("pulse"));
          strcpy_P(defaultAnimation, PSTR("pulse"));
          break;
        case 3:
          setRingAnimation_P(PSTR("loading"));
          strcpy_P(defaultAnimation, PSTR("loading"));
          break;
        case 4:
          setRingAnimation_P(PSTR("off"));
          strcpy_P(defaultAnimation, PSTR("off"));
          index = 0;
          break;
      }
    }

    void cycleMode() {
      forceUpdateDisplay = true;
      clearScreen = true;
      Mode = mode(Mode + 1);
      if (Mode == MODEEND) Mode = (mode)0;

      char msg0[10] = {'\0'};
      strcpy_P(msg0, PSTR("Mode"));
      setLine0(msg0, 0, mirolo.DSP.scrolldelay0);
      char msg1[40] = {'\0'};
      if (Mode == manual) {
        strcpy_P(msg1, PSTR("manual"));
      }
      else if (Mode == external) {
        strcpy_P(msg1, PSTR("external"));
      }
      setLine1(msg1, 0, mirolo.DSP.scrolldelay1);

      eventTimer = millis();
      eventDelay = 2000;
    }

    void cycleColor() {
      static uint8_t index = 0;

      setRingColor(colors[index]);
      defaultColor = colors[index];
      index++;
      if (index >= sizeof(colors) / sizeof(uint32_t)) index = 0;
    }

    void adjustBrightness(int8_t brightness) {
      forceUpdateDisplay = true;
      clearScreen = true;
      if (brightness > 16) brightness = 16;
      if (brightness < 0) brightness = 0;

      mirolo.DSP.brightness = brightness;
      mirolo.DSP.apply_changes |= BRIGHT;

      char msg0[10] = {'\0'};
      strcpy_P(msg0, PSTR("Bright"));
      setLine0(msg0, 0, mirolo.DSP.scrolldelay0);
      char msg1[40] = {'\0'};
      if (mirolo.DSP.brightness < 16) {
        ltoa(10 + (mirolo.DSP.brightness) * (96 / 16), msg1, 10);
        strcat_P(msg1, PSTR("%"));
      }
      else {
        strcat_P(msg1, PSTR("auto (LDR)"));
      }
      setLine1(msg1, 0, mirolo.DSP.scrolldelay1);

      eventTimer = millis();
      eventDelay = 2000;
    }

    void adjustScrollDelay(uint8_t scrolldelay) {
      forceUpdateDisplay = true;
      clearScreen = true;
      if (scrolldelay > 100) scrolldelay = 100;
      if (scrolldelay >= 20 && scrolldelay <= 100) {
        mirolo.DSP.scrolldelay0 = scrolldelay;
        mirolo.DSP.scrolldelay1 = scrolldelay;
        mirolo.DSP.apply_changes |= (LINE0 | LINE1);
      }

      char msg0[10] = {'\0'};
      strcpy_P(msg0, PSTR("Scroll"));
      setLine0(msg0, 0, mirolo.DSP.scrolldelay0);
      char msg1[40] = {'\0'};
      ltoa(mirolo.DSP.scrolldelay0, msg1, 10);
      strcat_P(msg1, PSTR("ms"));
      setLine1(msg1, 0, mirolo.DSP.scrolldelay1);

      eventTimer = millis();
      eventDelay = 2000;
    }

    uint16_t processMessage(char * message, uint8_t &prefix, MD_MAX72XX &mx) { //replaces time strings with clock data.returns update interval in ms, 0 if static
      uint16_t updateInterval = 0;

      char * clkStart = strstr_P(message, PSTR("hh:mm:ss")); //full time format hh:mm:ss
      if (clkStart != NULL) {
        strcpy(message, clkStart);
        clkStart = message;
        char * clkEnd = buildClock(clkStart, hour(), minute(), second());
        clkEnd = NULL;
        prefix = 0;
        updateInterval = 1000;
      }
      else {
        clkStart = strstr_P(message, PSTR("hh:mm")); // time format hh:mm
        if (clkStart != NULL) {
          strcpy(message, clkStart);
          clkStart = message;
          char * clkEnd = buildClock(clkStart, hour(), minute());
          if (strlen(clkEnd) > 0) {
            if (staticDivisionOK(message, 5, mx)) {
              prefix = 5;
            }
            else {
              clkEnd = NULL;
              prefix = 0;
            }
          }
          updateInterval = 60000;
        }

      }

      return updateInterval;
    }

    uint16_t showScreen(const uint8_t &index) {
      uint16_t delay0 = 0;
      uint16_t delay1 = 0;
      if (index < (sizeof(screens) / sizeof(st_screen)) ) {
        char msg0[max_msg_length] = {'\0'};
        char msg1[max_msg_length] = {'\0'};
        strcpy(msg0, screens[index].Message0);
        strcpy(msg1, screens[index].Message1);
        delay0 = processMessage(msg0, screens[index].Prefix0, max_Line[0].mx);
        delay1 = processMessage(msg1, screens[index].Prefix1, max_Line[1].mx);
        setLine0(msg0, screens[index].Prefix0, mirolo.DSP.scrolldelay0);
        setLine1(msg1, screens[index].Prefix1, mirolo.DSP.scrolldelay1);

        if (screens[index].Color == 0x100001) {
          setRingColor(defaultColor);
        }
        else {
          setRingColor(screens[index].Color);
        }
        if (strcmp_P(screens[index].Animation, PSTR("")) == 0) {
          setRingAnimation(defaultAnimation);
        }
        else {
          setRingAnimation(screens[index].Animation);

        }
      }

      if (delay0 == 0 && delay1 == 0) return 0;
      else if (delay0 == 0) return delay1;
      else if (delay1 == 0) return delay0;
      else if (delay1 >= delay0) return delay0;
      else if (delay0 >= delay1) return delay1;
    }


    void selectScreen(uint8_t index) {
      forceUpdateDisplay = true;
      clearScreen = true;

      uint8_t numberOfScreens = sizeof(screens) / sizeof(st_screen);

      if (index < numberOfScreens && index >= 0) {
        nextScreenIndex = index;
      }


      char msg0[10] = {'\0'};
      strcpy_P(msg0, PSTR("Screen"));
      setLine0(msg0, 0, mirolo.DSP.scrolldelay0);
      char msg1[40] = {'\0'};
      ltoa(nextScreenIndex + 1, msg1, 10);
      strcat_P(msg1, PSTR("/"));
      char numScreens[10] = {'\0'};
      ltoa(sizeof(screens) / sizeof(st_screen), numScreens, 10);
      strcat(msg1, numScreens);
      setLine1(msg1, 0, mirolo.DSP.scrolldelay1);

      eventTimer = millis();
      eventDelay = 3000;
    }

    void selectScreenKeypad(const int8_t &value) {
      static uint16_t index = 0;
      if (forceUpdateDisplay == false) {
        index = 0;
      }
      if (value == -1) {
        index = 0;
        selectScreen(currentScreenIndex);
      }
      else if (index < 255) {
        index *= 10;
        uint8_t numberOfScreens = sizeof(screens) / sizeof(st_screen);
        if (index >= numberOfScreens) index = 0;
        index += value;

        if (index > 255) index = 255;
        selectScreen(index - 1);
      }


    }



    void updateDisplay() {
      //display panels
      if (Mode != external && millis() - eventTimer >= eventDelay) {

        //immediate update
        if (eventDelay == 0) eventDelay = 5000;

        //update screen
        if (forceUpdateDisplay) {
          forceUpdateDisplay = false;
          eventDelay = showScreen(currentScreenIndex);
          if (eventDelay != 0) forceUpdateDisplay = true;
        }

        if (nextScreenIndex != currentScreenIndex) {
          lastScreenIndex = currentScreenIndex;
          currentScreenIndex = nextScreenIndex;
          eventDelay = showScreen(currentScreenIndex);
          if (eventDelay != 0) forceUpdateDisplay = true;
        }


        eventTimer = millis();
      }
      else if (Mode == external && clearScreen == true && millis() - eventTimer >= eventDelay) {
        clearScreen = false;
        setLine0("", 0, mirolo.DSP.scrolldelay0);
        setLine1("", 0, mirolo.DSP.scrolldelay1);
        setRingColor(0x00);
      }
      else if (Mode == external && millis() - eventTimer >= eventDelay) {
        eventTimer = millis();
      }

    }

  public:

    //constructor
    schedule() {
    }

    //destructor
    ~schedule() {
    }


    //setup
    void Setup() {
      // initialize I2C bus
      Wire.begin();

      // initialize Real Time Clock module DS1307 to provide the system time
      setSyncProvider(RTC.get);
      if (timeStatus() != timeSet || digitalRead(pin_s1) == 0 || EEPROM.read(0) == true) {
        Serial.println(F("RTC Setup: Waiting for Serial. Press S2 to cancel"));
        while (digitalRead(pin_s2) != 0) {
          if (Serial.available()) {
            RTC.set(Serial.parseInt());   // set the RTC and the system time to the received value
            EEPROM.update(0, false);
            asm volatile ("  jmp 0");     // reboot
          }
        }
      }
      setSyncInterval(3600);

      //restore ring color and animation
      if (Mode != external) mirolo.DSP.apply_changes |= ANIM;
      //restore screen
      forceUpdateDisplay = true;
    }

    //mainloop
    void Loop() {

      //react to IR codes
      uint32_t * code = infrared.getCurrentCode();
      if (code != NULL && testMode == true) { //disable display test mode with any key
        setDisplayTest(false);
      }
      else if (code != NULL && strcmp_P(session, PSTR("0")) == 0) {
        switch (*code) {
          case IR_RED: //red
            setRingColor(0xff0000);
            defaultColor = 0xff0000;
            settingsChanged = true; //update eeprom
            break;
          case IR_GREEN: //green
            setRingColor(0x00ff00);
            defaultColor = 0x00ff00;
            settingsChanged = true; //update eeprom
            break;
          case IR_YELLOW: //yellow
            setRingColor(0xffb000);
            defaultColor = 0xffb000;
            settingsChanged = true; //update eeprom
            break;
          case IR_BLUE: //blue
            setRingColor(0x0000ff);
            defaultColor = 0x0000ff;
            settingsChanged = true; //update eeprom
            break;
          case IR_PWR1: //power(right)
            cycleAnimationType();
            settingsChanged = true; //update eeprom
            break;
          case IR_PWR: //power(left)
            mirolo.DSP.pwr = !mirolo.DSP.pwr;
            settingsChanged = true; //update eeprom
            break;
          case IR_BACK: //back
            asm volatile ("  jmp 0");     // reboot
            break;
          case IR_UP: //up
            adjustBrightness(uint8_t(mirolo.DSP.brightness + 1));
            settingsChanged = true; //update eeprom
            break;
          case IR_DOWN: //down
            adjustBrightness(uint8_t(mirolo.DSP.brightness - 1));
            settingsChanged = true; //update eeprom
            break;
          case IR_LEFT: { //left
              uint8_t index = nextScreenIndex == 0 ? (sizeof(screens) / sizeof(st_screen)) - 1 : nextScreenIndex - 1; //rollover
              selectScreen(index);
              settingsChanged = true; //update eeprom
              break;
            }
          case IR_RIGHT: { //right
              uint8_t index = nextScreenIndex >= ((sizeof(screens) / sizeof(st_screen)) - 1) ? 0 : nextScreenIndex + 1; //rollover
              selectScreen(index);
              settingsChanged = true; //update eeprom
              break;
            }
          case IR_MOUSE: //mouse
            adjustBrightness(16);
            settingsChanged = true; //update eeprom
            break;
          case IR_HOME: //home
            cycleAB();
            settingsChanged = true; //update eeprom
            break;
          case IR_1: //1
            selectScreenKeypad(1);
            settingsChanged = true; //update eeprom
            break;
          case IR_2: //2
            selectScreenKeypad(2);
            settingsChanged = true; //update eeprom
            break;
          case IR_3: //3
            selectScreenKeypad(3);
            settingsChanged = true; //update eeprom
            break;
          case IR_4: //4
            selectScreenKeypad(4);
            settingsChanged = true; //update eeprom
            break;
          case IR_5: //5
            selectScreenKeypad(5);
            settingsChanged = true; //update eeprom
            break;
          case IR_6: //6
            selectScreenKeypad(6);
            settingsChanged = true; //update eeprom
            break;
          case IR_7: //7
            selectScreenKeypad(7);
            settingsChanged = true; //update eeprom
            break;
          case IR_8: //8
            selectScreenKeypad(8);
            settingsChanged = true; //update eeprom
            break;
          case IR_9: //9
            selectScreenKeypad(9);
            settingsChanged = true; //update eeprom
            break;
          case IR_0: //0
            selectScreenKeypad(0);
            settingsChanged = true; //update eeprom
            break;
          case IR_DEL: //delete
            selectScreenKeypad(-1);
            settingsChanged = true; //update eeprom
            break;
          case IR_MUTE: //mute
            updateEEPROM(); //update eeprom immediately
            break;
          case IR_MENU: //menu
            cycleMode();
            settingsChanged = true; //update eeprom
            break;
          case IR_SET: //set
            cycleColor();
            settingsChanged = true; //update eeprom
            break;
          case IR_TVIN: //tv in
            setDisplayTest(true); //toggle display test mode
            break;
          case IR_VOLUP: //vol+
            adjustScrollDelay(uint8_t(mirolo.DSP.scrolldelay0 + 5));
            settingsChanged = true; //update eeprom
            break;
          case IR_VOLDOWN: //vol-
            adjustScrollDelay(uint8_t(mirolo.DSP.scrolldelay0 - 5));
            settingsChanged = true; //update eeprom
            break;
          case IR_OK: //ok
            if (forceUpdateDisplay == true) eventDelay = 0; //exit menu immediately
            break;
        }
      }


      //run updater
      if (strcmp_P(session, PSTR("0")) == 0) updateDisplay();

    }
};


schedule scheduler;



#endif
