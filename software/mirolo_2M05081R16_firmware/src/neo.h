//@(#) neo.h

#ifndef NEO_H
#define NEO_H

#include <Adafruit_NeoPixel.h>          //WS2812 library
#include "WS2812_Definitions.h"


class neo
{

  private:

    unsigned long neo_show_timer = 0;
    const uint16_t neo_show_delay = 300;

    //color calibration in %
    const uint8_t calibration_R;
    const uint8_t calibration_G;
    const uint8_t calibration_B;

    const uint16_t led_count;
    const uint16_t led_pin;
    uint32_t * colors;
    uint8_t brightness = 100;



    //applies color calibration
    void calibrateLEDs() {
      for (uint16_t i = 0; i < led.numPixels(); i++) {
        uint32_t color = led.getPixelColor(i);
        byte red = (((color & 0xFF0000) >> 16) * calibration_R + 100 / 2) / 100; //always round to the next highest number
        byte green = (((color & 0x00FF00) >> 8) * calibration_G + 100 / 2) / 100;
        byte blue = (((color & 0x0000FF)) * calibration_B + 100 / 2) / 100;
        led.setPixelColor(i, red, green, blue);
      }
    }


    //applies brightness setting in %
    void applyBrightness() {
      for (uint16_t i = 0; i < led.numPixels(); i++) {
        uint32_t color = led.getPixelColor(i);
        byte red = (((color & 0xFF0000) >> 16) * this->brightness + 100 / 2) / 100; //always round to the next highest number
        byte green = (((color & 0x00FF00) >> 8) * this->brightness + 100 / 2) / 100;
        byte blue = (((color & 0x0000FF)) * this->brightness + 100 / 2) / 100;
        led.setPixelColor(i, red, green, blue);
      }
    }



  public:

    Adafruit_NeoPixel led;

    //constructor
    neo(const uint16_t &count, const uint16_t &pin, const uint8_t &calR = 100, const uint8_t &calG = 100, const uint8_t &calB = 100) : led_count(count), led_pin(pin), led(Adafruit_NeoPixel(this->led_count, this->led_pin, NEO_GRB + NEO_KHZ800)), calibration_R(calR), calibration_G(calG), calibration_B(calB) {

      this->colors = new uint32_t[this->led_count];
      for (uint16_t i = 0; i < led_count; i++) {
        this->colors[i] = led.getPixelColor(i);
      }
    }

    //destructor
    ~neo() {
      delete [] this->colors;
    }



    //Sets all LEDs to off, but DOES NOT update the display
    void clearLEDs() {
      for (uint16_t i = 0; i < led.numPixels(); i++) {
        led.setPixelColor(i, 0);
      }
    }


    //set the overall LED brightness level in %
    void setBrightness(uint8_t b) {
      this->brightness = constrain(b, 0, 100);
    }

    //get the overall LED brightness level in %
    uint8_t & getBrightness() {
      return this->brightness;
    }


    //Force update of the LED string on the next programm cycle
    void forceUpdate() {
      neo_show_timer = millis() - neo_show_delay;
    }




    //setup
    void Setup() {
      led.begin();
      clearLEDs();
      led.show();
    }

    //mainloop
    void Loop() {

      //update rgb LED ring
      if (millis() - neo_show_timer >= neo_show_delay) {
        //save color data before calibration and brightness level adjustment
        for (uint16_t i = 0; i < led.numPixels(); i++) {
          this->colors[i] = led.getPixelColor(i);
        }
        calibrateLEDs();
        applyBrightness();
        led.show();
        //reset color data after calibration and brightness level adjustment
        for (uint16_t i = 0; i < led.numPixels(); i++) {
          led.setPixelColor(i, this->colors[i]);
        }
        neo_show_timer = millis();
      }
    }
};


//leds.setPixelColor(i, color);
//leds.setPixelColor(i, red, green, blue);

#endif
