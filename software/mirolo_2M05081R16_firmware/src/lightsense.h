//@(#) lightsense.h

#ifndef LIGHTSENSE_H
#define LIGHTSENSE_H

class lightsense
{

  private:

    const uint16_t ldr;

    unsigned long lightsensor_timer = 0;
    const uint16_t lightsensor_delay = 500;


  public:

    word value;


    lightsense(const uint16_t &pin) : ldr(pin) {

    }

    void Update() { //handle ldr light sensor


      const byte value_store = 10;
      static int sense_light[value_store];

      static int light_count = -1;

      if (light_count == -1) { //init data arrays

        for (int i = 0; i < value_store; i++) {
          sense_light[i] = analogRead(ldr);
        }
        light_count = 0;
      }

      sense_light[light_count] = analogRead(ldr);

      if (++light_count >= value_store) { //full cycle
        light_count = 0; //reset counter
      }

      //value mediation to prevent peaks
      int tmp_light = 0;
      for (int i = 0; i < value_store; i++) {
        tmp_light += sense_light[i];
      }
      value = tmp_light / value_store;
    }


    //mainloop
    void Loop() {

      //update rgb LED ring
      if (millis() - lightsensor_timer >= lightsensor_delay) {
        Update();
        lightsensor_timer = millis();
      }
    }

};

#endif
