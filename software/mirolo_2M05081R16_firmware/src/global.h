//@(#) global.h

#ifndef GLOBAL_H
#define GLOBAL_H

#include "neo.h"
#include "input.h"
#include "ir.h"
#include "lightsense.h"
#include "led.h"

//Device specific settings. Set the MAC address, RGB color calibration in %,
//the direction and start pixel of the LED ring (positive = clockwise, negative = counter clockwise) and a unique serial number.
#if SERIALNR == 1
const byte mac[] PROGMEM = {0x20, 0xDC, 0x93, 0x3B, 0x9B, 0x0C}; //Prefix 20-DC-93 Cheetah Hi-Tech, Inc. + HEX(flip_horiz(SNR))
const uint8_t calibration_R = 100;
const uint8_t calibration_G = 50;
const uint8_t calibration_B = 63;
const int8_t ring_start = 1;
const IPAddress static_ip = IPAddress(192, 168, 130, 91);
#define serial_nr "2017.10.0001"
#endif
#if SERIALNR == 2
const byte mac[] PROGMEM = {0x20, 0xDC, 0x93, 0x77, 0x3E, 0xDB}; //Prefix 20-DC-93 Cheetah Hi-Tech, Inc. + HEX(flip_horiz(SNR))
const uint8_t calibration_R = 100;
const uint8_t calibration_G = 50;
const uint8_t calibration_B = 63;
const int8_t ring_start = 1;
const IPAddress static_ip = IPAddress(192, 168, 130, 92);
#define serial_nr "2018.06.0002"
#endif
#if SERIALNR == 3
const byte mac[] PROGMEM = {0x20, 0xDC, 0x93, 0xB2, 0xD9, 0xA5}; //Prefix 20-DC-93 Cheetah Hi-Tech, Inc. + HEX(flip_horiz(SNR))
const uint8_t calibration_R = 100;
const uint8_t calibration_G = 50;
const uint8_t calibration_B = 63;
const int8_t ring_start = 1;
const IPAddress static_ip = IPAddress(192, 168, 130, 93);
#define serial_nr "2018.06.0003"
#endif
#if SERIALNR == 4
const byte mac[] PROGMEM = {0x20, 0xDC, 0x93, 0xEE, 0x74, 0x6F}; //Prefix 20-DC-93 Cheetah Hi-Tech, Inc. + HEX(flip_horiz(SNR))
const uint8_t calibration_R = 100;
const uint8_t calibration_G = 50;
const uint8_t calibration_B = 63;
const int8_t ring_start = 1;
const IPAddress static_ip = IPAddress(192, 168, 130, 94);
#define serial_nr "2018.06.0004"
#endif
#if SERIALNR == 5
const byte mac[] PROGMEM = {0x20, 0xDC, 0x93, 0x12, 0xA0, 0xF3}; //Prefix 20-DC-93 Cheetah Hi-Tech, Inc. + HEX(flip_horiz(SNR))
const uint8_t calibration_R = 100;
const uint8_t calibration_G = 50;
const uint8_t calibration_B = 63;
const int8_t ring_start = 1;
const IPAddress static_ip = IPAddress(192, 168, 130, 95);
#define serial_nr "2018.06.0005"
#endif
#if SERIALNR == 6
const byte mac[] PROGMEM = {0x20, 0xDC, 0x93, 0x16, 0x5A, 0xA0}; //Prefix 20-DC-93 Cheetah Hi-Tech, Inc. + HEX(flip_horiz(SNR))
const uint8_t calibration_R = 100;
const uint8_t calibration_G = 50;
const uint8_t calibration_B = 63;
const int8_t ring_start = -2;
const IPAddress static_ip = IPAddress(192, 168, 130, 96);
#define serial_nr "2019.06.0006"
#endif


//ir receiver
irRecv infrared(ir);

//neopixel
neo ring(16, ws2812, calibration_R, calibration_G, calibration_B);

//button inputs
input s1(pin_s1);
input s2(pin_s2);

//lightsensor
lightsense lightsensor(ldr);

//system led
singleLED led1(pin_led1);
singleLED led2(pin_led2);

//global variables
EthernetServer server(80);
EthernetClient client;

//update constants
const uint8_t LINE0 = B00000001;
const uint8_t LINE1 = B00000010;
const uint8_t RING0 = B00000100;
const uint8_t BRIGHT = B00001000;
const uint8_t ANIM = B00010000;
const uint8_t NAME = B00100000;
const uint8_t MEMCPY = B10000000;
const uint8_t ALL = B01111111;

uint8_t method = 0; //http Method (UNDEFINED/GET/POST/....)
const uint8_t url_size = 50;
const uint8_t frame_size = 25;
char url[url_size] = {'\0'}; //http receive buffer
const uint16_t recv_content_size = 500;
char recv_content[recv_content_size] = {'\0'}; //http receive buffer
char session[17] = {"0"}; //http session cookie 128bit/16byte
boolean authOK = false; //http authentification
boolean sessionOK = false; //http session

char pin[5] = {HTTPPASSWD}; //access password
char username[21] = {"\0"}; //access username

bool settingsChanged = false; //request update eeprom bit
bool testMode = false; //display test mode

const uint8_t dsp_name_size = 25;
const uint8_t dsp_prefix_size = 4;
const uint8_t dsp_animation_size = 20;

//network configuration
struct st_netconf {
  IPAddress ip = IPAddress(0, 0, 0, 0);
};

st_netconf netconf;


//animation frame
struct st_frame {
  uint32_t Colors[16];
  uint32_t Time;
};

//the actual animation
struct st_animation {
  st_frame Frames[frame_size];
  boolean Loop;
  uint8_t CurrentFrame;
  uint8_t LastFrame;
};

st_animation mirolo_animation;
st_animation mirolo_active_animation;


//the display parameters. changes to these may not be applied immediatly
struct miroloDISPLAY {
  char    message0[max_bufsize];   //line0
  char    prefix0[4];
  uint8_t scrolldelay0;
  char    message1[max_bufsize];   //line1
  char    prefix1[4];
  uint8_t scrolldelay1;

  uint8_t brightness;
  uint32_t color;
  char animation[20];
  boolean pwr;
  uint8_t apply_changes;
};

//the display hardware parameters. changes to these are applied immediatly
struct miroloHW {
  boolean htmlON;
  boolean restON;
  char Name[25];
  const char HW[20];
  const char CPU[20];
  const char SNR[13];
  char FW[25];
  char MAC[18];
  char IP[16];
  miroloDISPLAY DSP;
};


miroloHW mirolo = {true, true, "Panel Room 2", "mirolo 2M05081R16", "Atmega1284p@20MHz", serial_nr, "", "", "", {"", "off", 75, "", "off", 75, 8, 0, "off", true, MEMCPY} };

miroloHW mirolo_active = {true, true, "", "", "", "", "", "", "", {"", "off", 0, "", "off", 0, 0, 0, "off", true, MEMCPY} };

#endif
