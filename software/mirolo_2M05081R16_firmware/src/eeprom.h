//@(#) eeprom.h

#ifndef EEPROM_H
#define EEPROM_H

/*Other EEPROM values:
    Index           Value
    0               set clock bit
*/

const uint8_t start_index = 30;

void updateEEPROM() { //update EEPROM if necessary
  uint16_t index = start_index;
  EEPROM.put(index, mirolo_active);
  EEPROM.put(index += sizeof(miroloHW), netconf);
  EEPROM.put(index += sizeof(netconf), mirolo_active_animation);

#if LOAD_MODULE == 1
  EEPROM.put(index += sizeof(mirolo_active_animation), scheduler);
  index += sizeof(scheduler);
  for (uint8_t i = 0; i < (sizeof(events) / sizeof(st_event)); ++i) { //only save event.Status
    uint16_t Status = events[i].Status;
    EEPROM.put(index, Status);
    index += sizeof(Status);
  }
#endif
#if LOAD_MODULE == 2
  EEPROM.put(index += sizeof(mirolo_active_animation), scheduler);
  index += sizeof(scheduler);
#endif
}

void loadEEPROM() { //get settings from EEPROM
  uint16_t index = start_index;
  EEPROM.get(index, mirolo_active);
  EEPROM.get(index += sizeof(miroloHW), netconf);
  EEPROM.get(index += sizeof(netconf), mirolo_active_animation);

#if LOAD_MODULE == 1
  EEPROM.get(index += sizeof(mirolo_active_animation), scheduler);
  index += sizeof(scheduler);
  for (uint8_t i = 0; i < (sizeof(events) / sizeof(st_event)); ++i) { //only save event.Status
    uint16_t Status = 0;
    EEPROM.get(index, Status);
    events[i].Status = Status;
    index += sizeof(Status);
  }
#endif
#if LOAD_MODULE == 2
  EEPROM.get(index += sizeof(mirolo_active_animation), scheduler);
  index += sizeof(scheduler);
#endif
}


#endif
