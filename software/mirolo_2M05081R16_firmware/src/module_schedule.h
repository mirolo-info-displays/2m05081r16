//@(#) module_schedule.h

/*
  Module for displaying preconfigured scheduled events and timetables automatically. This is intended for
  use as an independent information display at conventions and the like to inform visitors about
  room status, upcoming, delayed or rescheduled events.
  A DS1307 Module has to be connected to the I²C Bus for time management.

  Edit the "room" and "events" structs below to your needs
  and have a look at the remote control documentation for information on button assignment and codes.
*/

#ifndef MODULE_SCHEDULE_H
#define MODULE_SCHEDULE_H

// INFO: set system time via serial command: 
//   stty -F /dev/ttyUSB0 115200
//   echo "system:setclock" > /dev/ttyUSB0 && sleep 5 && date '+%s' -d '+ 2 hour' > /dev/ttyUSB0
// DTR (auto reset) has to be disconnected!

#include <time.h>
#include <Wire.h>
#include <DS1307RTC.h>
#include "remote.h"

//function declaration
void updateEEPROM();

#if EVENT == gc
enum room : uint8_t {mainhall = 0, foyer, panel1, panel2, partyhall, ccg, workshop, outside, UNASSIGNED};
const char * const roomTitle[] = {"Main Hall", "Foyer", "Panel Room 1", "Panel Room 2", "Party Hall", "Card Games", "Workshop", "Outside", ""};
#else
enum room : uint8_t {mainhall = 0, panel1, UNASSIGNED};
const char * const roomTitle[] = {"Main Hall", "Panel Room 1", ""};
#endif

const uint32_t colors[] = {RED, GREEN, BLUE, WHITE, CYAN, MAGENTA, DARKORANGE};


struct st_event {
  time_t Start;
  time_t End;
  char Title[60];
  uint16_t Status; //0(normal), 1-99(delay in minutes), 3xx(moved to other room, xx = room Index), 400(canceled)
  room Room;
};

#if EVENT == gc
st_event events[] = {

  //friday
  {1659092400, 1659110400, "Convention Setup | GalaCon 2022 /)(\\", 0, mainhall},
  //{1659092400, 1659110400, "Volunteer Briefing", 0, mainhall},
  {1659092400, 1659110400, "Convention Setup | GalaCon 2022 /)(\\", 0, foyer},
  {1659092400, 1659110400, "Convention Setup | GalaCon 2022 /)(\\", 0, panel1},
  {1659092400, 1659110400, "Convention Setup | GalaCon 2022 /)(\\", 0, panel2},
  {1659092400, 1659110400, "Convention Setup | GalaCon 2022 /)(\\", 0, partyhall},
  {1659092400, 1659110400, "Convention Setup | GalaCon 2022 /)(\\", 0, ccg},
  {1659092400, 1659110400, "Convention Setup | GalaCon 2022 /)(\\", 0, workshop},
  {1659092400, 1659110400, "Convention Setup | GalaCon 2022 /)(\\", 0, outside},
  
  //saturday
  //{1564218000, 1564219800, "Volunteer Briefing", 0, mainhall},
  {1659175200, 1659177900, "Opening Ceremony", 0, mainhall},
  {1659179700, 1659183200, "That Shouldn't Bend that Way: Animation Horror Stories", 0, mainhall},
  {1659185100, 1659191400, "Trixie Wants YOU!", 0, mainhall},
  {1659193200, 1659195000, "Cosplay Contest", 0, mainhall},
  {1659196800, 1659200400, "The Songs of Magic", 0, mainhall},
  {1659202200, 1659204000, "Award Ceremony", 0, mainhall},
  {1659204900, 1659207600, "My Big Karaoke", 0, mainhall},
  {1659214800, 1659222000, "PlushieCon", 0, mainhall},

  {1659179700, 1659185100, "BIZAAM Signing Session", 0, foyer},
  {1659188700, 1659194100, "Signing Session", 0, foyer},
  {1659210300, 1659211200, "Ball Opening", 0, foyer},
  {1659221200, 1659214800, "Gala Ball: Poni1Kenobi", 0, foyer},
  {1659214800, 1659218400, "Gala Ball: Cyril The Wolf", 0, foyer},
  {1659218400, 1659222000, "Gala Ball: Coltastrophe", 0, foyer},
  {1659222000, 1659225600, "Gala Ball: DJ Direkt", 0, foyer},

  {1659185100, 1659188700, "Tails of Equestria - Choose Your Own Adventure", 0, panel1},
  {1659190500, 1659194100, "History of MLP and the Longevity of the Fandom", 0, panel1},
  {1659195900, 1659199500, "How to Voiceact: How to Get Started 101", 0, panel1},
  {1659201300, 1659204900, "Unicorns and other Fable-Equines", 0, panel1},

  {1659181500, 1659185100, "Improvisation Acting - An Introduction", 0, panel2},
  {1659188700, 1659192300, "Social Media for Bronys", 0, panel2},
  {1659194100, 1659197700, "Bright Your Art", 0, panel2},
  {1659199500, 1659203100, "StormXF3 Panel", 0, panel2},

  {1659207600, 1659211200, "Gala Party: Blackened Blue", 0, partyhall},
  {1659211200, 1659214800, "Gala Party: Prince Whateverer", 0, partyhall},
  {1659214800, 1659218400, "Gala Party: Tw3lv3", 0, partyhall},
  {1659218400, 1659222000, "Gala Party: Waranto & Javier", 0, partyhall},
  {1659222000, 1659225600, "Gala Party: Faulty & General Mumble", 0, partyhall},
  
  {1659186000, 1659200400, "CCG Constructed Tournament", 0, ccg},
  {1659207600, 1659218400, "The Seapony's Pearl - a Heist RPG", 0, ccg},

  {1659183300, 1659188700, "Needlefelting Workshop with HipsterOwlet", 0, workshop},
  {1659192300, 1659197700, "Melo's Little Plushie Workshop", 0, workshop},
  {1659200400, 1659214800, "Game Tavern", 0, workshop},

  {1659183300, 1659186000, "Fursuit Walk", 0, outside},
  {1659189600, 1659200400, "Buckball", 0, outside},
  {1659205800, 1659211200, "Our Little Chalk Ponies", 0, outside},
  

  //sunday
  {1659266100, 1659272300, "Creating a New Generation", 0, mainhall},
  {1659273300, 1659276900, "German Voice Actor Panel", 0, mainhall},
  {1659277800, 1659281400, "The Spark of Creativity", 0, mainhall},
  {1659283200, 1659291300, "Charity Auction", 0, mainhall},
  {1659291300, 1659294000, "Closing Ceremony", 0, mainhall},

  {1659276900, 1659282300, "Signing Session", 0, foyer},

  {1659266100, 1659269700, "MLP Cosplay 101", 0, panel2},
  {1659271500, 1659275100, "Behind the Cons", 0, panel2},

  {1659268800, 1659281400, "PlushieCon Plush Return", 0, partyhall},

  {1659270600, 1659276000, "MLP Cosplay 101 (Workshop)", 0, workshop},

  {1659269700, 1659275100, "CCG Open Play", 0, ccg},

  {1659269700, 1659280500, "Buckball", 0, outside}

};
#else
st_event events[] = {
  //friday
  {1564146000, 1564162200, "Convention Setup | Test Event", 0, mainhall},
  {1564162200, 1564164000, "Volunteer Briefing", 0, panel1},
};
#endif


class schedule
{

  private:
    const uint8_t maxHoursBetweenPanels = 8;
    const uint8_t maxUpcomingPanels = 4;
    enum screen : uint8_t {current = 0, upcoming, next, roomclock};
    enum mode : uint8_t {automatic = 0, staticclock, external, MODEEND};
    uint8_t eventIndex = 0;
    uint32_t eventTimer = 0;
    uint16_t eventDelay = 0;
    room Room = UNASSIGNED;
    screen Screen = roomclock;
    mode Mode = automatic;

    bool clearScreen = false;


    void setLine0(const char * message, const uint8_t &prefix, const uint8_t &scrolldelay) {
      memset(mirolo.DSP.message0, 0, max_bufsize);
      strcpy(mirolo.DSP.message0, message);
      if (prefix == 0)strcpy_P(mirolo.DSP.prefix0, PSTR("off"));
      else ltoa(prefix, mirolo.DSP.prefix0, 10);
      mirolo.DSP.scrolldelay0 = scrolldelay;
      mirolo.DSP.apply_changes |= LINE0;
    }

    void setLine1(const char * message, const uint8_t &prefix, const uint8_t &scrolldelay) {
      memset(mirolo.DSP.message1, 0, max_bufsize);
      strcpy(mirolo.DSP.message1, message);
      if (prefix == 0)strcpy_P(mirolo.DSP.prefix1, PSTR("off"));
      else ltoa(prefix, mirolo.DSP.prefix1, 10);
      mirolo.DSP.scrolldelay1 = scrolldelay;
      mirolo.DSP.apply_changes |= LINE1;
    }

    void buildClock(char * clockstring, const uint8_t &h, const uint8_t &m, const uint8_t &s = 60) { //build clock in hh:mm:ss or hh:mm format
      char tmp[3] = {'\0'};
      ltoa(h, tmp, 10);
      if (h < 10) strcat_P(clockstring, PSTR("0"));
      strcat(clockstring, tmp);
      strcat_P(clockstring, PSTR(":"));
      ltoa(m, tmp, 10);
      if (m < 10) strcat_P(clockstring, PSTR("0"));
      strcat(clockstring, tmp);
      if (s < 60) {
        strcat_P(clockstring, PSTR(":"));
        ltoa(s, tmp, 10);
        if (s < 10) strcat_P(clockstring, PSTR("0"));
        strcat(clockstring, tmp);
      }
    }


    void setRingColor(const uint32_t & color) {
      mirolo.DSP.color = color;
      mirolo.DSP.apply_changes |= ANIM;
    }

    void setRingAnimation(const char * animation) {
      strcpy_P(mirolo.DSP.animation, animation);
      mirolo.DSP.apply_changes |= ANIM;
    }

    void modifyStatus(const int8_t &value) {
      clearScreen = true;
      char msg0[10] = {'\0'};
      char msg1[40] = {'\0'};

      if (Room != UNASSIGNED) {
        if (value == -1) {
          events[eventIndex].Status = 0;
        }
        else if (events[eventIndex].Status < 400) {
          events[eventIndex].Status *= 10;
          events[eventIndex].Status += value;
        }

        ltoa(events[eventIndex].Status, msg1, 10);

      }
      else {
        strcpy_P(msg1, PSTR("---"));
      }

      strcpy_P(msg0, PSTR("Code"));
      setLine0(msg0, 0, mirolo.DSP.scrolldelay0);
      setLine1(msg1, 0, mirolo.DSP.scrolldelay1);

      eventTimer = millis();
      eventDelay = 2000;
    }

    void cycleAnimationType() {
      static uint8_t index = 0;
      switch (index++) {
        case 0:
          setRingAnimation(PSTR("on"));
          break;
        case 1:
          setRingAnimation(PSTR("blink (1Hz)"));
          break;
        case 2:
          setRingAnimation(PSTR("pulse"));
          break;
        case 3:
          setRingAnimation(PSTR("loading"));
          break;
        case 4:
          setRingAnimation(PSTR("off"));
          index = 0;
          break;
      }
    }

    void cycleRoom() {
      clearScreen = true;
      Room = room(Room + 1);
      if (Room > UNASSIGNED) Room = (room)0;

      for (uint8_t i = 0; i < (sizeof(events) / sizeof(st_event)); ++i) { //try to set eventIndex to a valid value for this room
        if (events[i].Room == Room) {
          eventIndex = i;
          break;
        }
      }

      char msg0[10] = {'\0'};
      strcpy_P(msg0, PSTR("Room"));
      setLine0(msg0, 0, mirolo.DSP.scrolldelay0);
      setLine1(roomTitle[Room], 0, mirolo.DSP.scrolldelay1);

      if (Room == UNASSIGNED) strcpy_P(mirolo.Name, PSTR("---"));
      else strcpy(mirolo.Name, roomTitle[Room]);
      mirolo.DSP.apply_changes |= NAME;

      eventTimer = millis();
      eventDelay = 2000;
    }

    void cycleMode() {
      clearScreen = true;
      Mode = mode(Mode + 1);
      if (Mode == MODEEND) Mode = (mode)0;

      char msg0[10] = {'\0'};
      strcpy_P(msg0, PSTR("Mode"));
      setLine0(msg0, 0, mirolo.DSP.scrolldelay0);
      char msg1[40] = {'\0'};
      if (Mode == automatic) {
        strcpy_P(msg1, PSTR("auto"));
      }
      else if (Mode == staticclock) {
        strcpy_P(msg1, PSTR("static clock"));
        Screen = roomclock;
      }
      else if (Mode == external) {
        strcpy_P(msg1, PSTR("external"));
      }
      setLine1(msg1, 0, mirolo.DSP.scrolldelay1);

      eventTimer = millis();
      eventDelay = 2000;
    }

    void cycleColor() {
      static uint8_t index = 0;

      setRingColor(colors[index]);
      index++;
      if (index >= sizeof(colors) / sizeof(uint32_t)) index = 0;
    }

    void adjustBrightness(int8_t brightness) {
      clearScreen = true;
      if (brightness > 16) brightness = 16;
      if (brightness < 0) brightness = 0;

      mirolo.DSP.brightness = brightness;
      mirolo.DSP.apply_changes |= BRIGHT;

      char msg0[10] = {'\0'};
      strcpy_P(msg0, PSTR("Bright"));
      setLine0(msg0, 0, mirolo.DSP.scrolldelay0);
      char msg1[40] = {'\0'};
      if (mirolo.DSP.brightness < 16) {
        ltoa(10 + (mirolo.DSP.brightness) * (96 / 16), msg1, 10);
        strcat_P(msg1, PSTR("%"));
      }
      else {
        strcat_P(msg1, PSTR("auto (LDR)"));
      }
      setLine1(msg1, 0, mirolo.DSP.scrolldelay1);

      eventTimer = millis();
      eventDelay = 2000;
    }

    void adjustScrollDelay(uint8_t scrolldelay) {
      clearScreen = true;
      if (scrolldelay > 100) scrolldelay = 100;
      if (scrolldelay >= 20 && scrolldelay <= 100) {
        mirolo.DSP.scrolldelay0 = scrolldelay;
        mirolo.DSP.scrolldelay1 = scrolldelay;
        mirolo.DSP.apply_changes |= (LINE0 | LINE1);
      }

      char msg0[10] = {'\0'};
      strcpy_P(msg0, PSTR("Scroll"));
      setLine0(msg0, 0, mirolo.DSP.scrolldelay0);
      char msg1[40] = {'\0'};
      ltoa(mirolo.DSP.scrolldelay0, msg1, 10);
      strcat_P(msg1, PSTR("ms"));
      setLine1(msg1, 0, mirolo.DSP.scrolldelay1);

      eventTimer = millis();
      eventDelay = 2000;
    }



    void updateDisplay() {
      //display panels
      if (Mode != external && millis() - eventTimer >= eventDelay) {

        switch (Screen) {
          case roomclock: {
              //display clock and room title
              static uint8_t counter = 0;
              char msg0[10] = {'\0'};
              buildClock(msg0, hour(), minute(), second());
              setLine0(msg0, 0, mirolo.DSP.scrolldelay0);
              setLine1(roomTitle[Room], 0, mirolo.DSP.scrolldelay1);
              eventDelay = 1000;
              if (counter++ > 8) {
                eventDelay = 25; //fall through to have a smooth running clock in case there are no panels to list
                counter = 0;
                if (Mode == staticclock) Screen = roomclock;
                else Screen = current;
              }
              break;
            }
          case current: {
              //display current panel
              bool eventfound = false;
              for (uint8_t i = 0; i < (sizeof(events) / sizeof(st_event)); ++i) {
                if (events[i].Room == Room) {
                  uint16_t minsDelay = events[i].Status < 100 ? events[i].Status : 0;
                  if (!eventfound && (events[i].Start + minsDelay * 60) <= now() && (events[i].End + minsDelay * 60) >= now()) {
                    eventfound = true;
                    eventIndex = i;
                    char msg0[40] = {'\0'};
                    char msg1[max_bufsize] = {'\0'};


                    if (events[eventIndex].Status == 400) { //panel cancelled
                      strcpy_P(msg0, PSTR("Panel cancelled"));
                      setLine0(msg0, 0, mirolo.DSP.scrolldelay0);
                      strcat_P(msg1, PSTR("(CANCELLED) "));
                      strcat(msg1, events[eventIndex].Title);
                      setLine1(msg1, 0, mirolo.DSP.scrolldelay1);
                    }

                    else if (events[eventIndex].Status >= 300 && events[eventIndex].Status <= 399) { //panel moved
                      strcpy_P(msg0, PSTR("Panel moved"));
                      setLine0(msg0, 0, mirolo.DSP.scrolldelay0);
                      strcat(msg1, events[eventIndex].Title);
                      strcat_P(msg1, PSTR(" MOVED TO "));
                      uint8_t movedRoom = events[eventIndex].Status - 300;
                      if (movedRoom < UNASSIGNED) {
                        strcat(msg1, roomTitle[movedRoom]);
                      }
                      else {
                        strcat_P(msg1, PSTR("ANOTHER ROOM"));
                      }
                      setLine1(msg1, 0, mirolo.DSP.scrolldelay1);
                    }

                    else { //all fine
                      strcpy_P(msg0, PSTR("NowPanel ends in "));
                      char tmp[10] = {'\0'};
                      uint32_t minsRemaining = (((events[eventIndex].End + minsDelay * 60) - now()) / 60) + 1;
                      if (minsRemaining > 60) {
                        uint8_t hoursRemaining = minsRemaining / 60;
                        ltoa(hoursRemaining, tmp, 10);
                        strcat(msg0, tmp);
                        strcat_P(msg0, PSTR(" hr "));
                        minsRemaining %= 60;
                      }
                      ltoa(minsRemaining, tmp, 10);
                      strcat(msg0, tmp);
                      strcat_P(msg0, PSTR(" min"));
                      setLine0(msg0, 3, mirolo.DSP.scrolldelay0);
                      strcat(msg1, events[eventIndex].Title);
                      setLine1(msg1, 0, mirolo.DSP.scrolldelay1);

                    }

                    eventDelay = 8000 + 2 * getMsgRoundtriptime(msg1, atol(mirolo_active.DSP.prefix1), mirolo_active.DSP.scrolldelay1, max_Line[1].mx);
                  }
                }
              }
              if (!eventfound) Screen = next;
              else Screen = upcoming;

              break;
            }
          case next: {
              //display next panel
              bool eventfound = false;
              for (uint8_t i = 0; i < (sizeof(events) / sizeof(st_event)); ++i) {
                if (events[i].Room == Room) {
                  uint16_t minsDelay = events[i].Status < 100 ? events[i].Status : 0;
                  if (!eventfound && events[i].Start < (now() + maxHoursBetweenPanels * 3600) && (events[i].Start + minsDelay * 60) > now()) {
                    eventfound = true;
                    eventIndex = i;
                    char msg0[40] = {'\0'};
                    char msg1[max_bufsize] = {'\0'};


                    if (events[eventIndex].Status == 400) { //panel cancelled
                      strcpy_P(msg0, PSTR("Panel cancelled"));
                      setLine0(msg0, 0, mirolo.DSP.scrolldelay0);
                      strcat_P(msg1, PSTR("(CANCELLED) "));
                      strcat(msg1, events[eventIndex].Title);
                      setLine1(msg1, 0, mirolo.DSP.scrolldelay1);
                    }

                    else if (events[eventIndex].Status >= 300 && events[eventIndex].Status <= 399) { //panel moved
                      strcpy_P(msg0, PSTR("Panel moved"));
                      setLine0(msg0, 0, mirolo.DSP.scrolldelay0);
                      buildClock(msg1, hour(events[eventIndex].Start), minute(events[eventIndex].Start));
                      strcat(msg1, events[eventIndex].Title);
                      strcat_P(msg1, PSTR(" MOVED TO "));
                      uint8_t movedRoom = events[eventIndex].Status - 300;
                      if (movedRoom < UNASSIGNED) {
                        strcat(msg1, roomTitle[movedRoom]);
                      }
                      else {
                        strcat_P(msg1, PSTR("ANOTHER ROOM"));
                      }
                      setLine1(msg1, 5, mirolo.DSP.scrolldelay1);
                    }

                    else { //all fine
                      uint32_t minsRemaining = (((events[eventIndex].Start + minsDelay * 60) - now()) / 60) + 1;
                      if (minsRemaining <= 90) {
                        ltoa(minsRemaining, msg0, 10);
                        strcat_P(msg0, PSTR(" min"));
                      }
                      else {
                        strcpy_P(msg0, PSTR("Next"));
                      }
                      setLine0(msg0, 0, mirolo.DSP.scrolldelay0);

                      buildClock(msg1, hour(events[eventIndex].Start), minute(events[eventIndex].Start));
                      if (minsDelay > 0) {
                        strcat_P(msg1, PSTR("(+"));
                        char mins[5] = {'\0'};
                        ltoa(minsDelay, mins, 10);
                        strcat(msg1, mins);
                        strcat_P(msg1, PSTR("min) "));
                      }
                      strcat(msg1, events[eventIndex].Title);
                      setLine1(msg1, 5, mirolo.DSP.scrolldelay1);
                    }
                    eventDelay = 8000 + 2 * getMsgRoundtriptime(msg1, atol(mirolo_active.DSP.prefix1), mirolo_active.DSP.scrolldelay1, max_Line[1].mx);
                    break;
                  }
                }
              }
              if (!eventfound) {
                eventDelay = 25; //fall through
                Screen = roomclock;
              }
              else {
                Screen = upcoming;
              }
              break;
            }
          case upcoming: {
              //display next panels
              uint8_t eventCounter = 0;
              char msg1[max_bufsize] = {'\0'};
              for (uint8_t i = 0; i < (sizeof(events) / sizeof(st_event)); ++i) {
                if (events[i].Room == Room) {
                  uint16_t minsDelay = events[i].Status < 100 ? events[i].Status : 0;
                  if (events[i].Start < (now() + maxHoursBetweenPanels * 3600) && (events[i].Start + minsDelay * 60) > now()) {
                    char clk[10] = {'\0'};
                    buildClock(clk, hour(events[i].Start), minute(events[i].Start));
                    strcat(msg1, clk);

                    if (events[i].Status == 400) { //panel cancelled
                      strcat_P(msg1, PSTR(" (CANCELLED)"));
                    }

                    else if (events[i].Status >= 300 && events[i].Status <= 399) { //panel moved
                      strcat_P(msg1, PSTR(" (MOVED TO "));
                      uint8_t movedRoom = events[i].Status - 300;
                      if (movedRoom < UNASSIGNED) {
                        strcat(msg1, roomTitle[movedRoom]);
                      }
                      else {
                        strcat_P(msg1, PSTR("ANOTHER ROOM"));
                      }
                      strcat_P(msg1, PSTR(")"));
                    }

                    else { //all fine
                      if (minsDelay > 0) {
                        strcat_P(msg1, PSTR(" (+"));
                        char mins[5] = {'\0'};
                        ltoa(minsDelay, mins, 10);
                        strcat(msg1, mins);
                        strcat_P(msg1, PSTR("min)"));
                      }
                    }

                    strcat_P(msg1, PSTR(" | "));
                    strcat(msg1, events[i].Title);
                    strcat_P(msg1, PSTR("          "));

                    if (eventCounter >= maxUpcomingPanels) break; //don't display more than 4 upcoming events
                    eventCounter++;
                  }
                }
              }
              if (eventCounter > 0) {
                char msg0[10] = {'\0'};
                strcpy_P(msg0, PSTR("Today"));
                setLine0(msg0, 0, mirolo.DSP.scrolldelay0);
                setLine1(msg1, 0, mirolo.DSP.scrolldelay1);
                eventDelay = 8000 + 2 * getMsgRoundtriptime(msg1, atol(mirolo_active.DSP.prefix1), mirolo_active.DSP.scrolldelay1, max_Line[1].mx);
              }
              else {
                eventDelay = 25; //fall through
              }
              Screen = roomclock;
              break;
            }
        }
        eventTimer = millis();
      }
      else if (Mode == external && clearScreen == true && millis() - eventTimer >= eventDelay) {
        clearScreen = false;
        setLine0("", 0, mirolo.DSP.scrolldelay0);
        setLine1("", 0, mirolo.DSP.scrolldelay1);
        setRingColor(0x00);
      }
      else if (Mode == external && millis() - eventTimer >= eventDelay) {
        eventTimer = millis();
      }

    }

  public:

    //constructor
    schedule() {

    }

    //destructor
    ~schedule() {
    }


    //setup
    void Setup() {
      // initialize I2C bus
      Wire.begin();

      // initialize Real Time Clock module DS1307 to provide the system time
      setSyncProvider(RTC.get);
      if (timeStatus() != timeSet || digitalRead(pin_s1) == 0 || EEPROM.read(0) == true) {
        Serial.println(F("RTC Setup: Waiting for Serial. Press S2 to cancel"));
        while (digitalRead(pin_s2) != 0) {
          if (Serial.available()) {
            RTC.set(Serial.parseInt());   // set the RTC and the system time to the received value
            EEPROM.update(0, false);
            asm volatile ("  jmp 0");     // reboot
          }
        }
      }
      setSyncInterval(3600);

      //restore ring color and animation
      if (Mode != external) mirolo.DSP.apply_changes |= ANIM;
    }

    //mainloop
    void Loop() {

      //react to IR codes
      uint32_t * code = infrared.getCurrentCode();
      if (code != NULL && testMode == true) { //disable display test mode with any key
        setDisplayTest(false);
      }
      else if (code != NULL && strcmp_P(session, PSTR("0")) == 0) {
        switch (*code) {
          case IR_RED: //red
            setRingColor(0xff0000);
            settingsChanged = true; //update eeprom
            break;
          case IR_GREEN: //green
            setRingColor(0x00ff00);
            settingsChanged = true; //update eeprom
            break;
          case IR_YELLOW: //yellow
            setRingColor(0xffb000);
            settingsChanged = true; //update eeprom
            break;
          case IR_BLUE: //blue
            setRingColor(0x0000ff);
            settingsChanged = true; //update eeprom
            break;
          case IR_PWR1: //power(right)
            cycleAnimationType();
            settingsChanged = true; //update eeprom
            break;
          case IR_PWR: //power(left)
            mirolo.DSP.pwr = !mirolo.DSP.pwr;
            settingsChanged = true; //update eeprom
            break;
          case IR_BACK: //back
            asm volatile ("  jmp 0");     // reboot
            break;
          case IR_UP: //up
            adjustBrightness(uint8_t(mirolo.DSP.brightness + 1));
            settingsChanged = true; //update eeprom
            break;
          case IR_DOWN: //down
            adjustBrightness(uint8_t(mirolo.DSP.brightness - 1));
            settingsChanged = true; //update eeprom
            break;
          case IR_LEFT: //left
            adjustScrollDelay(uint8_t(mirolo.DSP.scrolldelay0 - 5));
            settingsChanged = true; //update eeprom
            break;
          case IR_RIGHT: //right
            adjustScrollDelay(uint8_t(mirolo.DSP.scrolldelay0 + 5));
            settingsChanged = true; //update eeprom
            break;
          case IR_MOUSE: //mouse
            adjustBrightness(16);
            settingsChanged = true; //update eeprom
            break;
          case IR_HOME: //home
            cycleRoom();
            settingsChanged = true; //update eeprom
            break;
          case IR_1: //1
            modifyStatus(1);
            settingsChanged = true; //update eeprom
            break;
          case IR_2: //2
            modifyStatus(2);
            settingsChanged = true; //update eeprom
            break;
          case IR_3: //3
            modifyStatus(3);
            settingsChanged = true; //update eeprom
            break;
          case IR_4: //4
            modifyStatus(4);
            settingsChanged = true; //update eeprom
            break;
          case IR_5: //5
            modifyStatus(5);
            settingsChanged = true; //update eeprom
            break;
          case IR_6: //6
            modifyStatus(6);
            settingsChanged = true; //update eeprom
            break;
          case IR_7: //7
            modifyStatus(7);
            settingsChanged = true; //update eeprom
            break;
          case IR_8: //8
            modifyStatus(8);
            settingsChanged = true; //update eeprom
            break;
          case IR_9: //9
            modifyStatus(9);
            settingsChanged = true; //update eeprom
            break;
          case IR_0: //0
            modifyStatus(0);
            settingsChanged = true; //update eeprom
            break;
          case IR_DEL: //delete
            modifyStatus(-1);
            settingsChanged = true; //update eeprom
            break;
          case IR_MUTE: //mute
            //cycleMode();
            break;
          case IR_MENU: //menu
            cycleMode();
            settingsChanged = true; //update eeprom
            break;
          case IR_SET: //set
            cycleColor();
            settingsChanged = true; //update eeprom
            break;
          case IR_TVIN: //tv in
            setDisplayTest(true); //toggle display test mode
            break;
          case IR_VOLUP: //vol+

            break;
          case IR_VOLDOWN: //vol-

            break;
          case IR_OK: //ok
            updateEEPROM(); //update eeprom immediately
            break;
        }
      }


      //run updater
      if (strcmp_P(session, PSTR("0")) == 0) updateDisplay();

    }
};


schedule scheduler;



#endif
