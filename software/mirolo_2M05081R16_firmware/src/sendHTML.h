//@(#) sendHTML.h

#ifndef SENDHTML_H
#define SENDHTML_H

enum msg_types {ERR, WARN, SUCC, INFO};

enum snr {line0, line1, prefix0, prefix1, colorpick0, animation0, sliderbrightness, sliderscroll, displayname, displaypwr, messages, confirm, seq_nr_count};


//global variables
//DynamicJsonBuffer jsonBuffer(3000);
StaticJsonBuffer<900> jsonBuffer;
const uint16_t msg_size = 900;
char msg[msg_size] = {'\0'};

unsigned long seq_numbers[seq_nr_count] = {0};






//send c-string in flash to client or calculate characters to send
unsigned int Send_PROGMEM(EthernetClient &client, const boolean &transmit, const __FlashStringHelper* p_text) { //send PROGMEM String to client, returns length of String

  if (transmit) {
    client.print(p_text);
  }
  return strlen_P((const char*)p_text);
}



//send c-string to client or calculate characters to send
unsigned int Send_String(EthernetClient &client, const boolean &transmit, const char* p_text) { //send String to client, returns length of String

  if (transmit) {
    client.print(p_text);
  }
  return strlen(p_text);
}


//encode html special characters to prevent cross-site-scripting
uint16_t countHTMLspecialchars(const char * data) {
  uint16_t count = 0;
  for (uint16_t pos = 0; pos <= strlen(data); ++pos) {
    switch (data[pos]) {
      case '&':  count += 5;  break;
      case '\"': count += 6;  break;
      case '\'': count += 6;  break;
      case '<':  count += 4;  break;
      case '>':  count += 4;  break;
      default:   count++;   break;
    }
  }
  return count;
}

char * encodeHTMLspecialchars(const char * data) {
  static char * encoded = NULL;

  if (encoded) {
    delete [] encoded;
    encoded = NULL;
  }
  encoded = new char[countHTMLspecialchars(data)];
  encoded[0] = '\0';

  for (uint16_t pos = 0; pos <= strlen(data); ++pos) {
    switch (data[pos]) {
      case '&':  strcat_P(encoded, PSTR("&amp;"));          break;
      case '\"': strcat_P(encoded, PSTR("&quot;"));         break;
      case '\'': strcat_P(encoded, PSTR("&apos;"));         break;
      case '<':  strcat_P(encoded, PSTR("&lt;"));           break;
      case '>':  strcat_P(encoded, PSTR("&gt;"));           break;
      default:   strncat(encoded, &data[pos], 1);   break;
    }
  }
  return encoded;
}





//generate and send the HTML index page or calculate its character size before sending
unsigned int generateHTMLindex(EthernetClient &client, const boolean &transmit, char * contentType, uint16_t * statusCode) {
  const uint16_t count_init = __COUNTER__;
  static uint16_t line = count_init;
  unsigned int content_length = 0;

  line++;

  //HEAD
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<!DOCTYPE html>\n<html>\n<head>\n<meta charset=\"utf-8\">\n<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" >\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<title id=\"title\">connecting | mirolo</title>\n<style>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("body {background-color: #202935;font-family: Cantarell, Arial;}\n"));



  //CSS
  /*top navigation bar*/
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".topnav {position: relative;width: 100%;background-color: #2a303d;box-shadow: 0 0.2em 0.3em 0 rgba(0,0,0,0.2);overflow: hidden;font-size: 30pt;white-space: nowrap;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".topnav-dspinfo {width: 100%;background-color: #2a303d;overflow: hidden;font-size: 14pt;display: none;cursor:default;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".topnav-dspinfo a, .topnav-dspinfo span{color: #f2f2f2;text-align: center;padding: 0.4em 1.5em;text-decoration: none;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".topnav-dspinfo a{color: #6aa7e0;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".topnav-dspinfo p{margin-bottom: 2em;}\n"));
  /* Style the links/text inside the navigation bar */
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".topnav a, .topnav span {float: left;color: #f2f2f2;text-align: center;padding: 0.3em 0.8em;text-decoration: none;cursor:pointer;}\n"));
  /* Change the color of links on hover */
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".topnav a:hover {background-color: #323f5a;}\n"));
  /* Add a color to the home link */
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".topnav a.home, .topnav span.home{background-color: #466ec8;padding-right: 0.2em;color: white;cursor:default;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".topnav a.home-right, .topnav span.home-right {background-color: #466ec8;padding-left: 0.2em;color: white;cursor:text;}\n"));
  /* Add a color to the active/current link */
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".topnav a.active {background-color: #323f5a;}\n"));
  /*toggle switch*/
  /* The switch - the box around the slider */
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".switch {position: relative;display: inline-block;width: 1.7em;height: 1em;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".switchfloatright {position: relative;float: right;margin-top: 0.5em;margin-right: 1em;}\n"));
  /* Hide default HTML checkbox */
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".switch input {display:none;}\n"));
  /* The slider */
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".toggleslider {position: absolute;cursor: pointer;top: 0;left: 0;right: 0;bottom: 0;background-color: #7e7e7e;border-radius: 1em;-webkit-transition: .2s;transition: .2s;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".toggleslider:before {position: absolute;content: \"\";height: 0.8em;width: 0.8em;left: 0.1em;bottom: 0.1em;background-color: white;border-radius: 50%;-webkit-transition: .2s;transition: .2s;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("input:checked + .toggleslider {background-color: #466ec8;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("input:checked + .toggleslider:before {-webkit-transform: translateX(0.7em);-ms-transform: translateX(0.7em);transform: translateX(0.7em);}\n"));
  /* Slider */
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".slidecontainer {float: left;position: relative;width: 60%;margin-bottom: 0.5em;}\n"));
  /* The slider itself */
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".slider {-webkit-appearance: none;width: 100%;height: 0.8em;border-radius: 1em;  background: #d3d3d3;outline: none;opacity: 0.8;-webkit-transition: .2s;transition: opacity .2s;}\n"));
  /* Mouse-over effects */
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".slider:hover {opacity: 1; /* Fully shown on mouse-over */}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".slider::-webkit-slider-thumb {-webkit-appearance: none;width: 2em;height: 2em;border-radius: 50%; background: #2a303d;cursor: pointer;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".slider::-moz-range-thumb {width: 2em;height: 2em;border-radius: 50%;background: #2a303d;cursor: pointer;}\n"));
  /*textbox*/
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".textbox {background-color: #2a303d;box-shadow: 0 0.2em 0.3em 0 rgba(0,0,0,0.2);color: white;padding: 1em;font-size: 16pt;margin-bottom: 1em;border: none;cursor:default;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".textbox-slider {background-color: #466ec8;font-size: 14pt;text-align: right;width: 40%;margin: 0.3em 0.3em 0.3em 1.7em;display: inline-block;}\n"));
  /*standard div box*/
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".contentbox {position: relative;float: left;width: 57%;min-height: 12em;background-color: #44546a;box-shadow: 0 0.2em 0.3em 0 rgba(0,0,0,0.2);padding: 0.5em 0.5em;margin: 1em 0.5em 1em;display: inline-block;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".contentbox-right {width: 37%;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".show {display: inline-block;}\n"));
  /*ws2812 ring*/
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".dottedring {position: relative;border-radius: 50%;width: 15em;height: 15em;margin-top: 2em;display: inline-block;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".dottedring-back {   position: absolute !important;box-shadow: 0 0 0.4em 0.4em rgba(0,0,0,0.3);border: 2em solid #2a303d;border-radius: 50%;width: 9.2em;height: 9.2em;z-index: 1;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".dottedring-ring {position: absolute !important;margin-top: 0.3em;margin-left: 0.3em;border: 1.3em dotted #3498db;border-radius: 50%;width: 10em;height: 10em;opacity: 1;z-index: 2;cursor: pointer;}\n"));
  /*color picker*/
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("input[type=\"color\"] {display: none;}\n"));
  /*info notification*/
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".error {background-color: #ffdddd;border-left: 0.3em solid #f44336;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".success {background-color: #ddffdd;border-left: 0.3em solid #4CAF50;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".info {background-color: #e7f3fe;border-left: 0.3em solid #2196F3;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".warning {background-color: #ffffcc;border-left: 0.3em solid #ffeb3b;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".system {background-color: #ddccff;border-left: 0.3em solid #9b3bff;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".messagediv{margin: 1em;padding: 0.1em 0.3em;font-size: 18pt;cursor:default;}\n"));
  /* text input boxes*/
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".textline{width: 90%;margin: 3em 1em;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".textline input {width: 100%;padding: 0.5em 0 0.5em;margin: 0.2em;font-size: 18pt;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".textline-info{border-left: 0.5em dotted #f2f2f2;width: 50%;margin-right: 4em;margin-top: 8em;float: left;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".textline-message{border-left: 0.5em dotted #f44336;}\n"));
  /* Mark input boxes that gets an error on validation: */
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".textline input.invalid {background-color: #ffdddd;}\n"));
  /* Confirm Button */
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".confirmbtn{color: white;box-shadow: 0 0.2em 0.3em 0 rgba(0,0,0,0.2);padding: 1em;font-size: 16pt;margin: 0.3em;border: none;cursor: pointer;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".apply .confirmbtn{background-color: #62ac39;position: relative;float: right;display: inline-block;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".cancel .confirmbtn{background-color: #ac393e;position: relative;float: right;display: inline-block;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".disable .confirmbtn{background-color: #747474;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".apply:hover .confirmbtn{background-color: #427427;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".cancel:hover .confirmbtn{background-color: #742730;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".disable:hover .confirmbtn{background-color: #747474;}\n"));
  /* Dropdown Button */
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".dropbtn {background-color: #466ec8;box-shadow: 0 0.2em 0.3em 0 rgba(0,0,0,0.2);color: white;padding: 1em;font-size: 14pt;margin: 0.3em;border: none;cursor: pointer;}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".dropbtn-round {position: absolute;margin-top: 1.3em;margin-left: 2.2em;height: 7em;width: 7em;border-radius: 50%;z-index: 3;}\n"));
  /* The container <div> - needed to position the dropdown content */
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".dropdown {position: relative;display: inline-block;}\n"));
  /* Dropdown Content (Hidden by Default) */
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".dropdown-content {display: none;position: absolute;background-color: #f9f9f9;left: 10em;top: -2em;min-width: 8em;box-shadow: 0 0.2em 0.3em 0 rgba(0,0,0,0.2);z-index: 4;}\n"));
  /* Links inside the dropdown */
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".dropdown-content a {color: black;padding: 0.4em 0.8em;text-decoration: none;display: block;cursor: pointer;}\n"));
  /* Change color of dropdown links on hover */
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".dropdown-content a:hover {background-color: #e1e1e1 !important}\n"));
  /* Show the dropdown menu on hover */
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".dropdown:hover .dropdown-content {display: block;}\n"));
  /* Change the background color of the dropdown button when the dropdown content is shown */
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".dropdown:hover .dropbtn {background-color: #36559a;}\n"));
  /* small screens */
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("@media only screen and (max-width:1250px) {.contentbox{width:97%;}}\n"));



  //HEAD
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</style>\n</head>\n<body>\n"));

  //HTML
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"topnav\" id=\"siteTopnav\">\n"));

  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a class=\"home\"><strong>mirolo | </strong></a>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<span class=\"home-right\" id=\"displayname\" "));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("contenteditable "));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(">"));

  if (!transmit || line == __COUNTER__)content_length += Send_String(client, transmit, encodeHTMLspecialchars(mirolo_active.Name));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</span>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a id=\"dspInfoBtn\" onclick=\"toggleDSPinfo()\">Display Info</a>\n"));
  if ((!authOK || !sessionOK) && strcmp_P(username, PSTR("")) != 0)if (!transmit || line == __COUNTER__) {
      content_length += Send_PROGMEM(client, transmit, F("<a id=\"dspLoginBtn\" href=\"login\">In use("));
      content_length += Send_String(client, transmit, username);
      content_length += Send_PROGMEM(client, transmit, F(")</a>\n"));
    }
  if ((!authOK || !sessionOK) && strcmp_P(username, PSTR("")) == 0)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a id=\"dspLoginBtn\" href=\"login\">Login</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__) {
      content_length += Send_PROGMEM(client, transmit, F("<a id=\"dspLoginBtn\" href=\"logout\">Logout("));
      content_length += Send_String(client, transmit, encodeHTMLspecialchars(username));
      content_length += Send_PROGMEM(client, transmit, F(")</a>\n"));
    }

  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"switchfloatright\">\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<label class=\"switch\">\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<input type=\"checkbox\" "));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("disabled "));
  if (!transmit || line == __COUNTER__) {
    if (mirolo_active.DSP.pwr == true)
      content_length += Send_PROGMEM(client, transmit, F("checked "));
  }
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("id=\"displaypwr\" onchange=\"togglePWR(this)\">\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<span class=\"toggleslider round\"></span>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</label>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));

  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));

  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"topnav-dspinfo\" id=\"dspInfo\">\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<p>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<span><strong>Hardware </strong>"));
  if (!transmit || line == __COUNTER__)content_length += Send_String(client, transmit, mirolo_active.HW);
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</span>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<span><strong>CPU </strong>"));
  if (!transmit || line == __COUNTER__)content_length += Send_String(client, transmit, mirolo_active.CPU);
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</span>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<span><strong>SNR </strong>"));
  if (!transmit || line == __COUNTER__)content_length += Send_String(client, transmit, mirolo_active.SNR);
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</span>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<span><strong>Firmware </strong>"));
  if (!transmit || line == __COUNTER__)content_length += Send_String(client, transmit, mirolo_active.FW);
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</span>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<span><strong>MAC </strong>"));
  if (!transmit || line == __COUNTER__)content_length += Send_String(client, transmit, mirolo_active.MAC);
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</span>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<span><strong>IP </strong>"));
  if (!transmit || line == __COUNTER__)content_length += Send_String(client, transmit, mirolo_active.IP);
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</span>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</p>\n"));

  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<p>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<span><strong>Simon Junga</strong><a target=\"_blank\" href=\"http://www.telegram.me/theolyn\">@theolyn</a></span>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a style=float:right target=\"_blank\" href=\"https://gitlab.com/mirolo-info-displays/2m05081r16/issues\">Report a Bug</a>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</p>\n"));

  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));


  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"contentbox\">\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"textbox\">Display Settings</div>\n"));


  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"textline textline-info\">\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<input "));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("disabled "));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("value=\""));
  if (!transmit || line == __COUNTER__)content_length += Send_String(client, transmit, encodeHTMLspecialchars(mirolo_active.DSP.message0));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("\" maxlength=\"280\" placeholder=\"info line\" id=\"line0\" oninput=\"sendline(this)\">\n"));

  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"dropdown\">\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<button class=\"dropbtn\">Prefix Characters <strong><span id=\"prefix0\">"));
  if (!transmit || line == __COUNTER__)content_length += Send_String(client, transmit, encodeHTMLspecialchars(mirolo_active.DSP.prefix0));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</span></strong></button>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"dropdown-content\">\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange(this, span_prefix0)\">off</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange(this, span_prefix0)\">1</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange(this, span_prefix0)\">2</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange(this, span_prefix0)\">3</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange(this, span_prefix0)\">4</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange(this, span_prefix0)\">5</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange(this, span_prefix0)\">6</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange(this, span_prefix0)\">7</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));

  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));

  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"dottedring\">\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"dottedring-back\"></div>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"dropdown dottedring-ring\" id=\"ws28ring\">\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"dropdown-content\">\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"activateColorpick0()\">Color Picker</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a style=\"color:#ffffff; background-color:#eeeeee;\" onclick=\"colorpickSet0(this)\">⬤ ffffff</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a style=\"color:#ff0000; background-color:#eeeeee;\" onclick=\"colorpickSet0(this)\">⬤ ff0000</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a style=\"color:#00ff00; background-color:#eeeeee;\" onclick=\"colorpickSet0(this)\">⬤ 00ff00</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a style=\"color:#0000ff; background-color:#eeeeee;\" onclick=\"colorpickSet0(this)\">⬤ 0000ff</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a style=\"color:#ffb000; background-color:#eeeeee;\" onclick=\"colorpickSet0(this)\">⬤ ffb000</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a style=\"color:#ffff00; background-color:#eeeeee;\" onclick=\"colorpickSet0(this)\">⬤ ffff00</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a style=\"color:#00ffff; background-color:#eeeeee;\" onclick=\"colorpickSet0(this)\">⬤ 00ffff</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a style=\"color:#ff00ff; background-color:#eeeeee;\" onclick=\"colorpickSet0(this)\">⬤ ff00ff</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"dropdown\">\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<button "));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("disabled "));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(" class=\"dropbtn dropbtn-round\"><strong id=\"animation0\">"));
  if (!transmit || line == __COUNTER__)content_length += Send_String(client, transmit, encodeHTMLspecialchars(mirolo_active.DSP.animation));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</strong></button>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"dropdown-content\">\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange(this, span_animation0)\">off</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange(this, span_animation0)\">on</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange(this, span_animation0)\">blink (1Hz)</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange(this, span_animation0)\">pulse</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange(this, span_animation0)\">loading</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<input type=\"color\" value=\"#"));
  if (!transmit || line == __COUNTER__) {
    char tmp[16];
    sprintf(tmp, "%.6lX", mirolo_active.DSP.color);
    content_length += Send_String(client, transmit, tmp);
  }
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("\" id=\"colorpick0\" oninput=\"colorpickInput0()\" onchange=\"colorpickChange0()\">\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));



  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"textline textline-message\">\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<input "));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("disabled "));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("value=\""));
  if (!transmit || line == __COUNTER__)content_length += Send_String(client, transmit, encodeHTMLspecialchars(mirolo_active.DSP.message1));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("\" maxlength=\"280\" placeholder=\"message line\" id=\"line1\" oninput=\"sendline(this)\">\n"));

  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"dropdown\">\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<button class=\"dropbtn\">Prefix Characters <strong><span id=\"prefix1\">"));
  if (!transmit || line == __COUNTER__)content_length += Send_String(client, transmit, encodeHTMLspecialchars(mirolo_active.DSP.prefix1));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</span></strong></button>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"dropdown-content\">\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange(this, span_prefix1)\">off</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange(this, span_prefix1)\">1</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange(this, span_prefix1)\">2</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange(this, span_prefix1)\">3</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange(this, span_prefix1)\">4</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange(this, span_prefix1)\">5</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange(this, span_prefix1)\">6</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange(this, span_prefix1)\">7</a>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));

  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));


  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"textbox textbox-slider\">\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"slidecontainer\">\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<input "));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("disabled "));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("type=\"range\" min=\"0\" max=\"16\" value=\""));
  if (!transmit || line == __COUNTER__) {
    char tmp[16];
    content_length += Send_String(client, transmit, itoa(mirolo_active.DSP.brightness, tmp, 10));
  }
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("\" class=\"slider\" id=\"sliderbrightness\" oninput=\"sliderBrightnessMoved(true)\">\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<span style=margin-left:0.5em >Brightness </span><strong><span id=\"span_brightness\">8</span></strong>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));

  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"textbox textbox-slider\">\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"slidecontainer\">\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<input "));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("disabled "));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("type=\"range\" min=\"10\" max=\"200\" value=\""));
  if (!transmit || line == __COUNTER__) {
    char tmp[16];
    content_length += Send_String(client, transmit, itoa(220 - mirolo_active.DSP.scrolldelay0, tmp, 10));
  }
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("\" class=\"slider\" id=\"sliderscroll\" oninput=\"sliderScrollMoved(true)\">\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<span style=margin-left:0.5em >Scroll </span><strong><span id=\"span_scroll\">medium</span></strong>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));

  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"apply\" id=\"confdiv\">\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<button class=\"confirmbtn\" id=\"confirm\" onclick=\"settingsApply(this)\">Apply</button>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));

  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"cancel\">\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<button class=\"confirmbtn\" onclick=\"location.href='';\">Cancel</button>\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));


  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));



  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"contentbox contentbox-right\">\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"textbox\">System Messages</div>\n"));


  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div id=\"messagebox\">\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"messagediv success\">\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<p><strong>Success!</strong> Display ready</p>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));



  //JAVASCRIPT
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<script>\n"));

  /* global sequence number*/
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("var seq_number = 0;\n"));
  /*AJAX request timers*/
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("var timers = [];\n\n"));


  /* When the user clicks on the button,
    toggle between hiding and showing the dropdown content */
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function toggleDSPinfo() {\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("document.getElementById(\"dspInfo\").classList.toggle(\"show\");\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("document.getElementById(\"dspInfoBtn\").classList.toggle(\"active\");}\n\n"));

  /*color picker*/
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("var color_picker0 = document.getElementById(\"colorpick0\");\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("var color_ws28ring = document.getElementById(\"ws28ring\");\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("color_ws28ring.style.borderColor = color_picker0.value;\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function colorpickInput0() {\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("color_ws28ring.style.borderColor = color_picker0.value;}\n"));

  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function colorpickChange0(){\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("refreshMessages();\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("sendAJAXrequest(color_picker0.id + \"=\" + color_picker0.value, function() {\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if (this.readyState == 4 && this.status == 200) {\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("updateMessages(true);}}, color_picker0.id, 50);}\n\n"));

  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function colorpickSet0(element){\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("color_picker0.value = \"#\" + element.textContent.substring(element.textContent.indexOf(\" \")+1);\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("colorpickInput0();\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("colorpickChange0();}\n\n"));

  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function activateColorpick0(){\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("color_picker0.click();}\n\n"));

  /*brightness control*/
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("var slider_brightness = document.getElementById(\"sliderbrightness\");\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("var span_brightness = document.getElementById(\"span_brightness\");\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function sliderBrightnessMoved(update) {\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(slider_brightness.value == 16) span_brightness.textContent = \"auto\";\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("else span_brightness.textContent = (10 + (slider_brightness.value)*(96/16)) + \"%\";\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(update == true){\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("refreshMessages();\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("sendAJAXrequest(slider_brightness.id + \"=\" + slider_brightness.value, function() {\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if (this.readyState == 4 && this.status == 200) {\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("updateMessages(true);}\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("}, slider_brightness.id, 1000);}"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("sliderBrightnessMoved(true);\n\n"));

  /*scroll speed control*/
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("var slider_scroll = document.getElementById(\"sliderscroll\");\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("var span_scroll = document.getElementById(\"span_scroll\");\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function sliderScrollMoved(update){\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("var speed = 220 - slider_scroll.value;\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(speed < 40)span_scroll.textContent = \"very fast\";\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("else if(speed < 80)span_scroll.textContent = \"fast\";\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("else if(speed < 120)span_scroll.textContent = \"medium\";\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("else if(speed < 160)span_scroll.textContent = \"slow\";\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("else if(speed <= 200)span_scroll.textContent = \"very slow\";\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(update == true){\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("refreshMessages();  \n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("sendAJAXrequest(slider_scroll.id + \"=\" + speed, function() {\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if (this.readyState == 4 && this.status == 200) {\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("updateMessages(true);}\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("}, slider_scroll.id, 1000);}"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("sliderScrollMoved(true);\n\n"));

  /*prefix, animation*/
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("var span_prefix0 = document.getElementById(\"prefix0\");\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("var span_prefix1 = document.getElementById(\"prefix1\");\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("var span_animation0 = document.getElementById(\"animation0\");\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function dropdownChange(element, span) {\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("refreshMessages();\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("span.textContent = element.textContent;\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("sendAJAXrequest(span.id + \"=\" + span.textContent, function() {\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if (this.readyState == 4 && this.status == 200) {\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("updateMessages(true);}}, element.id, 50);}\n\n"));

  /*Edit Display Name*/
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("var span_panelname = document.getElementById('displayname');\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("var title = document.getElementById('title');\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("span_panelname.addEventListener('keypress', function(evt) {\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(evt.keyCode == 8 || evt.keyCode == 46);\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("else if (evt.keyCode == 13 || span_panelname.textContent.length > 20) {\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("evt.preventDefault();}});\n\n"));

  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("span_panelname.addEventListener('keyup', function(evt) {\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("updateTitle();\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("sendAJAXrequest(span_panelname.id + \"=\" + span_panelname.textContent, null, span_panelname.id, 2000);});\n\n"));

  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function updateTitle(){\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("title.textContent = span_panelname.textContent + \" | mirolo\";}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("updateTitle();\n\n"));


  /*functions*/
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function sendline(element) {\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("refreshMessages();\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("sendAJAXrequest(element.id + \"=\" + element.value, function() {\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if (this.readyState == 4 && this.status == 200) {\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("updateMessages(true);}}, element.id, 1000);}\n\n"));

  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function togglePWR(element) {\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("refreshMessages();\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("sendAJAXrequest(element.id + \"=\" + element.checked, function() {\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if (this.readyState == 4 && this.status == 200) {\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("updateMessages(true);}}, element.id, 500);}\n\n"));

  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function settingsApply(element) {\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("refreshMessages();\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("let msg = element.id + \"=\" + element.textContent;\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("sendAJAXrequest(msg, function() {\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if (this.readyState == 4 && this.status == 200) {\n"));
  if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("updateMessages(true);}}, element.id, 200);}\n\n"));

  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function updateMessages(delete_msg) {\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("sendAJAXrequest(\"messages=update\", function() {\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if (this.readyState == 4 && this.status == 200) {\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("var obj = JSON.parse(this.responseText);\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(obj.messages){\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(delete_msg == true) deleteMessages();\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("for (var i=0; i<obj.messages.length; i++){\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("insertMessage(obj.messages[i].type, obj.messages[i].text)};\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("try{document.getElementById(\"confirm\").disabled = false;\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("document.getElementById(\"confdiv\").classList.remove(\"disable\");}catch(e){}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("}}}, \"messages\", 1000);}\n\n"));

  /*messages*/
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function insertMessage(type, text){\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("var str = document.createElement(\"STRONG\");\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(type == \"error\")str.textContent = \"Error \";\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("else if(type == \"warning\")str.textContent = \"Warning \";\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("else if(type == \"success\")str.textContent = \"Success \";\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("else if(type == \"system\")str.textContent = \"System \";\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("else str.textContent = \"Info \";\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("var p = document.createElement(\"P\");\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("p.appendChild(str);\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("p.appendChild(document.createTextNode(text));\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("var div = document.createElement(\"DIV\");\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("div.classList.add(\"messagediv\");\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("div.classList.add(type);\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("div.appendChild(p);\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("var box = document.getElementById(\"messagebox\");\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("box.appendChild(div);}\n\n"));

  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function deleteMessages(){\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("var box = document.getElementById(\"messagebox\");\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("while (box.firstChild) box.removeChild(box.firstChild);}\n\n"));

  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function refreshMessages(){\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("try{document.getElementById(\"confirm\").disabled = true;\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("document.getElementById(\"confdiv\").classList.add(\"disable\");}catch(e){}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("deleteMessages();\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("insertMessage(\"system\", \"refreshing...\");}\n\n"));

  /*AJAX request*/
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function sendAJAXrequest(content, callback, id, delay){\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("callback = callback || null;\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(timers[id])clearTimeout(timers[id]);\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("timers[id] = setTimeout(function() {\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("seq_number++;\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("var xhttp = new XMLHttpRequest();\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(callback)xhttp.onreadystatechange = callback;\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("xhttp.open(\"POST\", \"mirolo.ajax\", true);\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("xhttp.send(encodeURIComponent(\"seq_number=\" + seq_number + \"&\" + content));\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("}, delay);}\n\n"));


  /*AJAX site update | heartbeat*/
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function enableAJAXupdate(id, delay){\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(timers[id])clearInterval(timers[id]);\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("timers[id] = setInterval(function() {\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("var xhttp = new XMLHttpRequest();\n"));

  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("xhttp.onreadystatechange = function() {\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if (this.readyState == 4 && this.status == 200) {\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("var obj = JSON.parse(this.responseText);\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(obj){\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("span_panelname.textContent = obj.displayname;\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("updateTitle();\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("document.getElementById(\"line0\").value = obj.line0;\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("span_prefix0.textContent = obj.prefix0;\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("document.getElementById(\"line1\").value = obj.line1;\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("span_prefix1.textContent = obj.prefix1;\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("slider_scroll.value = 220 - obj.scrolldelay0;\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("sliderScrollMoved(false);\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("slider_brightness.value = obj.brightness;\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("sliderBrightnessMoved(false);\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("color_ws28ring.style.borderColor = obj.ws28ring;\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("span_animation0.textContent = obj.animation0;\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("document.getElementById(\"displaypwr\").checked = obj.displaypwr;\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(obj.username != \"\"){\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("document.getElementById(\"dspLoginBtn\").textContent = \"In use(\" + obj.username + \")\";\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("deleteMessages();\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("insertMessage(\"system\", \"display managed by \" + obj.username + \" (read only mode)\");\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("updateMessages(false);}\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("else {\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("document.getElementById(\"dspLoginBtn\").textContent = \"Login\";\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("updateMessages(true);\n"));
  if (!authOK || !sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("}}}};\n\n"));

  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("xhttp.open(\"POST\", \"mirolo.ajax\", true);\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("xhttp.send(encodeURIComponent(\""));
  if (!authOK || !sessionOK) {
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("site=update"));
  }
  else if (authOK && sessionOK)if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("site=heartbeat"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("\"));\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("}, delay);}\n"));
  if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("enableAJAXupdate(\"siteupdate\", 10000);\n\n"));

  if (!transmit || line == __COUNTER__) {
    content_length += Send_PROGMEM(client, transmit, F("</script>\n</body>\n</html>\n\n"));
    line = count_init;
  }

  strcpy_P(contentType, PSTR("text/html"));
  *statusCode = 200;
  return content_length;
}



//build JSON response "messages"
void buildJSONmsg(char * msg, const uint16_t &msg_size) {
  jsonBuffer.clear();
  JsonObject& json = jsonBuffer.createObject();
  JsonArray& messages = json.createNestedArray(F("messages"));

  boolean settings_apply = false;
  if (memcmp( &mirolo_active, &mirolo, sizeof(miroloHW) ) != 0) {
    settings_apply = false;
  }
  else settings_apply = true;

  for (int type = ERR; type <= INFO; type++) {
    if (type == WARN && mirolo.DSP.pwr == false) {
      JsonObject& message = messages.createNestedObject();
      message[F("type")] = F("warning");
      message[F("text")] = F("display is disabled");
    }

    if (type == SUCC && settings_apply) {
      JsonObject& message = messages.createNestedObject();
      message[F("type")] = F("success");
      message[F("text")] = F("settings applied");
    }
    else if (type == WARN && !settings_apply) {
      JsonObject& message = messages.createNestedObject();
      message[F("type")] = F("warning");
      message[F("text")] = F("new settings have not been applied");
    }

    uint8_t divider = atoi(mirolo.DSP.prefix0);
    if (type == INFO && divider > 0 && divider >= strlen(mirolo.DSP.message0)) {
      JsonObject& message = messages.createNestedObject();
      message[F("type")] = F("info");
      message[F("text")] = F("prefix on info line is longer or equal to its message");
    }
    char * static_msg;
    static_msg = new char[divider];
    strncpy (static_msg, mirolo.DSP.message0 , divider);
    static_msg[divider] = '\0'; //terminate string
    if (type == WARN && !staticDivisionOK(mirolo.DSP.message0, atoi(mirolo.DSP.prefix0), max_Line[0].mx)) {
      JsonObject& message = messages.createNestedObject();
      message[F("type")] = F("warning");
      message[F("text")] = F("prefix on info line can't be applied");
    }
    if (type == INFO && (isScrolling(mirolo.DSP.message0, max_Line[0].mx) || (divider > 0 && strlen(mirolo.DSP.message0) - strlen(static_msg) > 0))) {
      JsonObject& message = messages.createNestedObject();
      message[F("type")] = F("info");
      message[F("text")] = F("info line is scrolling");
    }
    if (type == ERR && !checkFont(mirolo.DSP.message0, max_Line[0].mx)) {
      JsonObject& message = messages.createNestedObject();
      message[F("type")] = F("error");
      message[F("text")] = F("info line contains invalid characters");
    }
    delete [] static_msg;


    divider = atoi(mirolo.DSP.prefix1);
    if (type == INFO && divider > 0 && divider >= strlen(mirolo.DSP.message1)) {
      JsonObject& message = messages.createNestedObject();
      message[F("type")] = F("info");
      message[F("text")] = F("prefix on message line is longer or equal to its message");
    }
    static_msg = new char[divider];
    strncpy (static_msg, mirolo.DSP.message1 , divider);
    static_msg[divider] = '\0'; //terminate string
    if (type == WARN && !staticDivisionOK(mirolo.DSP.message1, atoi(mirolo.DSP.prefix1), max_Line[1].mx)) {
      JsonObject& message = messages.createNestedObject();
      message[F("type")] = F("warning");
      message[F("text")] = F("prefix on message line can't be applied");
    }
    if (type == INFO && (isScrolling(mirolo.DSP.message1, max_Line[1].mx) || (divider > 0 && strlen(mirolo.DSP.message1) - strlen(static_msg) > 0))) {
      JsonObject& message = messages.createNestedObject();
      message[F("type")] = F("info");
      message[F("text")] = F("message line is scrolling");
    }
    if (type == ERR && !checkFont(mirolo.DSP.message1, max_Line[1].mx)) {
      JsonObject& message = messages.createNestedObject();
      message[F("type")] = F("error");
      message[F("text")] = F("message line contains invalid characters");
    }
    delete [] static_msg;



    if (type == WARN && mirolo.DSP.color == 0 && strcmp_P(mirolo.DSP.animation, PSTR("off")) != 0) {
      JsonObject& message = messages.createNestedObject();
      message[F("type")] = F("warning");
      message[F("text")] = F("RGB animation is selected but the color is set to black");
    }
  }
  json.printTo(msg, msg_size);
}


//build JSON response "siteupdate"
void buildJSONsiteupdate(char * msg, const uint16_t &msg_size) {
  jsonBuffer.clear();
  JsonObject& json = jsonBuffer.createObject();

  json[F("displayname")] = mirolo.Name;
  json[F("line0")] = mirolo.DSP.message0;   //line0
  json[F("prefix0")] = mirolo.DSP.prefix0;
  json[F("scrolldelay0")] = mirolo.DSP.scrolldelay0;
  json[F("line1")] = mirolo.DSP.message1;   //line1
  json[F("prefix1")] = mirolo.DSP.prefix1;
  json[F("scrolldelay1")] = mirolo.DSP.scrolldelay1;

  json[F("brightness")] = mirolo.DSP.brightness;
  char tmp[16];
  sprintf(tmp, "#%.6lX", mirolo.DSP.color); //convert uint32_t color to html compatible #HEX notation
  json[F("ws28ring")] = tmp;
  json[F("animation0")] = mirolo.DSP.animation;
  json[F("displaypwr")] = mirolo.DSP.pwr;
  json[F("username")] = username;

  json.printTo(msg, msg_size);
}



//generate and send ajax reply or calculate its character size before sending
unsigned int generateAJAXreply(EthernetClient &client, const boolean &transmit, char * contentType, uint16_t * statusCode) {
  //const uint16_t count_init = __COUNTER__;
  //static uint16_t line = count_init;
  unsigned int content_length = 0;

  static boolean create_siteupdate = false;
  static boolean create_msg = false;

  //line++;

  //decode client message
  if (!transmit)urldecode(recv_content);

  unsigned long seq_number = 0;

  char id[50];
  char * id_ptr = id;
  char value[300];
  char * value_ptr = value;
  char * cont_ptr = recv_content;

  //get id and value pairs from message
  while (*cont_ptr != '\0' && !transmit) {

    id_ptr = id;
    value_ptr = value;
    uint16_t counter = 0;

    //get id
    while (counter < 49 && *cont_ptr != '=') {
      *id_ptr = *cont_ptr;
      id_ptr++;
      cont_ptr++;
      counter++;
    }
    *id_ptr = '\0';
    cont_ptr++;
    counter = 0;

    //get value
    while (counter < 299 && *cont_ptr != '&' && *cont_ptr != '\0') {
      *value_ptr = *cont_ptr;
      value_ptr++;
      cont_ptr++;
      counter++;
    }
    *value_ptr = '\0';
    if (*cont_ptr == '&') cont_ptr++;


    //evaluate data
    //no authorization required
    if (strcmp_P(id, PSTR("site")) == 0 && strcmp_P(value, PSTR("update")) == 0 ) {
      create_siteupdate = true;
    }
    else if ((!authOK || !sessionOK) && strcmp_P(id, PSTR("messages")) == 0 && strcmp_P(value, PSTR("update")) == 0) {

      create_msg = true;
    }

    //authentification required (make sure user is authentificated to make changes to the display)
    else if (authOK && sessionOK && strcmp_P(id, PSTR("seq_number")) == 0) {
      seq_number = strtoul (value, NULL, 10);
    }
    else if (authOK && sessionOK && seq_number != 0) {

      if (strcmp_P(id, PSTR("line0")) == 0 && seq_numbers[line0] < seq_number) {
        if (strlen(value) < max_bufsize) {
          memset(mirolo.DSP.message0, 0, max_bufsize);
          strcpy(mirolo.DSP.message0, value);
        }
        seq_numbers[line0] = seq_number;
      }

      else if (strcmp_P(id, PSTR("line1")) == 0 && seq_numbers[line1] < seq_number) {
        if (strlen(value) < max_bufsize) {
          memset(mirolo.DSP.message1, 0, max_bufsize);
          strcpy(mirolo.DSP.message1, value);
        }
        seq_numbers[line1] = seq_number;
      }

      else if (strcmp_P(id, PSTR("prefix0")) == 0 && seq_numbers[prefix0] < seq_number) {
        if (strlen(value) < dsp_prefix_size) {
          memset(mirolo.DSP.prefix0, 0, dsp_prefix_size);
          strcpy(mirolo.DSP.prefix0, value);
        }
        seq_numbers[prefix0] = seq_number;
      }

      else if (strcmp_P(id, PSTR("prefix1")) == 0 && seq_numbers[prefix1] < seq_number) {
        if (strlen(value) < dsp_prefix_size) {
          memset(mirolo.DSP.prefix1, 0, dsp_prefix_size);
          strcpy(mirolo.DSP.prefix1, value);
        }
        seq_numbers[prefix1] = seq_number;
      }

      else if (strcmp_P(id, PSTR("colorpick0")) == 0 && seq_numbers[colorpick0] < seq_number) {
        value[0] = '0'; //html notation! override '#'
        uint32_t color = strtoul (value, NULL, 16);
        mirolo.DSP.color = color;
        seq_numbers[colorpick0] = seq_number;
      }

      else if (strcmp_P(id, PSTR("animation0")) == 0 && seq_numbers[animation0] < seq_number) {
        if (strlen(value) < dsp_animation_size) {
          memset(mirolo.DSP.animation, 0, dsp_animation_size);
          strcpy(mirolo.DSP.animation, value);
        }
        seq_numbers[animation0] = seq_number;
      }

      else if (strcmp_P(id, PSTR("sliderbrightness")) == 0 && seq_numbers[sliderbrightness] < seq_number) {
        mirolo.DSP.brightness = constrain((uint8_t)atoi(value), 0, 16);
        seq_numbers[sliderbrightness] = seq_number;
      }

      else if (strcmp_P(id, PSTR("sliderscroll")) == 0 && seq_numbers[sliderscroll] < seq_number) {
        mirolo.DSP.scrolldelay0 = constrain((uint8_t)atoi(value), 20, 200);
        mirolo.DSP.scrolldelay1 = constrain((uint8_t)atoi(value), 20, 200);
        seq_numbers[sliderscroll] = seq_number;
      }

      else if (strcmp_P(id, PSTR("displayname")) == 0 && seq_numbers[displayname] < seq_number) {
        if (strlen(value) < dsp_name_size) {
          memset(mirolo.Name, 0, dsp_name_size);
          strcpy(mirolo.Name, value);
        }
        seq_numbers[displayname] = seq_number;
      }

      else if (strcmp_P(id, PSTR("displaypwr")) == 0 && seq_numbers[displaypwr] < seq_number) {
        if (strcmp_P(value, PSTR("true")) == 0) mirolo.DSP.pwr = true;
        else mirolo.DSP.pwr = false;
        seq_numbers[displaypwr] = seq_number;
      }

      else if (strcmp_P(id, PSTR("messages")) == 0 && strcmp_P(value, PSTR("update")) == 0 && seq_numbers[messages] < seq_number) {

        create_msg = true;
        seq_numbers[messages] = seq_number;
      }

      else if (strcmp_P(id, PSTR("confirm")) == 0 && seq_numbers[confirm] < seq_number) {
        mirolo.DSP.apply_changes = ALL & ~RING0;
        seq_numbers[confirm] = seq_number;
      }
      else {
        *statusCode = 404;
      }

    }
    else {
      *statusCode = 404;
    }

  }


  //create JSON response message if requested
  if (create_msg) {

    if (transmit) create_msg = false;
    else buildJSONmsg(msg, 500);

    content_length += Send_String(client, transmit, msg);
    strcpy_P(contentType, PSTR("application/json"));
    *statusCode = 200;
  }
  else if (create_siteupdate) {

    if (transmit) create_siteupdate = false;
    else buildJSONsiteupdate(msg, 500);

    content_length += Send_String(client, transmit, msg);
    strcpy_P(contentType, PSTR("application/json"));
    *statusCode = 200;
  }

  return content_length;
}


//create json response for line 0
void buildJSONline0(JsonObject & data, uint8_t &method) {
  JsonObject& pending = data.createNestedObject(F("pending"));
  pending[F("message")] = mirolo.DSP.message0;
  pending[F("messageok")] = checkFont(mirolo.DSP.message0, max_Line[0].mx);
  if (strcmp_P(mirolo.DSP.prefix0, PSTR("off")) == 0)pending[F("prefix")] = 0;
  else pending[F("prefix")] = atol(mirolo.DSP.prefix0);
  pending[F("prefixok")] = staticDivisionOK(mirolo.DSP.message0, atoi(mirolo.DSP.prefix0), max_Line[0].mx);
  pending[F("speed")] = mirolo.DSP.scrolldelay0;
  pending[F("scrolling")] = isScrolling(mirolo.DSP.message0, max_Line[0].mx) || isScrollingPrefix(mirolo.DSP.message0, atoi(mirolo.DSP.prefix0));
  pending[F("roundtriptime")] = getMsgRoundtriptime(mirolo.DSP.message0, atoi(mirolo.DSP.prefix0), mirolo.DSP.scrolldelay0, max_Line[0].mx);
  JsonObject& active = data.createNestedObject(F("active"));
  //changes not applied yet. send pseudo updated values
  if (method == POST) {
    active[F("message")] = pending[F("message")];
    active[F("messageok")] = pending[F("messageok")];
    active[F("prefix")] = pending[F("prefix")];
    active[F("prefixok")] = pending[F("prefixok")];
    active[F("speed")] = pending[F("speed")];
    active[F("scrolling")] = pending[F("scrolling")];
    active[F("roundtriptime")] = pending[F("roundtriptime")];
  }
  else {
    active[F("message")] = mirolo_active.DSP.message0;
    active[F("messageok")] = checkFont(mirolo_active.DSP.message0, max_Line[0].mx);
    if (strcmp_P(mirolo_active.DSP.prefix0, PSTR("off")) == 0)active[F("prefix")] = 0;
    else active[F("prefix")] = atol(mirolo_active.DSP.prefix0);
    active[F("prefixok")] = atoi(mirolo_active.DSP.prefix0) == 0 ? true : staticDivisionOK(mirolo_active.DSP.message0, atoi(mirolo_active.DSP.prefix0), max_Line[0].mx);
    active[F("speed")] = mirolo_active.DSP.scrolldelay0;
    active[F("scrolling")] = isScrolling(mirolo_active.DSP.message0, max_Line[0].mx) || isScrollingPrefix(mirolo_active.DSP.message0, atoi(mirolo_active.DSP.prefix0));
    active[F("roundtriptime")] = getMsgRoundtriptime(mirolo_active.DSP.message0, atoi(mirolo_active.DSP.prefix0), mirolo_active.DSP.scrolldelay0, max_Line[0].mx);
  }
}

//create json response for line 1
void buildJSONline1(JsonObject & data, uint8_t &method) {
  JsonObject& pending = data.createNestedObject(F("pending"));
  pending[F("message")] = mirolo.DSP.message1;
  pending[F("messageok")] = checkFont(mirolo.DSP.message1, max_Line[1].mx);
  if (strcmp_P(mirolo.DSP.prefix1, PSTR("off")) == 0)pending[F("prefix")] = 0;
  else pending[F("prefix")] = atol(mirolo.DSP.prefix1);
  pending[F("prefixok")] = staticDivisionOK(mirolo.DSP.message1, atoi(mirolo.DSP.prefix1), max_Line[1].mx);
  pending[F("speed")] = mirolo.DSP.scrolldelay1;
  pending[F("scrolling")] = isScrolling(mirolo.DSP.message1, max_Line[1].mx) || isScrollingPrefix(mirolo.DSP.message1, atoi(mirolo.DSP.prefix1));
  pending[F("roundtriptime")] = getMsgRoundtriptime(mirolo.DSP.message1, atoi(mirolo.DSP.prefix1), mirolo.DSP.scrolldelay1, max_Line[1].mx);
  JsonObject& active = data.createNestedObject(F("active"));
  //changes not applied yet. send pseudo updated values
  if (method == POST) {
    active[F("message")] = pending[F("message")];
    active[F("messageok")] = pending[F("messageok")];
    active[F("prefix")] = pending[F("prefix")];
    active[F("prefixok")] = pending[F("prefixok")];
    active[F("speed")] = pending[F("speed")];
    active[F("scrolling")] = pending[F("scrolling")];
    active[F("roundtriptime")] = pending[F("roundtriptime")];
  }
  else {
    active[F("message")] = mirolo_active.DSP.message1;
    active[F("messageok")] = checkFont(mirolo_active.DSP.message1, max_Line[1].mx);
    if (strcmp_P(mirolo_active.DSP.prefix1, PSTR("off")) == 0)active[F("prefix")] = 0;
    else active[F("prefix")] = atol(mirolo_active.DSP.prefix1);
    active[F("prefixok")] = atoi(mirolo_active.DSP.prefix1) == 0 ? true : staticDivisionOK(mirolo_active.DSP.message1, atoi(mirolo_active.DSP.prefix1), max_Line[1].mx);
    active[F("speed")] = mirolo_active.DSP.scrolldelay1;
    active[F("scrolling")] = isScrolling(mirolo_active.DSP.message1, max_Line[1].mx) || isScrollingPrefix(mirolo_active.DSP.message1, atoi(mirolo_active.DSP.prefix1));
    active[F("roundtriptime")] = getMsgRoundtriptime(mirolo_active.DSP.message1, atoi(mirolo_active.DSP.prefix1), mirolo_active.DSP.scrolldelay1, max_Line[1].mx);
  }
}

//create json response for Ring 0
void buildJSONring0(JsonObject & data, uint8_t &method) {
  JsonObject& pending = data.createNestedObject(F("pending"));
  boolean disabled = false;
  uint32_t roundtriptime = 0;
  if (mirolo_animation.LastFrame == 0) { //check if animation is visible or all BLACK
    disabled = true;
    for (uint16_t i = 0; i < ring.led.numPixels(); i++) {
      if (mirolo_animation.Frames[0].Colors[i] != BLACK) {
        disabled = false;
        break;
      }
    }
  }

  if (disabled == false) { //calculate roundtriptime
    for (uint8_t i = 0; i <= mirolo_animation.LastFrame; i++) {
      roundtriptime += mirolo_animation.Frames[i].Time;
    }
  }
  pending[F("framelength")] = disabled ? 0 : mirolo_animation.LastFrame + 1;
  pending[F("loop")] = mirolo_animation.Loop;
  pending[F("roundtriptime")] = roundtriptime;

  JsonObject& active = data.createNestedObject(F("active"));
  //changes not applied yet. send pseudo updated values
  if (method == POST) {
    active[F("framelength")] = pending[F("framelength")];
    active[F("loop")] = pending[F("loop")];
    active[F("roundtriptime")] = pending[F("roundtriptime")];
  }
  else {
    boolean disabled = false;
    uint32_t roundtriptime = 0;
    if (mirolo_active_animation.LastFrame == 0) { //check if animation is visible or all BLACK
      disabled = true;
      for (uint16_t i = 0; i < ring.led.numPixels(); i++) {
        if (mirolo_active_animation.Frames[0].Colors[i] != BLACK) {
          disabled = false;
          break;
        }
      }
    }

    if (disabled == false) { //calculate roundtriptime
      for (uint8_t i = 0; i <= mirolo_active_animation.LastFrame; i++) {
        roundtriptime += mirolo_active_animation.Frames[i].Time;
      }
    }
    active[F("framelength")] = disabled ? 0 : mirolo_active_animation.LastFrame + 1;
    active[F("loop")] = mirolo_active_animation.Loop;
    active[F("roundtriptime")] = roundtriptime;
  }
}



//generate and send API reply or calculate its character size before sending
unsigned int generateAPIreply(EthernetClient &client, const boolean &transmit, char * contentType, uint16_t * statusCode) {
  unsigned int content_length = 0;

  if (!transmit) {

    //reset
    msg[0] = '\0';


    //create message
    if (method == GET || method == DELETE) {
      //no content. no parsing. no problem

      //routes
      if (strcmp_P(url, PSTR("/api/line0")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        buildJSONline0(data, method);

        *statusCode = 200;
        json[F("status")] = 200;
        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url, PSTR("/api/line1")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        buildJSONline1(data, method);

        *statusCode = 200;
        json[F("status")] = 200;
        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url, PSTR("/api/ring0")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        buildJSONring0(data, method);

        *statusCode = 200;
        json[F("status")] = 200;
        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url, PSTR("/api/brightness")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        JsonObject& pending = data.createNestedObject(F("pending"));
        pending[F("brightness")] = mirolo.DSP.brightness;
        JsonObject& active = data.createNestedObject(F("active"));
        active[F("brightness")] = mirolo_active.DSP.brightness;


        *statusCode = 200;
        json[F("status")] = 200;
        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url, PSTR("/api/ldr")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));
        data[F("ldr")] = lightsensor.value;

        *statusCode = 200;
        json[F("status")] = 200;
        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url, PSTR("/api/name")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));
        data[F("name")] = mirolo_active.Name;

        *statusCode = 200;
        json[F("status")] = 200;
        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url, PSTR("/api/info")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));
        data[F("hardware")] = mirolo_active.HW;
        data[F("processor")] = mirolo_active.CPU;
        data[F("serialnumber")] = mirolo_active.SNR;
        data[F("firmware")] = mirolo_active.FW;
        data[F("ip")] = mirolo_active.IP;
        data[F("mac")] = mirolo_active.MAC;

        *statusCode = 200;
        json[F("status")] = 200;
        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url, PSTR("/api/power")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));
        data[F("enable")] = mirolo.DSP.pwr;

        *statusCode = 200;
        json[F("status")] = 200;
        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url, PSTR("/api/apply")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));
        data[F("apply")] = (memcmp( &mirolo_active, &mirolo, sizeof(miroloHW) ) == 0 && memcmp( &mirolo_active_animation, &mirolo_animation, sizeof(mirolo_animation) ) == 0);

        *statusCode = 200;
        json[F("status")] = 200;
        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url, PSTR("/api/ir")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        JsonArray& codes = data.createNestedArray(F("codes"));
        uint32_t * nextCode = infrared.getNextCode();
        uint16_t count = 0;
        while (nextCode != NULL) {
          codes.add(*nextCode);
          nextCode = infrared.getNextCode();
          Serial.println(count++);
        }
        infrared.deleteCodes();

        *statusCode = 200;
        json[F("status")] = 200;
        //convert to string
        json.printTo(msg, msg_size);
      }
      //no matching route
      else {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));
        *statusCode = 404;
        json[F("status")] = 404;
        //convert to string
        json.printTo(msg, msg_size);
      }

    }
    else if (method >= POST) {
      //we expect JSON at these API routes


      JsonObject& json = jsonBuffer.parse(recv_content);
      //check for parsing failure
      boolean payloadOK = json.success();

      //routes
      if (payloadOK && strcmp_P(url, PSTR("/api/line0")) == 0 && (method == PATCH || method == POST)) {
        boolean parseOK = false; //at least one data field must match the request

        const char * message = json[F("message")];
        if (message) {
          memset(mirolo.DSP.message0, 0, max_bufsize);
          strcpy(mirolo.DSP.message0, message);
          parseOK = true;
        }
        JsonVariant prefix = json[F("prefix")];
        if (prefix.success()) {
          const uint8_t pre = prefix.as<uint8_t>();
          if (pre == 0)strcpy_P(mirolo.DSP.prefix0, PSTR("off"));
          else ltoa(pre, mirolo.DSP.prefix0, 10);
          parseOK = true;
        }
        const uint8_t scrolldelay = json[F("speed")];
        if (scrolldelay >= 20) {
          mirolo.DSP.scrolldelay0 = scrolldelay;
          parseOK = true;
        }

        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        if (parseOK) {
          if (method == POST) {
            mirolo.DSP.apply_changes |= LINE0;
          }
          buildJSONline0(data, method);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 400;
          json[F("status")] = 400;
        }
        //convert to string
        json.printTo(msg, msg_size);

      }
      else if (payloadOK && strcmp_P(url, PSTR("/api/line1")) == 0 && (method == PATCH || method == POST)) {
        boolean parseOK = false; //at least one data field must match the request

        const char * message = json[F("message")];
        if (message) {
          memset(mirolo.DSP.message1, 0, max_bufsize);
          strcpy(mirolo.DSP.message1, message);
          parseOK = true;
        }
        JsonVariant prefix = json[F("prefix")];
        if (prefix.success()) {
          const uint8_t pre = prefix.as<uint8_t>();
          if (pre == 0)strcpy_P(mirolo.DSP.prefix1, PSTR("off"));
          else ltoa(pre, mirolo.DSP.prefix1, 10);
          parseOK = true;
        }
        const uint8_t scrolldelay = json[F("speed")];
        if (scrolldelay >= 20) {
          mirolo.DSP.scrolldelay1 = scrolldelay;
          parseOK = true;
        }

        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        if (parseOK) {
          if (method == POST) {
            mirolo.DSP.apply_changes |= LINE1;
          }
          buildJSONline1(data, method);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 400;
          json[F("status")] = 400;
        }
        //convert to string
        json.printTo(msg, msg_size);

      }
      else if (payloadOK && strcmp_P(url, PSTR("/api/ring0")) == 0 && (method == PATCH || method == POST)) {
        static uint8_t LastFrame = mirolo_animation.LastFrame;
        boolean parseOK = false; //at least one data field must match the request
        if (method == POST) parseOK = true; //POST can be empty. just apply previously sent frames

        //get loop bit
        JsonVariant Loop = json[F("loop")];
        if (Loop.success()) {
          const boolean lo = Loop.as<boolean>();
          mirolo_animation.Loop = lo;
          parseOK = true;
        }
        bool frameOK = false;
        JsonVariant frame = json[F("frame")];
        if (frame.success()) {
          //get frame display time
          uint32_t Time = frame["time"];
          if (Time >= 20) {
            if (LastFrame < frame_size) mirolo_animation.Frames[LastFrame].Time = Time;
            frameOK = true;
          }
          //iterate over frame colors
          JsonVariant colors = frame["colors"];
          uint8_t colorIndex = 0;
          if (colors.success()) {
            for ( const auto& color : colors.as<JsonArray>() ) {
              const char * co = color.as<const char*>();
              char * colorString = new char[strlen(co) + 1];
              strcpy(colorString, co);

              if (colorIndex < 16 && colorString) {
                colorString[0] = '0'; //html notation! override '#'
                uint32_t col = strtoul (colorString, NULL, 16);
                if (LastFrame < frame_size) mirolo_animation.Frames[LastFrame].Colors[colorIndex] = col;
              }
              delete [] colorString;

              ++colorIndex;
            }
          }
          //set remaining pixels to black
          for (uint8_t i = colorIndex; i < ring.led.numPixels(); ++i) {
            if (LastFrame < frame_size) mirolo_animation.Frames[LastFrame].Colors[i] = BLACK;
          }
          if (LastFrame < frame_size) {
            mirolo_animation.LastFrame = LastFrame;
            ++LastFrame;
          }

          parseOK = true; //we have parsed at least one frame successfully
        }


        if (method == PATCH && frameOK == false) {
          //frame was empty or not found. clear animation
          LastFrame = 0;
          mirolo_animation.LastFrame = 0;

          for (uint8_t i = 0; i < ring.led.numPixels(); ++i) {
            mirolo_animation.Frames[LastFrame].Colors[i] = BLACK;
          }
          mirolo_animation.Frames[LastFrame].Time = 500;
        }

        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        if (parseOK) {
          if (method == POST) {
            mirolo.DSP.apply_changes |= RING0;
          }
          buildJSONring0(data, method);

          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 400;
          json[F("status")] = 400;
        }
        //convert to string
        json.printTo(msg, msg_size);

      }
      else if (payloadOK && strcmp_P(url, PSTR("/api/brightness")) == 0 && (method == PATCH || method == POST)) {
        boolean parseOK = false; //at least one data field must match the request

        JsonVariant brightness = json[F("brightness")];
        if (brightness.success()) {
          const uint8_t bri = brightness.as<uint8_t>();
          if (bri <= 16) {
            mirolo.DSP.brightness = bri;
            parseOK = true;
          }
        }

        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        if (parseOK) {
          if (method == POST) {
            mirolo.DSP.apply_changes |= BRIGHT;
          }
          JsonObject& pending = data.createNestedObject(F("pending"));
          pending[F("brightness")] = mirolo.DSP.brightness;
          JsonObject& active = data.createNestedObject(F("active"));
          //changes not applied yet. send pseudo updated values
          if (method == POST) {
            active[F("brightness")] = mirolo.DSP.brightness;
          }
          else {
            active[F("brightness")] = mirolo_active.DSP.brightness;
          }
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 400;
          json[F("status")] = 400;
        }
        //convert to string
        json.printTo(msg, msg_size);

      }
      else if (payloadOK && strcmp_P(url, PSTR("/api/name")) == 0 && method == POST) {
        boolean parseOK = false; //at least one data field must match the request

        const char * Name = json[F("name")];
        if (Name && strlen(Name) < 25) {
          memset(mirolo.Name, 0, dsp_name_size);
          strcpy(mirolo.Name, Name);
          parseOK = true;
        }

        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        if (parseOK) {
          mirolo.DSP.apply_changes |= NAME;
          data[F("name")] = mirolo.Name;
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 400;
          json[F("status")] = 400;
        }
        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (payloadOK && strcmp_P(url, PSTR("/api/power")) == 0 && method == POST) {
        boolean parseOK = false; //at least one data field must match the request

        JsonVariant enable = json[F("enable")];
        if (enable.success()) {
          const boolean en = enable.as<boolean>();
          mirolo.DSP.pwr = en;
          parseOK = true;
        }

        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        if (parseOK) {
          data[F("enable")] = mirolo.DSP.pwr;
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 400;
          json[F("status")] = 400;
        }
        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url, PSTR("/api/revert")) == 0 && method == POST) {
        //reset display (copy all values to the inactive struct)
        memcpy ( &mirolo, &mirolo_active, sizeof(miroloHW) );
        memcpy ( &mirolo_animation, &mirolo_active_animation, sizeof(mirolo_animation) );

        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));
        data[F("revert")] = true;
        *statusCode = 200;
        json[F("status")] = 200;
        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url, PSTR("/api/apply")) == 0 && method == POST) {
        mirolo.DSP.apply_changes |= ALL;

        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));
        data[F("apply")] = true;
        *statusCode = 200;
        json[F("status")] = 200;
        //convert to string
        json.printTo(msg, msg_size);
      }
      //no matching route
      else {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        if (payloadOK == false) {
          *statusCode = 400;
          json[F("status")] = 400;
        }
        else {
          *statusCode = 404;
          json[F("status")] = 404;
        }
        //convert to string
        json.printTo(msg, msg_size);
      }


    }
    else {
      //something went really wrong
      jsonBuffer.clear();
      JsonObject& json = jsonBuffer.createObject();
      JsonObject& data = json.createNestedObject(F("data"));

      *statusCode = 404;
      json[F("status")] = 404;
      //convert to string
      json.printTo(msg, msg_size);
    }

    //set content type
    strcpy_P(contentType, PSTR("application/json"));

  }


  content_length += Send_String(client, transmit, msg);
  return content_length;
}



#endif
