//@(#) input.h

#ifndef INPUT_H
#define INPUT_H


class input
{
  private:

    const uint16_t pin;
    unsigned long timer = 0;
    boolean state = false;
    boolean state_long = false;


  public:

    boolean pressed_long = false;
    boolean pressed_long_hold = false;
    boolean pressed = false;


    input(const uint16_t pin) : pin(pin) {
    }

    void Update() { //button input debouncing and press duration detection


      pressed = false;
      pressed_long = false;

      //button pressed
      if (!digitalRead(pin) && !state && millis() - timer >= 50) {
        state = true;
        timer = millis();
      }
      //button pressed long
      else if (!digitalRead(pin) && state && !state_long && millis() - timer >= 1000) {
        state_long = true;
        pressed_long = true;
        pressed_long_hold = true;
      }
      //button pressed short
      else if (digitalRead(pin) && state && millis() - timer >= 50) {
        if (millis() - timer < 1000) pressed = true;
        pressed_long_hold = false;
        state_long = false;
        state = false;
        timer = millis();
      }
    }

};

#endif
