//@(#) ir.h

#ifndef IR_H
#define IR_H

#include <IRremote.h>


class irRecv
{

  private:

    static const uint8_t ringBufferSize = 11;
    uint32_t ringBuffer[ringBufferSize];
    uint8_t Start = 0;
    uint8_t Current = 0;
    uint8_t End = 0;
    boolean newCodeReceived = false;
    IRrecv irrecv;
    decode_results ir_results;


  public:

    irRecv(const int &pin) : irrecv(IRrecv(pin)) {

    }

    //add a new code to the ringbuffer
    void addCode(const uint32_t &code) {
      ringBuffer[End] = code;
      newCodeReceived = true;

      Current = End;
      End++;

      if (End >= ringBufferSize) {
        End = 0;
      }
      if (End == Start) {
        Start++;
      }
      if (Start >= ringBufferSize) {
        Start = 0;
      }

    }


    //delete all codes from the ringbuffer
    void deleteCodes() {
      newCodeReceived = false;
      Start = 0;
      Current = 0;
      End = 0;
    }


    //index before sysmessageStart/End (needed for reverse listing of messages)
    uint8_t bufferIndexMinusOne(uint8_t & index) {
      if (uint8_t(index - 1) == UINT8_MAX) return ringBufferSize - 1;
      else return index - 1;
    }


    //get next code from ringbuffer. This will iterate through all previously received codes in the buffer. returns NULL when all codes have been returned
    uint32_t * getNextCode() {
      uint32_t * result = NULL;

      if (Start != End && Current != bufferIndexMinusOne(Start)) {
        result = &ringBuffer[Current];
        Current = bufferIndexMinusOne(Current);
      }
      else if (Current == bufferIndexMinusOne(Start)) {
        Current = bufferIndexMinusOne(End);
      }
      return result;
    }


    //get current newly received code from ringbuffer. returns NULL if no new code is available
    uint32_t * getCurrentCode() {
      uint32_t * result = NULL;

      if (newCodeReceived == true && Start != End) {
        newCodeReceived = false;
        result = &ringBuffer[bufferIndexMinusOne(End)];
      }
      return result;
    }


    //setup
    void Setup() {
      irrecv.enableIRIn();
    }

    //mainloop
    void Loop() {

      //receive ir codes
      if (irrecv.decode(&ir_results)) {
        //Serial.println(ir_results.value, HEX);
        if (ir_results.value != 0xFFFFFFFF) addCode(ir_results.value);
        irrecv.resume(); // Receive the next value
      }
    }

};

#endif
