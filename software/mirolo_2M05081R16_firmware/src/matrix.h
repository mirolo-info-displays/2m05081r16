//@(#) marix.h

#ifndef MATRIX_H
#define MATRIX_H

#define USE_NEW_FONT 0
#include <MD_MAX72xx.h>                 //MAX7219 library 

#define HARDWARE_TYPE MD_MAX72XX::FC16_HW

//matrix global settings
const uint8_t max_linecount = 2;
const int max_bufsize = (280 + 1); //280 chars + '\0'
const int max_devices0 = 5;
const int max_devices1 = 8;
const uint16_t max_scroll_delay = 75;
const uint8_t max_char_spacing = 1;


//matrix global variables
struct max_LineDefinition
{
  MD_MAX72XX  mx;                 //object definition
  char    message[max_bufsize];   //message for this display
  uint8_t staticDivider;          //message is displayed staticly up to this char if possible
  boolean newMessageAvailable;    //true if new message arrived
};

struct max_LineDefinition  max_Line[] = //MAX7219 line entries
{
  { MD_MAX72XX(HARDWARE_TYPE, max_data, max_clk, max_cs0, max_devices0), "", 0, true },
  { MD_MAX72XX(HARDWARE_TYPE, max_data, max_clk, max_cs1, max_devices1), "", 0, true }
};




uint8_t scrollTextSource0(uint8_t dev, MD_MAX72XX::transformType_t t) {
  // Line0 Callback function for data that is required for scrolling into the display

  const uint8_t   lineID = 0; // the max_line this callback is attached to
  static char *   msg_ptr = max_Line[lineID].message + max_Line[lineID].staticDivider * sizeof(max_Line[lineID].message[0]); //maniac pointer arithmetic. may explode at some point
  static uint8_t  state = 0;
  static uint16_t curLen = 0, showLen = 0;
  static uint8_t  cBuf[8] = {0};
  uint8_t colData = 0;

  //reset if new message arrived
  if (max_Line[lineID].newMessageAvailable == true) {
    max_Line[lineID].mx.clear();
    msg_ptr = max_Line[lineID].message + max_Line[lineID].staticDivider * sizeof(max_Line[lineID].message[0]); //maniac pointer arithmetic. may explode at some point
    curLen = 0;
    showLen = 0;
    state = 0;
  }

  // finite state machine to control what we do on the callback
  switch (state)
  {
    case 0: // Load the next character from the font table
      showLen = max_Line[lineID].mx.getChar(*msg_ptr++, sizeof(cBuf) / sizeof(cBuf[0]), cBuf); //returns width(dots) of the character using the internal font
      curLen = 0;
      state = 1;
    // !! deliberately fall through to next state to start displaying

    case 1: // display the next part of the character
      colData = cBuf[curLen++];
      if (curLen == showLen)
      {
        showLen = max_char_spacing; //inter character spacing
        curLen = 0;
        state = 2;
      }
      break;

    case 2: //display inter-character spacing (blank column) | blank character padding
      colData = 0;
      curLen++;
      if (curLen == showLen) {

        // if we reached end of message, reset the message pointer
        if (*msg_ptr == '\0') {
          // reset the pointer to start of message
          msg_ptr = max_Line[lineID].message + max_Line[lineID].staticDivider * sizeof(max_Line[lineID].message[0]); //maniac pointer arithmetic. may explode at some point

          showLen = max_Line[lineID].mx.getColumnCount() * 3 / 2 - max_Line[lineID].staticDivider * 4; //begin blank character padding and remain in state 2
          curLen = 0;
        }
        else {
          state = 0;
        }
      }
      break;

    default:
      state = 0;
  }

  return (colData);
}

uint8_t scrollTextSource1(uint8_t dev, MD_MAX72XX::transformType_t t) {
  // Line1 Callback function for data that is required for scrolling into the display

  const uint8_t   lineID = 1; // the max_line this callback is attached to
  static char *   msg_ptr = max_Line[lineID].message + max_Line[lineID].staticDivider * sizeof(max_Line[lineID].message[0]); //maniac pointer arithmetic. may explode at some point
  static uint8_t  state = 0;
  static uint16_t curLen = 0, showLen = 0;
  static uint8_t  cBuf[8] = {0};
  uint8_t colData = 0;

  //reset if new message arrived
  if (max_Line[lineID].newMessageAvailable == true) {
    max_Line[lineID].mx.clear();
    msg_ptr = max_Line[lineID].message + max_Line[lineID].staticDivider * sizeof(max_Line[lineID].message[0]); //maniac pointer arithmetic. may explode at some point
    curLen = 0;
    showLen = 0;
    state = 0;
  }

  // finite state machine to control what we do on the callback
  switch (state)
  {
    case 0: // Load the next character from the font table
      showLen = max_Line[lineID].mx.getChar(*msg_ptr++, sizeof(cBuf) / sizeof(cBuf[0]), cBuf); //returns width(dots) of the character using the internal font
      curLen = 0;
      state = 1;
    // !! deliberately fall through to next state to start displaying

    case 1: // display the next part of the character
      colData = cBuf[curLen++];
      if (curLen == showLen)
      {
        showLen = max_char_spacing; //inter character spacing
        curLen = 0;
        state = 2;
      }
      break;

    case 2: //display inter-character spacing (blank column) | blank character padding
      colData = 0;
      curLen++;
      if (curLen == showLen) {

        // if we reached end of message, reset the message pointer
        if (*msg_ptr == '\0') {
          // reset the pointer to start of message
          msg_ptr = max_Line[lineID].message + max_Line[lineID].staticDivider * sizeof(max_Line[lineID].message[0]); //maniac pointer arithmetic. may explode at some point

          showLen = max_Line[lineID].mx.getColumnCount() * 3 / 2 - max_Line[lineID].staticDivider * 4; //begin blank character padding and remain in state 2
          curLen = 0;
        }
        else {
          state = 0;
        }
      }
      break;

    default:
      state = 0;
  }

  return (colData);
}



void printText(uint8_t lineID, uint8_t modStart, uint8_t modEnd, char *msg) // Print the text string to the LED matrix modules specified.
// Message area is padded with blank columns after printing.
{
  uint8_t   state = 0;
  char *    msg_ptr = msg;
  uint16_t  curLen = 0, showLen = 0;
  uint8_t   cBuf[8] = {0};
  int16_t   col = ((modEnd + 1) * COL_SIZE) - 1; //maximum available columns (index)

  max_Line[lineID].mx.control(modStart, modEnd, MD_MAX72XX::UPDATE, MD_MAX72XX::OFF);

  do     // finite state machine to print the characters in the space available
  {
    switch (state)
    {
      case 0: // Load the next character from the font table
        if (*msg_ptr == '\0') //end of message
        {
          showLen = col - (modEnd * COL_SIZE);  // padding characters
          state = 2;
          break;
        }

        // retrieve the next character from the font file
        showLen = max_Line[lineID].mx.getChar(*msg_ptr++, sizeof(cBuf) / sizeof(cBuf[0]), cBuf);
        curLen = 0;
        state = 1;
      // !! deliberately fall through to next state to start displaying

      case 1: // display the next part of the character
        max_Line[lineID].mx.setColumn(col--, cBuf[curLen++]);

        // done with font character, now display the space between chars
        if (curLen == showLen)
        {
          showLen = max_char_spacing;
          state = 2;
        }
        break;

      case 2: // initialize state for displaying empty columns
        curLen = 0;
        state = 3;
      // fall through

      case 3: // display inter-character spacing or end of message padding (blank columns)
        max_Line[lineID].mx.setColumn(col--, 0);
        curLen++;
        if (curLen == showLen)
          state = 0;
        break;

      default:
        col = -1;   // this definitely ends the do loop
    }
  } while (col >= (modStart * COL_SIZE));

  max_Line[lineID].mx.control(modStart, modEnd, MD_MAX72XX::UPDATE, MD_MAX72XX::ON);
}



void printText(uint8_t lineID, int16_t colEnd, char *msg) // Print the text string to the LED matrix modules up to the specified column
// Message area is NOT padded with blank columns after printing.
{
  uint8_t   state = 0;
  char *    msg_ptr = msg;
  uint16_t  curLen = 0, showLen = 0;
  uint8_t   cBuf[8] = {0};
  int16_t   col = max_Line[lineID].mx.getColumnCount() - 1; //start at leftmost column (index)

  if (colEnd > col) return; //stop if colCount is out of bounds

  max_Line[lineID].mx.control((colEnd + 1) / COL_SIZE, max_Line[lineID].mx.getDeviceCount() - 1, MD_MAX72XX::UPDATE, MD_MAX72XX::OFF);

  do     // finite state machine to print the characters in the space available
  {
    switch (state)
    {
      case 0: // Load the next character from the font table
        if (*msg_ptr == '\0') //end of message
        {
          col = -1; //just in case there are columns left
          break;
        }

        // retrieve the next character from the font file
        showLen = max_Line[lineID].mx.getChar(*msg_ptr++, sizeof(cBuf) / sizeof(cBuf[0]), cBuf);
        curLen = 0;
        state = 1;
      // !! deliberately fall through to next state to start displaying

      case 1: // display the next part of the character
        max_Line[lineID].mx.setColumn(col--, cBuf[curLen++]);

        // done with font character, now display the space between chars
        if (curLen == showLen)
        {
          showLen = max_char_spacing;
          state = 2;
        }
        break;

      case 2: // initialize state for displaying empty columns
        curLen = 0;
        state = 3;
      // fall through

      case 3: // display inter-character spacing
        max_Line[lineID].mx.setColumn(col--, 0);
        curLen++;
        if (curLen == showLen)
          state = 0;
        break;

      default:
        col = -1;   // this definitely ends the do loop
    }
  } while (col >= colEnd);

  max_Line[lineID].mx.control((colEnd + 1) / COL_SIZE, max_Line[lineID].mx.getDeviceCount() - 1, MD_MAX72XX::UPDATE, MD_MAX72XX::ON);
}





boolean isScrolling(char * msg, MD_MAX72XX &mx) { //returns true if the message would need to scroll on the given display line

  char * msg_ptr = msg;
  uint8_t  cBuf[8]; //max character width is 8 dots!
  uint16_t msg_width = 0;
  uint16_t line_width = mx.getColumnCount();

  while (*msg_ptr != '\0') {
    msg_width += mx.getChar(*msg_ptr++, sizeof(cBuf) / sizeof(cBuf[0]), cBuf);
    msg_width += max_char_spacing;
    if (msg_width > line_width) return true;
  }

  return false;
}

boolean isScrollingPrefix(const char * msg, const uint8_t &prefix) { //returns true if the message would need to scroll on the given display line after the prefix is applied
  boolean result = false;

  if (prefix > 0) {
    char * static_msg = new char[prefix + 1];
    strncpy (static_msg, msg , prefix);
    static_msg[prefix] = '\0'; //terminate string

    result = (strlen(msg) - strlen(static_msg) > 0);

    delete [] static_msg;
  }
  return result;
}


uint16_t getColumnCountMessage(char * msg, MD_MAX72XX &mx) { //returns the columns needed to display the message on the specified display
  char * msg_ptr = msg;
  uint8_t  cBuf[8]; //max character width is 8 dots!
  uint16_t msg_width = 0;

  while (*msg_ptr != '\0') {
    msg_width += mx.getChar(*msg_ptr++, sizeof(cBuf) / sizeof(cBuf[0]), cBuf);
    msg_width += max_char_spacing;
  }
  return msg_width;
}


uint32_t getMsgRoundtriptime(char * msg, const uint8_t &prefix, const uint8_t &scrollspeed, MD_MAX72XX &mx) { //returns the time needed to fully display a scrolling message in ms
  uint32_t result = 0;

  if (isScrollingPrefix(msg, prefix)) {
    char * remainMessage = msg + prefix * sizeof(msg[0]); //maniac pointer arithmetic. may explode at some point
    result = uint32_t(getColumnCountMessage(remainMessage, mx) + mx.getColumnCount()) * scrollspeed;
  }
  else if (isScrolling(msg, mx)) {
    result = uint32_t(getColumnCountMessage(msg, mx) + mx.getColumnCount()) * scrollspeed;
  }

  return result;
}


boolean staticDivisionOK(const char * msg, const uint8_t &prefix, MD_MAX72XX &mx) { //checks if there's at least 1/3 of the display left for scrolling text
  boolean result = true;

  if (prefix > 0) {
    char * static_msg = new char[prefix];
    strncpy (static_msg, msg , prefix);
    static_msg[prefix] = '\0'; //terminate string

    result = getColumnCountMessage(static_msg, mx) <= (mx.getColumnCount() * 2 / 3);

    delete [] static_msg;
  }

  return result;
}




boolean checkFont(char * msg, MD_MAX72XX &mx) { //checks if the given message can be displayed with the internal Font

  uint8_t  cBuf[8] = {0};
  boolean result = true;

  for (uint16_t pos = 0; pos < strlen(msg); pos++) {
    if (mx.getChar(msg[pos], sizeof(cBuf) / sizeof(cBuf[0]), cBuf) == 0) { //returns width(dots) of the character using the internal font
      result = false;
      break;
    }
  }
  return result;
}








void displayText(uint8_t lineID) { //displays text on given line. automatically sets it scrolling if necessary

  static char * static_msg[max_linecount];
  static boolean divisionOK[max_linecount];
  static boolean scrolling[max_linecount];
  static uint16_t static_colCount[max_linecount];

  uint8_t divider = max_Line[lineID].staticDivider; //save value

  //prepare
  if (max_Line[lineID].newMessageAvailable) {
    if (static_msg[lineID]) delete[] static_msg[lineID];
    //delete[] scroll_msg;

    uint8_t divider = max_Line[lineID].staticDivider;

    static_msg[lineID] = new char[divider + 1];
    //scroll_msg = new char[max_bufsize - divider];

    //create static message
    strncpy (static_msg[lineID], max_Line[lineID].message , divider );
    static_msg[lineID][divider] = '\0'; //terminate string

    //check static division
    divisionOK[lineID] = staticDivisionOK(max_Line[lineID].message, max_Line[lineID].staticDivider, max_Line[lineID].mx);

    //check if message would scroll on this display
    scrolling[lineID] = isScrolling(max_Line[lineID].message, max_Line[lineID].mx);

    //save column count for static messages so we don't calculate them every time
    static_colCount[lineID] = getColumnCountMessage(static_msg[lineID], max_Line[lineID].mx);
  }


  //ommit divider if static division would render display unreadable
  if (!divisionOK[lineID] || strlen(max_Line[lineID].message) <= max_Line[lineID].staticDivider) {
    max_Line[lineID].staticDivider = 0;
  }


  //no static text divider
  if (max_Line[lineID].staticDivider == 0) {

    if (scrolling[lineID]) {
      max_Line[lineID].mx.transform(MD_MAX72XX::TSL); // scroll along - the callback will load all the data
      max_Line[lineID].newMessageAvailable = false;
    }
    else if (!scrolling[lineID] && max_Line[lineID].newMessageAvailable) {
      printText(lineID, 0, max_Line[lineID].mx.getDeviceCount() - 1, max_Line[lineID].message);
      max_Line[lineID].newMessageAvailable = false;
    }
  }

  //static text divider
  else {
    max_Line[lineID].mx.update(MD_MAX72XX::OFF); //turn auto update off to prevent display from flickering during lshift
    max_Line[lineID].mx.transform(MD_MAX72XX::TSL); // scroll along - the callback will load all the data
    printText(lineID, (max_Line[lineID].mx.getColumnCount() - 1) - static_colCount[lineID], static_msg[lineID]); //display static message
    max_Line[lineID].newMessageAvailable = false;
  }

  max_Line[lineID].staticDivider = divider; //reset value

}






#endif
