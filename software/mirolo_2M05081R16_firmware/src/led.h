//@(#) led.h

#ifndef LED_H
#define LED_H



/*usage example:
  singleLED.setLED(true/1) -> LED on
  singleLED.setLED(true/1, 400) -> LED on for 400ms
  singleLED.setLED(true/1, 300, 500) -> LED pulsing 300ms on 500ms off
*/
class singleLED
{

  private:

    const int led;

    unsigned long timer = 0;
    uint16_t p_on = 500;
    uint16_t p_off = 500;
    boolean pulse_status = false;


  public:

    singleLED(const uint16_t &pin) : led(pin) {

    }

    //set led parameter
    void setLED(const int8_t state = -1, const uint16_t pulse_on = 0, const uint16_t pulse_off = 0) {

      if (state == -1 && pulse_on == 0 && pulse_off == 0) {
        if (pulse_status) {
          if (!digitalRead(led) && millis() - timer >= p_off) {
            digitalWrite(led, HIGH);
            if (p_on > 0) timer = millis();
            else pulse_status = false;
          }
          else if (digitalRead(led) && millis() - timer >= p_on) {
            digitalWrite(led, LOW);
            if (p_off > 0) timer = millis();
            else pulse_status = false;

          }

        }
      }
      else if (state == 1 && pulse_on > 0) {
        p_on = pulse_on;
        p_off = pulse_off;
        pulse_status = true;
        if (pulse_off == 0) timer = millis() - p_on;
      }
      else if (state == 1) {
        digitalWrite(led, HIGH);
        pulse_status = false;
      }
      else if (state == 0) {
        digitalWrite(led, LOW);
        pulse_status = false;
      }
    }


    //mainloop
    void Loop() {

      setLED();
    }

};




/*usage example:
  colorLED.setLED('r') -> LED red on
  colorLED.setLED('y', 400) -> LED yellow on for 400ms
  colorLED.setLED('g', 300, 500) -> LED green pulsing 300ms on 500ms off
*/
class colorLED
{

  private:

    const int red_led;
    const int green_led;

    unsigned long timer = 0;
    uint16_t p_on = 500;
    uint16_t p_off = 500;
    boolean pulse_status = false;
    boolean led_state = false;
    char state_local = 'x';


  public:


    colorLED(const uint16_t &pinR, const uint16_t &pinG) : red_led(pinR), green_led(pinG) {

    }

    //set led parameter
    void setLED(const char state = 'o', const uint16_t pulse_on = 0, const uint16_t pulse_off = 0) {

      if (pulse_on == 0 && pulse_off == 0) {
        if (state == 'y' || state == 'r' || state == 'g' || state == 'x') {
          state_local = state;
        }
        else if (pulse_status) {
          if (!led_state && millis() - timer >= p_off) {
            led_state = true;
            if (p_on > 0) timer = millis();
            else {
              pulse_status = false;
              state_local = state;
            }
          }
          else if (led_state && millis() - timer >= p_on) {
            digitalWrite(red_led, LOW);
            digitalWrite(green_led, LOW);
            led_state = false;
            if (p_off > 0) timer = millis();
            else {
              pulse_status = false;
              state_local = state;
            }
            return;
          }

          else return;
        }
      }
      else if ((state == 'y' || state == 'r' || state == 'g') && pulse_on > 0) {
        p_on = pulse_on;
        p_off = pulse_off;
        state_local = state;
        pulse_status = true;
        if (pulse_off == 0) {
          timer = millis() - p_on;
          led_state = false;
        }
        return;
      }
      else {
        state_local = state;
        pulse_status = false;
      }

      if (state_local == 'r') {
        digitalWrite(red_led, HIGH);
        digitalWrite(green_led, LOW);
      }
      else if (state_local == 'g') {
        digitalWrite(green_led, HIGH);
        digitalWrite(red_led, LOW);
      }
      else if (state_local == 'y') {
        digitalWrite(red_led, HIGH);
        digitalWrite(green_led, HIGH);
      }
      else {
        digitalWrite(red_led, LOW);
        digitalWrite(green_led, LOW);
      }

    }


    //mainloop
    void Loop() {

      setLED();
    }

};


#endif
