//@(#) remote.h

#ifndef REMOTE_H
#define REMOTE_H

#define IR_RED 2155856445 //red

#define IR_GREEN 2155868175 //green

#define IR_YELLOW 2155809015 //yellow

#define IR_BLUE 2155813095 //blue

#define IR_PWR 2155807485 //power(left)

#define IR_PWR1 2155843440 //power(right)

#define IR_BACK 2155845735 //back

#define IR_UP 2155833495 //up

#define IR_DOWN 2155829415 //down

#define IR_LEFT 2155842165 //left

#define IR_RIGHT 2155809525 //right

#define IR_MOUSE 2155806975 //mouse

#define IR_HOME 2155841655 //home

#define IR_1 2155836045 //1

#define IR_2 2155851855 //2

#define IR_3 2155819215 //3

#define IR_4 2155827885 //4

#define IR_5 2155843695 //5

#define IR_6 2155811055 //6

#define IR_7 2155831965 //7

#define IR_8 2155847775 //8

#define IR_9 2155815135 //9

#define IR_0 2155839615 //0

#define IR_DEL 2155823805 //delete

#define IR_MUTE 2155840125 //mute

#define IR_MENU 2155819725 //menu

#define IR_SET 2155827120 //set

#define IR_TVIN 2155859760 //tv in

#define IR_VOLUP 2155851600 //vol+

#define IR_VOLDOWN 2155818960 //vol-

#define IR_OK 2155857975 //ok

#endif
