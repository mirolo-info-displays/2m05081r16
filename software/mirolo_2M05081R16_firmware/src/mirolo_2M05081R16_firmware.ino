/*
  Firmware for mirolo 2M05081R16 led info panel

  Created 01. Dec. 2017
  by Simon Junga (https://gitlab.com/Theolyn)
*/

//set the unique serial number here to upload device specific calibration and mac address
//have a look at settings and definitions in global.h
#define gc 0
#define ef 1
#define none 2
#define EVENT ef
#define SERIALNR 4
#define HTTPPASSWD "1346"

#define LOAD_EEPROM 1
#if EVENT == gc
#define INITIAL_MSG_LINE0 "galacon"
#define LOAD_MODULE 1 //0 none, 1 schedule, 2 messageboard
#elif EVENT == ef
#define INITIAL_MSG_LINE0 "ef27"
#define LOAD_MODULE 2 //0 none, 1 schedule, 2 messageboard
#else
#define INITIAL_MSG_LINE0 "mirolo"
#define LOAD_MODULE 0 //0 none, 1 schedule, 2 messageboard
#endif



//defines for ethernet hang check
#define NET_ENC28J60_EIR                                 0x1C
#define NET_ENC28J60_ESTAT                               0x1D
#define NET_ENC28J60_ECON1                               0x1F
#define NET_ENC28J60_EIR_RXERIF                          0x01
#define NET_ENC28J60_ESTAT_BUFFER                        0x40
#define NET_ENC28J60_ECON1_RXEN                          0x04



//pins
const int pin_s1 = 10;
const int pin_s2 = 15;
const int pin_led1 = 13;
const int pin_led2 = 14;

const int ws2812 = 1;
const int ir = 0;
const int ldr = 24;

const int max_data = 23;
const int max_clk = 22;
const int max_cs0 = 21;
const int max_cs1 = 20;


//function declaration
void setDisplayTest(const bool &state);


#include <EEPROM.h>
#include <avr/wdt.h>
#include <limits.h>
#include <UIPEthernet.h>
#include <ArduinoJson.h>
#include <base64.hpp>
#include "urldecode.h"
#include "matrix.h"
#include "global.h"
#include "http.h"
#include "sendHTML.h"
#if LOAD_MODULE == 1
#include "module_schedule.h"
#endif
#if LOAD_MODULE == 2
#include "module_messageboard.h"
#endif
#include "eeprom.h"



//global settings
const uint8_t serial_stringsize = 31;
const unsigned long serial_baud PROGMEM = 115200;


//I²C
//const byte i2cDelay = 2;
//byte request_bytes = 0; //amount of bytes to send on next data transmission request
//byte request_data[32] = {0}; //array to store data to send




//global variables
uint8_t animation_state = 0;
char ser_inputString[serial_stringsize] = {'\0'};
char action[serial_stringsize] = {'\0'};
char address[serial_stringsize] = {'\0'};
char value[serial_stringsize] = {'\0'};








// ******* setup
void setup() {

  //save MCUSR register to determine reset cause
  byte ResetSrc = MCUSR; // save reset source
  MCUSR = 0x00; //clear for next reset detection

  //disable watchdog timer
  wdt_disable();

  //error flag
  bool error = false;

  //initialize inputs/outputs
  pinMode(ldr, INPUT);
  pinMode(ir, INPUT);

  pinMode(pin_s1, INPUT);
  pinMode(pin_s2, INPUT);
  digitalWrite(pin_s1, INPUT_PULLUP);
  digitalWrite(pin_s2, INPUT_PULLUP);

  pinMode(pin_led1, OUTPUT);
  pinMode(pin_led2, OUTPUT);
  digitalWrite(pin_led1, LOW);
  digitalWrite(pin_led2, LOW);

  //init matrix display
  max_Line[0].mx.begin();
  max_Line[1].mx.begin();
  max_Line[0].mx.setShiftDataInCallback(scrollTextSource0);
  max_Line[1].mx.setShiftDataInCallback(scrollTextSource1);

  delay(300);

  //boot message
  lineBootAnimate(F(INITIAL_MSG_LINE0), 0);
  lineBootAnimate(F("init"), 1);

  //settings from EEPROM
  if (LOAD_EEPROM) {
    loadEEPROM();
    //copy to inactive struct
    memcpy(&mirolo, &mirolo_active, sizeof(miroloHW));
    memcpy(&mirolo_animation, &mirolo_active_animation, sizeof(mirolo_animation));
    mirolo.DSP.apply_changes |= MEMCPY;
  }

  //init WS2812 LEDs
  ring.Setup();

  //boot animation
  ringBootAnimate(0, 1, 0x466EC8);

  //randomseed from Analog1/Pin25
  randomSeed(analogRead(25));

  //add hardware description
  strcpy(mirolo.FW, __DATE__);
  strcat_P(mirolo.FW, PSTR(" | "));
  strcat(mirolo.FW, __TIME__);

  uint8_t counter = 0;
  uint8_t tmpMac[6] = {0};
  memcpy_P (tmpMac, mac, sizeof(tmpMac));
  for (uint8_t i = 0; i < 18; i += 3) {
    char tmp[2];
    sprintf(tmp, "%.2hhx", tmpMac[counter++]);
    mirolo.MAC[i] = tmp[0];
    mirolo.MAC[i + 1] = tmp[1];
    if (i < 15)mirolo.MAC[i + 2] = '-';
  }

  //boot animation
  ringBootAnimate(2, 3, 0x466EC8);

  //Serial
  Serial.begin(serial_baud);

#if LOAD_MODULE == 1 || LOAD_MODULE == 2
  //start event scheduler
  scheduler.Setup();
#endif

  //serial hello
  Serial.print(mirolo.HW);
  Serial.println(F(" led info panel"));
  Serial.println(F("Simon Junga | theolyn@is-a-furry.org"));
  Serial.println(F("_____________________________________"));
  Serial.println(F("Firmware Compile Date: "));
  Serial.println(mirolo.FW);
  Serial.println(F("Device Serial Number:"));
  Serial.println(mirolo.SNR);
  Serial.println(F("_____________________________________"));

  //boot animation
  ringBootAnimate(4, 5, 0x466EC8);
  //boot message
  lineBootAnimate(F("network"), 1);

  //serial hello
  //Serial.println(F("Init I2C"));

  //init I²C
  //Wire.begin(i2c_address);    //join i2c bus
  //Wire.onRequest(wireRequest);   //register event for master request
  //Wire.onReceive(wireReceive);   //register event for receiving data

  //serial hello
  Serial.println(F("Initializing Ethernet..."));
  //init ethernet
#if LOAD_EEPROM == 1
  //static IP
  if (netconf.ip != IPAddress(0, 0, 0, 0)){
    Ethernet.begin(tmpMac, netconf.ip);
  }
  else {
    //try to get IP from DHCP
    if (1 != Ethernet.begin(tmpMac)) {
      Serial.println(F("DHCP not responding. Rebooting with static IP in 5 seconds\n\n"));
      //boot animation
      ringBootAnimate(6, 7, RED);
      //boot message
      lineBootAnimate(F("no dhcp"), 1);
      delay(4800);
      //fallback to static IP
      netconf.ip = static_ip;
      updateEEPROM(); //update eeprom
      delay(20);
      asm volatile ("  jmp 0"); // reboot
    }
  }
#else
  Ethernet.begin(tmpMac, static_ip);
  netconf.ip = static_ip;
#endif

  //add hardware description
  sprintf(mirolo.IP, "%d.%d.%d.%d", Ethernet.localIP()[0], Ethernet.localIP()[1], Ethernet.localIP()[2], Ethernet.localIP()[3]);

  //start TCP server
  server.begin(); 

  //boot animation
  ringBootAnimate(6, 7, 0x466EC8);

  //serial hello
  Serial.print(F("IP-Address: "));
  Serial.println(mirolo.IP);
  Serial.println(F("OK\n"));

  //boot message
  lineBootAnimate(F("services"), 1);
  //boot animation
  ringBootAnimate(8, 9, 0x466EC8);

  //start ir receiver
  infrared.Setup();

  //boot animation
  ringBootAnimate(10, 11, 0x466EC8);

  //react to boot source
  if (ResetSrc & _BV(PORF)) Serial.println(F("System Boot: Power on Source"));
  else if (ResetSrc & _BV(EXTRF)) Serial.println(F("System Boot: External Reset"));
  else if (ResetSrc & _BV(BORF)) Serial.println(F("System Boot: Brown out Detection"));
  else if (ResetSrc & _BV(WDRF)) Serial.println(F("System Boot: Watchdog System Reset"));

  //boot message
  if ( (ResetSrc & _BV(WDRF)) == (0x0 | _BV(WDRF)) ) { //show last message in case of system crash (WDT reset)
    //apply all changes
    mirolo.DSP.apply_changes |= ALL;
  }
  else { //clear
    //clearMiroloHW(MEMCPY);
    lineBootAnimate((mirolo.IP + 8), 1);
  }
  //boot animation
  ringBootAnimate(12, 13, 0x466EC8);

  //end serial hello
  if (error) Serial.println(F("Boot SUCCESSFULL WITH ERRORS"));
  else Serial.println(F("Boot SUCCESSFULL"));
  Serial.println(F("Type \"help\" for a list of commands"));
  Serial.println(F("(Terminate serial commands with \\n)"));
  Serial.println(F("_____________________________________\n\n"));

  //set backside leds
  setLedState();

  //enable watchdog timer (1s)
  wdt_enable(WDTO_4S);

  //boot animation
  ringBootAnimate(14, 15, 0x466EC8);
}








// ******* mainloop
void loop() {



  //receive Serial input
  if (readSerial(ser_inputString, serial_stringsize)) {

    //reset
    strcpy_P(action, PSTR(""));
    strcpy_P(address, PSTR(""));
    strcpy_P(value, PSTR(""));

    char * token = strtok_P(ser_inputString, PSTR(":\n"));
    if (token != NULL) {
      strcpy(action, token); //store string before ":" to string
    }

    token = strtok_P(NULL, PSTR("=\n"));
    if (token != NULL) {
      strcpy(address, token); //store string between ":" and "=" to string
    }

    token = strtok_P(NULL, PSTR("\n"));
    if (token != NULL) {
      strcpy(value, token); //store string after "=" to string
    }


    //process data
    if (strcmp_P(action, PSTR("help")) == 0) {
      Serial.println(F("List of commands:"));
      Serial.println(F("help                              Print this manual"));
      Serial.println(F("network:ip=<IP>                   Set IP Address. Use \"dhcp\" or \"0.0.0.0\" for auto config"));
      Serial.println(F("display:test=<VALUE[0,1]>         Enable/Disable display hardware test"));
      Serial.println(F("system:html=<VALUE[0,1]>          Enable/Disable the html webservice"));
      Serial.println(F("system:rest=<VALUE[0,1]>          Enable/Disable the REST api"));
      Serial.println(F("system:reboot                     Hard Reset the Device"));
#if LOAD_MODULE == 1 || LOAD_MODULE == 2
      Serial.println(F("system:setclock                   Reboot and set the internal Clock"));
#endif
      Serial.println(F("\n"));

    }
    else if (strcmp_P(action, PSTR("network")) == 0 && strcmp_P(address, PSTR("ip")) == 0 && strcmp_P(value, PSTR("")) > 0) {
      boolean dhcp = false;
      IPAddress tmpIP = netconf.ip; //we need to save our address temporarily because IPAddress.fromString() is a pile of garbage
      if (strcmp_P(value, PSTR("dhcp")) == 0) strcpy_P(value, PSTR("0.0.0.0"));
      if (netconf.ip.fromString(value)) {
        if (netconf.ip == IPAddress(0, 0, 0, 0)) dhcp = true;
        Serial.print(F("Setting IP addreess to "));
        if (dhcp) {
          Serial.println(F("auto(dhcp)"));
        }
        else {
          char tmp[16] = {'\0'};
          sprintf(tmp, "%d.%d.%d.%d", netconf.ip[0], netconf.ip[1], netconf.ip[2], netconf.ip[3]);
          Serial.println(tmp);
        }
        updateEEPROM(); //update eeprom
        Serial.print(F("Reboot...\n\n\n"));
        delay(500);
        asm volatile ("  jmp 0"); // reboot
      }
      else {
        netconf.ip = tmpIP; //reset
        Serial.print(F("ERROR: Malformed IP Address("));
        Serial.print(value);
        Serial.println(F(")\n"));
      }
    }
    else if (strcmp_P(action, PSTR("display")) == 0 && strcmp_P(address, PSTR("test")) == 0 && strcmp_P(value, PSTR("")) > 0) {
      bool state = atoi(value);
      Serial.print(F("Setting display test to "));
      if (state)Serial.println(F("ON"));
      else Serial.println(F("OFF"));
      setDisplayTest(state);
      Serial.println(F("\n"));
    }
    else if (strcmp_P(action, PSTR("system")) == 0 && strcmp_P(address, PSTR("html")) == 0 && strcmp_P(value, PSTR("")) > 0) {
      bool state = atoi(value);
      Serial.print(F("Setting html service to "));
      if (state)Serial.println(F("ON"));
      else Serial.println(F("OFF"));
      mirolo.htmlON = state;
      mirolo_active.htmlON = state;
      settingsChanged = true; //update eeprom
      setLedState();
      Serial.println(F("\n"));
    }
    else if (strcmp_P(action, PSTR("system")) == 0 && strcmp_P(address, PSTR("rest")) == 0 && strcmp_P(value, PSTR("")) > 0) {
      bool state = atoi(value);
      Serial.print(F("Setting rest service to "));
      if (state)Serial.println(F("ON"));
      else Serial.println(F("OFF"));
      mirolo.restON = state;
      mirolo_active.restON = state;
      settingsChanged = true; //update eeprom
      setLedState();
      Serial.println(F("\n"));
    }
#if LOAD_MODULE == 1 || LOAD_MODULE == 2
    else if (strcmp_P(action, PSTR("system")) == 0 && (strcmp_P(address, PSTR("reboot")) == 0 || strcmp_P(address, PSTR("setclock")) == 0)) {
      if (strcmp_P(address, PSTR("setclock")) == 0) EEPROM.update(0, true);
      Serial.print(F("Reboot...\n\n\n"));
      delay(500);
      asm volatile ("  jmp 0"); // reboot
    }
#endif
    else {
      Serial.println(F("ERROR: Wrong syntax"));
      Serial.println(F("Type \"help\" for a list of commands"));
      Serial.println(F("(Terminate serial commands with \\n)\n"));
    }
  }




  //system buttons
  if (testMode == true && (s1.pressed || s2.pressed || s1.pressed_long || s2.pressed_long)) { //any button disables test mode
    setDisplayTest(false);
  }
  else {
    if (s1.pressed) { //html on/off
      mirolo.htmlON = !mirolo.htmlON;
      mirolo_active.htmlON = mirolo.htmlON;
      settingsChanged = true; //update eeprom
      setLedState();
    }

    if (s2.pressed) { //rest on/off
      mirolo.restON = !mirolo.restON;
      mirolo_active.restON = mirolo.restON;
      settingsChanged = true; //update eeprom
      setLedState();
    }

    if (s1.pressed_long_hold && s2.pressed_long_hold) { //hard reset
      asm volatile ("  jmp 0");     // reboot
    }

    if (s1.pressed_long) { //display test
      setDisplayTest(true);
    }
  }



  //display test mode
  static uint32_t timer_testmode = 0;
  if (testMode == true) {
    if (millis() - timer_testmode >= 600) {
      testDisplay();
      timer_testmode = millis();
    }
  }




  // listen for incoming clients
  static boolean sendResponse = false;

  static boolean checkHeartbeat = false;
  static unsigned long heartbeatTimeout;

  if (!sendResponse) {
    sendResponse = listenEthernet(server, client, method, url, url_size, pin, session, username, authOK, sessionOK, recv_content, recv_content_size);

    //DEBUG
    /*
      if(sendResponse){
      Serial.print("method: ");
      Serial.println(method);
      Serial.print("url: ");
      Serial.println(url);
      Serial.print("content: ");
      Serial.println(recv_content);
      Serial.print("content_length: ");
      Serial.println(strlen(recv_content));
      }
    */
  }
  //login / auth request / redirect to index
  else if (sendResponse && method == GET && strcmp_P(url, PSTR("/login")) == 0) {
    if (mirolo_active.htmlON) {
      if (!sessionOK && strcmp_P(session, PSTR("0")) == 0) { //login new user

        createSession(session); //create a new session
        checkHeartbeat = true; //enable auto-logout on broken connection
        heartbeatTimeout = millis();
        http_authrequest(client, session);
        sendResponse = false;
        client.stop();
      }
      else if (!authOK && sessionOK) { //login failed
        strcpy_P(session, PSTR("0")); //invalidate session
        strcpy_P(username, PSTR("")); //reset username
        checkHeartbeat = false;

        http_redirect(client, "/login", NULL);
        sendResponse = false;
        client.stop();
      }
      else if (authOK && sessionOK) { //login successfull

        heartbeatTimeout = millis();
        http_redirect(client, "/", NULL);
        sendResponse = false;
        client.stop();
      }
      else {  //standard redirect
        http_redirect(client, "/", NULL);
        sendResponse = false;
        client.stop();
      }
    }
    else {
      if (http_response(client, 423, NULL)) {
        sendResponse = false;
        client.stop();
      }
    }
  }
  //logout / redirect to index
  else if (sendResponse && authOK && sessionOK && method == GET && strcmp_P(url, PSTR("/logout")) == 0) {
    if (mirolo_active.htmlON) {
      if (http_redirect(client, "/", NULL)) {
        checkHeartbeat = false;
        strcpy_P(session, PSTR("0")); //invalidate session
        strcpy_P(username, PSTR("")); //reset username
        sendResponse = false;
        client.stop();
      }
    }
    else {
      if (http_response(client, 423, NULL)) {
        sendResponse = false;
        client.stop();
      }
    }
  }
  //index.html
  else if (sendResponse && method == GET && strcmp_P(url, PSTR("/")) == 0) {
    if (mirolo_active.htmlON) {
      if (http_response(client, NULL, generateHTMLindex)) {
        sendResponse = false;
        client.stop();

        if (authOK && sessionOK) {

          //reset sequence numbers
          for (uint8_t i = 0; i < seq_nr_count; i++) {
            seq_numbers[i] = 0;
          }
          //reset display (copy all values to the inactive struct)
          memcpy ( &mirolo, &mirolo_active, sizeof(miroloHW) );
        }

      }
    }
    else {
      if (http_response(client, 423, NULL)) {
        sendResponse = false;
        client.stop();
      }
    }
  }
  //mirolo.ajax
  else if (sendResponse && method == POST && strcmp_P(url, PSTR("/mirolo.ajax")) == 0) {
    if (mirolo_active.htmlON) {
      if (authOK && sessionOK) {
        heartbeatTimeout = millis();
      }
      if (http_response(client, NULL, generateAJAXreply)) {
        sendResponse = false;
        client.stop();
      }
    }
    else {
      if (http_response(client, 423, NULL)) {
        sendResponse = false;
        client.stop();
      }
    }
  }
  //OPTIONS (cors) api call
  else if (sendResponse && method == OPTIONS && strstr_P(url, PSTR("/api/")) > NULL) {
    if (mirolo_active.restON && strcmp_P(session, PSTR("0")) == 0) {
      if (http_response(client, 200, NULL)) {
        sendResponse = false;
        client.stop();
      }
    }
    else {
      if (http_response(client, 423, NULL)) {
        sendResponse = false;
        client.stop();
      }
    }
  }
  //REST api calls
  else if (sendResponse && strstr_P(url, PSTR("/api/")) > NULL) {
    if (mirolo_active.restON && strcmp_P(session, PSTR("0")) == 0) {
      if (http_response(client, NULL, generateAPIreply)) {
        sendResponse = false;
        client.stop();
      }
    }
    else {
      if (http_response(client, 423, NULL)) {
        sendResponse = false;
        client.stop();
      }
    }
  }
  //http 404 Not Found
  else if (sendResponse) {
    if (http_response(client, 404, NULL)) {
      sendResponse = false;
      client.stop();
    }
  }

  //heartbeat timeout (clear session in case of connection timeout)
  if (checkHeartbeat && millis() - heartbeatTimeout >= 35000) {
    checkHeartbeat = false;
    strcpy_P(session, PSTR("0")); //invalidate session
    strcpy_P(username, PSTR("")); //reset username
  }






  //enable/disable display and update display

  if (mirolo.DSP.pwr == true && testMode == false) { //stop display updater if testMode is running

    //reset state after powerup
    if (mirolo_active.DSP.pwr == false) {
      enableMatrix();
    }
    /*
      static unsigned long t = 0;
      static uint8_t c1 = 0;
      {

      static unsigned int a = 0;
      a+= millis()-t;
      t = millis();
      c1++;
      if(c1 == 255){
      a/=c1;
      Serial.print("time0: ");
      Serial.println(a);

      a=0;
      }
      }*/

    static uint8_t last_line = 0;
    //scroll max line0
    static uint32_t max_line0_scroll = 0;
    if (last_line == 0 && millis() - max_line0_scroll >= mirolo_active.DSP.scrolldelay0) {
      displayText(0);  // display line 0
      max_line0_scroll = millis();
    }
    last_line++;
    /*
      {

      static unsigned int a = 0;
      a+= millis()-t;
      t = millis();
      if(c1 == 255){
        a/=c1;
        Serial.print("time1: ");
        Serial.println(a);

        a=0;
      }
      }*/
    //scroll max line1
    static uint32_t max_line1_scroll = 0;
    if (last_line == 1 && millis() - max_line1_scroll >= mirolo_active.DSP.scrolldelay1) {
      displayText(1);  // display line 1
      max_line1_scroll = millis();
    }

    last_line++;
    last_line %= max_linecount;
    /*
        {

      static unsigned int a = 0;
      a+= millis()-t;
      t = millis();
      if(c1 == 255){
        a/=c1;
        c1=0;
        Serial.print("time2: ");
        Serial.println(a);

        a=0;
      }
      }*/

    //ws2812 ring animation
    static uint32_t frame_anim_time = 0;
    static uint32_t anim_timer = 0;
    static int8_t anim_counter = 0;

    //off
    if (animation_state == 0) {
      for (int i = 0; i < 16; i++) {
        ringSetPixelColor(i, 0, ring_start);
      }
    }
    //on
    else if (animation_state == 1) {
      for (int i = 0; i < 16; i++) {
        ringSetPixelColor(i, mirolo_active.DSP.color, ring_start);
      }
    }
    //blink
    else if (animation_state == 2) {

      if (millis() - anim_timer >= 1000) {
        if (anim_counter == 0) {
          for (uint8_t i = 0; i < ring.led.numPixels(); i++) {
            ringSetPixelColor(i, mirolo_active.DSP.color, ring_start);
          }
          anim_counter = 1;
        }
        else {
          for (uint8_t i = 0; i < ring.led.numPixels(); i++) {
            ringSetPixelColor(i, 0, ring_start);
          }
          anim_counter = 0;
        }

        ring.forceUpdate(); //LED colors have changed. Update the string immediately
        anim_timer = millis();
      }
    }
    //pulse
    else if (animation_state == 3) {

      if (millis() - anim_timer >= 80) {

        byte red = (mirolo_active.DSP.color & 0xFF0000) >> 16;
        byte green = (mirolo_active.DSP.color & 0x00FF00) >> 8;
        byte blue = (mirolo_active.DSP.color & 0x0000FF);

        red = (red * abs(anim_counter)) >> 8;
        green = (green * abs(anim_counter)) >> 8;
        blue = (blue * abs(anim_counter)) >> 8;

        for (uint8_t i = 0; i < ring.led.numPixels(); i++) {
          ringSetPixelColor(i, red, green, blue, ring_start);
        }

        anim_counter += 8;

        ring.forceUpdate(); //LED colors have changed. Update the string immediately
        anim_timer = millis();
      }
    }
    //load
    else if (animation_state == 4) {

      if (millis() - anim_timer >= 120) {

        if (anim_counter < 0 || anim_counter > (int8_t)ring.led.numPixels() - 1) anim_counter = 0;

        byte red = (mirolo_active.DSP.color & 0xFF0000) >> 16;
        byte green = (mirolo_active.DSP.color & 0x00FF00) >> 8;
        byte blue = (mirolo_active.DSP.color & 0x0000FF);
        uint8_t dim = 250;

        for (uint8_t i = anim_counter + ring.led.numPixels(); i > anim_counter; i--) {
          ringSetPixelColor(i % ring.led.numPixels(), red, green, blue, ring_start);

          red = (red * dim) >> 8;
          green = (green * dim) >> 8;
          blue = (blue * dim) >> 8;

          if (dim != 0) dim -= 50;
        }

        anim_counter++;

        ring.forceUpdate(); //LED colors have changed. Update the string immediately
        anim_timer = millis();
      }
    }
    //api frame animation
    else if (animation_state == 5) {
      if (millis() - anim_timer >= frame_anim_time) {

        if (mirolo_active_animation.CurrentFrame <= mirolo_active_animation.LastFrame) {
          frame_anim_time = mirolo_active_animation.Frames[mirolo_active_animation.CurrentFrame].Time;

          for (uint8_t i = 0; i < ring.led.numPixels(); ++i) {
            ringSetPixelColor(i, mirolo_active_animation.Frames[mirolo_active_animation.CurrentFrame].Colors[i], ring_start);
          }

          ++mirolo_active_animation.CurrentFrame;
          ++mirolo_animation.CurrentFrame; //not needed. only to keep both structs in sync for memcmp() to work
          //loop animation?
          if (mirolo_active_animation.CurrentFrame > mirolo_active_animation.LastFrame && mirolo_active_animation.Loop == true) {
            mirolo_active_animation.CurrentFrame = 0;
            mirolo_animation.CurrentFrame = 0; //not needed. only to keep both structs in sync for memcmp() to work
          }
        }
        //animation ended. no loop. blank ring
        else {
          for (uint8_t i = 0; i < ring.led.numPixels(); ++i) {
            ringSetPixelColor(i, BLACK, ring_start);
          }
          frame_anim_time = 500;
        }
        ring.forceUpdate(); //LED colors have changed. Update the string immediately
        anim_timer = millis();
      }
    }


    //automatic brightness control
    static uint8_t former_brightness = 0xFF;

    if (mirolo_active.DSP.brightness == 16 && former_brightness != map(lightsensor.value, 10, 1010, 0, 15)) {
      //Serial.println(lightsensor.value / (1024 / 16));
      max_Line[0].mx.control(MD_MAX72XX::INTENSITY, map(lightsensor.value, 10, 1010, 0, 15));
      max_Line[1].mx.control(MD_MAX72XX::INTENSITY, map(lightsensor.value, 10, 1010, 0, 15));
      ring.setBrightness(map(lightsensor.value, 10, 1010, 30, 100));
      former_brightness = map(lightsensor.value, 10, 1010, 0, 15);
    }
    else if (mirolo_active.DSP.brightness != 16) {
      former_brightness = 0xFF; //make sure brightness method above is called at least once when entering auto mode
    }

  }
  else if (testMode == false) {
    disableMatrix();

    ring.clearLEDs();
  }

  //update display
  if (mirolo.DSP.apply_changes) {
    //settingsChanged = true; //update eeprom | BUG: Not compatible with rapid changing displays like clock. disabled for now. might have side effects

    if (mirolo.DSP.apply_changes & MEMCPY) {
      //copy
      memcpy(&mirolo_active, &mirolo, sizeof(miroloHW));
    }

    if (mirolo.DSP.apply_changes & LINE0) {
      //copy
      memcpy(mirolo_active.DSP.message0, mirolo.DSP.message0, max_bufsize);
      memcpy(mirolo_active.DSP.prefix0, mirolo.DSP.prefix0, dsp_prefix_size);
      mirolo_active.DSP.scrolldelay0 = mirolo.DSP.scrolldelay0;

      //copy line messages
      if (strcmp(max_Line[0].message, mirolo_active.DSP.message0) != 0) {
        strcpy(max_Line[0].message, mirolo_active.DSP.message0);
        max_Line[0].newMessageAvailable = true;
      }

      //static divider (prefix)
      uint8_t prefix0 = atoi(mirolo_active.DSP.prefix0); //returns 0 if string is invalid ("off")
      if (max_Line[0].staticDivider != prefix0) {
        max_Line[0].newMessageAvailable = true;
        max_Line[0].staticDivider = prefix0;
      }
    }

    if (mirolo.DSP.apply_changes & LINE1) {
      //copy
      memcpy(mirolo_active.DSP.message1, mirolo.DSP.message1, max_bufsize);
      memcpy(mirolo_active.DSP.prefix1, mirolo.DSP.prefix1, dsp_prefix_size);
      mirolo_active.DSP.scrolldelay1 = mirolo.DSP.scrolldelay1;

      //copy line messages
      if (strcmp(max_Line[1].message, mirolo_active.DSP.message1) != 0) {
        strcpy(max_Line[1].message, mirolo_active.DSP.message1);
        max_Line[1].newMessageAvailable = true;
      }

      //static divider (prefix)
      uint8_t prefix1 = atoi(mirolo_active.DSP.prefix1); //returns 0 if string is invalid ("off")

      if (max_Line[1].staticDivider != prefix1) {
        max_Line[1].newMessageAvailable = true;
        max_Line[1].staticDivider = prefix1;
      }

    }

    if (mirolo.DSP.apply_changes & RING0) {
      //prepare
      memset(mirolo.DSP.animation, 0, dsp_animation_size);
      strcpy_P(mirolo.DSP.animation, PSTR("external"));
      mirolo_animation.CurrentFrame = 0;
      mirolo_active_animation.CurrentFrame = 0; //not needed. only to keep both structs in sync for memcmp() to work

      //copy
      memcpy(&mirolo_active_animation, &mirolo_animation, sizeof(mirolo_animation));
      memcpy(mirolo_active.DSP.animation, mirolo.DSP.animation, dsp_animation_size);
    }

    if (mirolo.DSP.apply_changes & BRIGHT) {
      //copy
      mirolo_active.DSP.brightness = mirolo.DSP.brightness;

      //brightness
      if (mirolo_active.DSP.brightness < 16) {
        max_Line[0].mx.control(MD_MAX72XX::INTENSITY, mirolo_active.DSP.brightness);
        max_Line[1].mx.control(MD_MAX72XX::INTENSITY, mirolo_active.DSP.brightness);
        ring.setBrightness(map(mirolo_active.DSP.brightness, 0, 15, 30, 100));
      }
    }

    if (mirolo.DSP.apply_changes & NAME) {
      //copy display name
      memcpy(mirolo_active.Name, mirolo.Name, dsp_name_size);
    }

    if (mirolo.DSP.apply_changes & ANIM) {
      //copy
      memcpy(mirolo_active.DSP.animation, mirolo.DSP.animation, dsp_animation_size);
      mirolo_active.DSP.color = mirolo.DSP.color;

      //ws2812 ring animation
      if (strcmp_P(mirolo_active.DSP.animation, PSTR("off")) == 0) animation_state = 0;
      else if (strcmp_P(mirolo_active.DSP.animation, PSTR("on")) == 0) animation_state = 1;
      else if (strcmp_P(mirolo_active.DSP.animation, PSTR("blink (1Hz)")) == 0) animation_state = 2;
      else if (strcmp_P(mirolo_active.DSP.animation, PSTR("pulse")) == 0) animation_state = 3;
      else if (strcmp_P(mirolo_active.DSP.animation, PSTR("loading")) == 0) animation_state = 4;
    }

    //changes applied. reset
    mirolo_active.DSP.apply_changes = 0;
    mirolo.DSP.apply_changes = 0;
  }

  //apply pwr setting immediately
  mirolo_active.DSP.pwr = mirolo.DSP.pwr;

  //apply external api animation immediately
  if (strcmp_P(mirolo_active.DSP.animation, PSTR("external")) == 0) animation_state = 5;

  //apply service setting immediately
  mirolo_active.htmlON = mirolo.htmlON;
  mirolo_active.restON = mirolo.restON;


  /*
    //enable/disable display
    if(mirolo_active.DSP.pwr == true){
      max_Line[0].mx.control(MD_MAX72XX::UPDATE, MD_MAX72XX::ON);
      max_Line[1].mx.control(MD_MAX72XX::UPDATE, MD_MAX72XX::ON);

      for(int i = 0; i<16; i++){
        ring.setPixelColor(i, mirolo_active.DSP.colors[i]);
      }
    }
    else{
      max_Line[0].mx.control(MD_MAX72XX::UPDATE, MD_MAX72XX::OFF);
      max_Line[1].mx.control(MD_MAX72XX::UPDATE, MD_MAX72XX::OFF);

      clearLEDs(ring);
    }
  */


  // This subroutine call internal tick() procedure, that need for ENC28J60 buffers processing by UIPEthernet. You can use it if not sure that other UIPEthernet's subroutines calls tick() periodically.
  Ethernet.maintain();
  checkEthernet();

  //receive infrared codes
  infrared.Loop();

  //update rgb LED ring
  ring.Loop();

  //button inputs
  s1.Update();
  s2.Update();

  //lightsensor
  lightsensor.Loop();

  //system led
  led1.Loop();
  led2.Loop();

#if LOAD_MODULE == 1 || LOAD_MODULE == 2
  //event scheduler
  scheduler.Loop();
#endif

  //update EEPROM if necessary
  static uint32_t timer_eeprom = 0;
  static uint8_t eeprom_counter = 0;
  if (settingsChanged == true) {
    settingsChanged = false;
    eeprom_counter++; //safety just in case there is something constantly updating faster than the 10s delay
    if (eeprom_counter < 100) timer_eeprom = millis() - (300000UL - 10000); //update settings immediately 10s after last change request
  }
  if (millis() - timer_eeprom >= 300000UL) {
    updateEEPROM();
    eeprom_counter = 0;
    timer_eeprom = millis();
  }

  /*
    {
    static uint8_t c1 = 0;
    static unsigned long t = 0;
    static unsigned int a = 0;
    a+= millis()-t;
    t = millis();
    c1++;
    if(c1 == 255){
      a/=c1;
      c1=0;
      Serial.print("timeLoop: ");
      Serial.println(a);

      a=0;
    }
    }
  */

  //reset watchdog timer
  wdt_reset();


}

















// ******* functions


bool readSerial(char * inputBuffer, uint16_t bufferSize) { //reads message from Serial input. Messages have to end with \n. returns true on NewMessageAvailable
  static uint16_t bufferIndex = 0;
  static bool readComplete = false;
  static char inChar[2] = {'\0'};

  if (readComplete) { //reset
    strcpy_P(inputBuffer, PSTR(""));
    bufferIndex = 0;
    readComplete = false;
  }

  while (Serial.available() > 0 && readComplete == false && bufferIndex < bufferSize - 1) {

    // read the bytes incoming from the client:
    inChar[0] = (char)Serial.read();
    ++bufferIndex;

    if (inChar[0] == 10) { //stop reading if NewLine is sent (ASCII 10, '\n' or ctrl-J)
      readComplete = true;
    }
    else if (bufferIndex >= bufferSize - 1) { //always end inputBuffer with '\n'
      inChar[0] = '\n';
      readComplete = true;
    }


    strcat(inputBuffer, inChar); //add the next inChar to the string (concat)


  }

  return readComplete;
}





boolean checkEthernet() { //check if enc28J60 is still responding and re-Init if device has stopped working
  static uint32_t timer_etherCheck = 0;

  if (millis() - timer_etherCheck >= 5000) {
    // Enc28J60 is Enc28J60Network class that defined in Enc28J60Network.h
    // readReg() subroutine must be moved from private to public members area in utility\Enc28J60Network.h
    // ENC28J60 ignore all incoming packets if ECON1.RXEN is not set
    uint8_t stateEconRxen = Enc28J60Network::readReg((uint8_t) NET_ENC28J60_ECON1) & NET_ENC28J60_ECON1_RXEN;
    // ESTAT.BUFFER rised on TX or RX error
    // I think the test of this register is not necessary - EIR.RXERIF state checking may be enough
    uint8_t stateEstatBuffer = Enc28J60Network::readReg((uint8_t) NET_ENC28J60_ESTAT) & NET_ENC28J60_ESTAT_BUFFER;
    // EIR.RXERIF set on RX error
    uint8_t stateEirRxerif = Enc28J60Network::readReg((uint8_t) NET_ENC28J60_EIR) & NET_ENC28J60_EIR_RXERIF;
    if (!stateEconRxen || (stateEstatBuffer && stateEirRxerif)) {
      Serial.println ("ENC28J60 Hang State detected. Restarting module");
      uint8_t tmpMac[6] = {0};
      memcpy_P (tmpMac, mac, sizeof(tmpMac));
      Enc28J60Network::init(tmpMac);
    }
    //Stop module testing for defined time
    timer_etherCheck = millis();
  }
}



void disableMatrix() {
  for (uint8_t i = 0; i < max_linecount; ++i) {
    max_Line[i].mx.clear();
    max_Line[i].mx.control(MD_MAX72XX::UPDATE, MD_MAX72XX::OFF);

  }
}


void enableMatrix() {
  for (uint8_t i = 0; i < max_linecount; ++i) {
    max_Line[i].mx.control(MD_MAX72XX::UPDATE, MD_MAX72XX::ON);
    max_Line[i].newMessageAvailable = true;

  }
}




void ringBootAnimate(const uint8_t &pixel0, const uint8_t &pixel1, const uint32_t &color) { //helper function for boot animation
  ringSetPixelColor(pixel0, color, ring_start);
  ringSetPixelColor(pixel1, color, ring_start);
  ring.led.show();
  delay(200);
}


void lineBootAnimate(const __FlashStringHelper * message, const uint8_t &line) { //helper function for boot text display
  strcpy_P(max_Line[line].message, (const char*)message);
  max_Line[line].newMessageAvailable = true;
  displayText(line);
}

void lineBootAnimate(const char * message, const uint8_t &line) { //helper function for boot text display
  strcpy(max_Line[line].message, message);
  max_Line[line].newMessageAvailable = true;
  displayText(line);
}


void setLedState() {
  if (mirolo.htmlON) led1.setLED(true);
  else led1.setLED(false);
  if (mirolo.restON) led2.setLED(true);
  else led2.setLED(false);
}


void clearMiroloHW(const uint8_t &changes) {
  strcpy_P(mirolo.DSP.message0, PSTR(""));
  strcpy_P(mirolo.DSP.prefix0, PSTR("off"));
  strcpy_P(mirolo.DSP.message1, PSTR(""));
  strcpy_P(mirolo.DSP.prefix1, PSTR("off"));
  strcpy_P(mirolo.DSP.animation, PSTR("off"));
  mirolo.DSP.apply_changes |= changes;
}



void setDisplayTest(const bool &state) {
  static uint8_t brightness = 0;
  if (state) {
    testMode = true;
    brightness = ring.getBrightness();
    ring.setBrightness(100);
    disableMatrix();
    led1.setLED(true, 300, 300);
    led2.setLED(true, 300, 300);
  }
  else {
    testMode = false;
    ring.setBrightness(brightness);
    for (uint8_t i = 0; i < max_linecount; ++i) {
      max_Line[i].mx.control(MD_MAX72XX::TEST, MD_MAX72XX::OFF);
    }
    setLedState();
    enableMatrix();
  }
}


void testDisplay() {
  static uint8_t count_line[max_linecount] = {0};
  static uint8_t count_ring = 0;

  //test lines (light up modules one after another)
  for (uint8_t i = 0; i < max_linecount; ++i) {
    uint8_t previous = count_line[i] == 0 ? max_Line[i].mx.getDeviceCount() - 1 : count_line[i] - 1;
    max_Line[i].mx.control(previous, previous, MD_MAX72XX::TEST, MD_MAX72XX::OFF);
    max_Line[i].mx.control(count_line[i], count_line[i], MD_MAX72XX::TEST, MD_MAX72XX::ON);
    if (++count_line[i] > max_Line[i].mx.getDeviceCount()) count_line[i] = 0;
  }

  //test ring (show rgb + w + cmy colors)
  uint32_t color = 0;
  switch (count_ring++) {
    case 0:
      color = RED;
      break;
    case 1:
      color = GREEN;
      break;
    case 2:
      color = BLUE;
      break;
    case 3:
      color = WHITE;
      break;
    case 4:
      color = CYAN;
      break;
    case 5:
      color = MAGENTA;
      break;
    case 6:
      color = YELLOW;
      count_ring = 0;
      break;
  }

  //apply color
  for (uint16_t i = 0; i < ring.led.numPixels(); i++) {
    ringSetPixelColor(i, color, ring_start);
  }
  ring.forceUpdate();
}


void ringSetPixelColor(const uint16_t & pixel, const uint32_t & color, const int8_t & start) { //wrapper for clockwise and counter clockwise oriented rings
  if (start > 0) {
    ring.led.setPixelColor((pixel + (start - 1)) % ring.led.numPixels(), color);
  }
  else if (start < 0) {
    ring.led.setPixelColor((ring.led.numPixels() - (pixel + (abs(start) - 1))) % ring.led.numPixels(), color);
  }
}


void ringSetPixelColor(const uint16_t & pixel, const uint8_t & red, const uint8_t & green, const uint8_t & blue, const int8_t & start) { //wrapper for clockwise and counter clockwise oriented rings
  if (start > 0) {
    ring.led.setPixelColor((pixel + (start - 1)) % ring.led.numPixels(), red, green, blue);
  }
  else if (start < 0) {
    ring.led.setPixelColor((ring.led.numPixels() - (pixel + (abs(start) - 1))) % ring.led.numPixels(), red, green, blue);
  }
}
