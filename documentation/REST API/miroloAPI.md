## mirolo 2M05081R16 REST-api Documentation

All Calls to the device's api must be done using the JSON format.
The displays use a latching data concept for displaying information:

GET requests can be performed at any time.
POST requests have an immediate effect on the display.
PATCH requests are stored in the "pending" buffer and have to be confirmed by making a POST request to /api/apply to transfer all changes to the "active" buffer and make them visible. Fields can be updated independently by only provoding data fields that have to be changed.


## GET Requests

**GET** /api/line0   /api/line1
	
reply:

	{
	"data": {
	    "pending": {
	        "message": "test 1234",
	        "messageok": true,	[true if the message contains displayable characters (ASCII)]
	        "prefix": 4,
	        "prefixok": true,	[true if the prefix could be applied]
	        "speed": 75,
	        "scrolling": true,	[true if the message or parts of it is scrolling]
	        "roundtriptime": 2490	[time in ms needed for scrolling the message once. 0 if message is static]
	     },
             "active": {
                "message": "",
                "messageok": true,	[true if the message contains displayable characters (ASCII)]
                "prefix": 0,
                "prefixok": true,	[true if the prefix could be applied]
                "speed": 80,
                "scrolling": false,	[true if the message or parts of it is scrolling]
                "roundtriptime": 0	[time in ms needed for scrolling the message once. 0 if message is static]
            }
	},
	"status": 200
	}


**GET** /api/ring0

reply:

	{
    	"data": {
    	    "pending": {
	        "framelength": 2,		[number of active frames]
            	"loop": true,         		[true if the animation loops the frames]
            	"roundtriptime": 520		[overall animation runtime in ms]
	    },
	    "active": {
	        "framelength": 5,		[number of active frames]
            	"loop": true,         		[true if the animation loops the frames]
            	"roundtriptime": 890		[overall animation runtime in ms]
	    }          
        },
        "status": 200
	}


**GET** /api/brightness

reply:

	{
	"data": {
	    "pending": {
	        "brightness": 3		[0-16, 16=auto]
	    },
	    "active": {
		"brightness": 8		[0-16, 16=auto]
	    }
	},
    	"status": 200
	}


**GET** /api/ldr

reply:

	{
	"data": {
	    "ldr": 463			[0-1023]
	},
    	"status": 200
	}


**GET** /api/ir

reply:

	{
	"data": {
	    "codes": [
                2155859760,		[the last 10 IR codes received by this display - latest at index 0]
                2155813095,		[GET request clears this array!]
                3412879762,
                2155868175,
                2155856445,
                2155841655,
                2155868175,
                2155833495,
                2155831965,
                2155847775
            ]
	},
    	"status": 200
	}


**GET** /api/info

reply:

	{
	"data": {
	    "hardware": "mirolo 2M05081R16",
            "processor": "Atmega1284p@20MHz",
            "serialnumber": "2017.10.0001",
            "firmware": "Jun  3 2018 | 19:19:51",
            "ip": "192.168.130.9",
            "mac": "20-dc-93-3b-9b-0c"
	},
    	"status": 200
	}


**GET** /api/name

reply:

	{
	"data": {
	    "name": "Panel Room 2"
	},
    	"status": 200
	}

	
**GET** /api/apply

reply:

	{
	"data": {
	    "apply": true		[true if all changes have been applied, false if "active" and "pending" are different]
	},
    	"status": 200
	}
	
	
**GET** /api/power

reply:

	{
	"data": {
	    "enable": true		[true if the display is powered on (showing data)]
	},
    	"status": 200
	}
	
	
## PATCH Requests

**PATCH** /api/line0 /api/line1

request:

	{
    	"message": "test 1234", 	[280 characters max]
    	"prefix": 4,			[0=off]
    	"speed": 75			[20-255]
	}
	
	
reply:

	{
    	"data": {
            "pending": {
                "message": "test 1234",
                "messageok": true,	[true if the message contains displayable characters (ASCII)]
                "prefix": 4,
                "prefixok": true,	[true if the prefix could be applied]
                "speed": 75,
                "scrolling": true,	[true if the message or parts of it is scrolling]
                "roundtriptime": 2490	[time in ms needed for scrolling the message once. 0 if message is static]
             },
             "active": {
                "message": "",		[true if the message contains displayable characters (ASCII)]
                "messageok": true,
                "prefix": 0,
                "prefixok": true,	[true if the prefix could be applied]
                "speed": 75,
                "scrolling": false,	[true if the message or parts of it is scrolling]
                "roundtriptime": 0	[time in ms needed for scrolling the message once. 0 if message is static]
            }
        },
        "status": 200
	}
	
	
**PATCH** /api/ring0			[frames will be added subsequently (25 max). To apply the animation send the last frame as POST or send POST apply]

request:

	{				[optional. omit frame in PATCH request to start over (revert)]
    	"frame": {			
    	    "colors": [
    	        "#cc99ee",		[16 RGB colors clockwise, #RRGGBB: 0-255 per channel, pixel 0 is at 12 o' clock]
    	        "#cc99ee",		
    	        "#cc99ee",
    	        "#cc99ee",
    	        "#cc99ee",
    	        "#cc99ee",
    	        "#cc99ee",
    	        "#cc99ee",
    	        "#cc99ee",
    	        "#cc99ee",
    	        "#cc99ee",
    	        "#cc99ee",
    	        "#cc99ee",
    	        "#cc99ee",
    	        "#cc99ee",
    	        "#cc99ee"
    	    ],
    	    "time": 260  	   	[frame display time in ms, 20-MAX_UINT32]
    	},
	"loop": true			[true if the animation loops the frames]
	}
	
	
reply:

	{
    	"data": {
    	    "pending": {
	        "framelength": 2,		[number of active frames]
            	"loop": true,         		[true if the animation loops the frames]
            	"roundtriptime": 520		[overall animation runtime in ms]
	    },
	    "active": {
	        "framelength": 5,		[number of active frames]
            	"loop": true,         		[true if the animation loops the frames]
            	"roundtriptime": 890		[overall animation runtime in ms]
	    }          
        },
        "status": 200
	}


**PATCH** /api/brightness

request:

	{
    	"brightness": 5			[0-16, 16=auto]
	}

reply:

	{
	"data": {
	    "pending": {
	        "brightness": 3		[0-16, 16=auto]
	    },
	    "active": {
		"brightness": 8		[0-16, 16=auto]
	    }
	},
    	"status": 200
	}
	



## POST Requests

**POST** /api/line0 /api/line1

request:

	{
    	"message": "test 1234", 	[280 characters max]
    	"prefix": 4,			[0=off]
    	"speed": 75			[20-255]
	}
	
	
reply:

	{
    	"data": {
            "pending": {
                "message": "test 1234",
                "messageok": true,	[true if the message contains displayable characters (ASCII)]
                "prefix": 4,
                "prefixok": true,	[true if the prefix could be applied]
                "speed": 75,
                "scrolling": true,	[true if the message or parts of it is scrolling]
                "roundtriptime": 2490	[time in ms needed for scrolling the message once. 0 if message is static]
             },
             "active": {
                "message": "test 1234",
                "messageok": true,	[true if the message contains displayable characters (ASCII)]
                "prefix": 4,
                "prefixok": true,	[true if the prefix could be applied]
                "speed": 75,
                "scrolling": true,	[true if the message or parts of it is scrolling]
                "roundtriptime": 2490	[time in ms needed for scrolling the message once. 0 if message is static]
            }
        },
        "status": 200
	}
	
	
**POST** /api/ring0			[frame will be applied immediately together with all frames formerly sent as PATCH]

request:

	{
    	"frame": {	 		[optional]
    	    "colors": [
    	        "#cc99ee",		[16 RGB colors clockwise, #RRGGBB: 0-255 per channel, pixel 0 is at 12 o' clock]
    	        "#cc99ee",		
    	        "#cc99ee",
    	        "#cc99ee",
    	        "#cc99ee",
    	        "#cc99ee",
    	        "#cc99ee",
    	        "#cc99ee",
    	        "#cc99ee",
    	        "#cc99ee",
    	        "#cc99ee",
    	        "#cc99ee",
    	        "#cc99ee",
    	        "#cc99ee",
    	        "#cc99ee",
    	        "#cc99ee"
    	    ],
    	    "time": 260  	   	[frame display time in ms, 20-MAX_UINT32]
    	},
	"loop": true			[true if the animation loops the frames]
	}
	
	
reply:

	{
    	"data": {
    	    "pending": {
	        "framelength": 5,		[number of active frames]
            	"loop": true,         		[true if the animation loops the frames]
	        "roundtriptime": 890		[overall animation runtime in ms]
	    },
	    "active": {
	        "framelength": 5,		[number of active frames]
            	"loop": true,         		[true if the animation loops the frames]
            	"roundtriptime": 890		[overall animation runtime in ms]
	    }          
        },
        "status": 200
	}
	
	
**POST** /api/brightness

request:

	{
    	"brightness": 5			[0-16, 16=auto]
	}

reply:

	{
	"data": {
	    "pending": {
	        "brightness": 3		[0-16, 16=auto]
	    },
	    "active": {
		"brightness": 3		[0-16, 16=auto]
	    }
	},
    	"status": 200
	}
	
	
**POST** /api/name

request:

	{
    	"name": "Display Hallway"	[24 characters max]
	}
	
reply:

	{
	"data": {
	    "name": "Display Hallway"
	},
    	"status": 200
	}
	

**POST** /api/power

reply:

	{
	"data": {
	    "enable": true		[true if the display is powered on (showing data)]
	},
    	"status": 200
	}
	

**POST** /api/apply

request:

	none

reply:

	{
	"data": {
	    "apply": true
	},
    	"status": 200
	}

	
**POST** /api/revert

request:

	none

reply:

	{
	"data": {
	    "revert": true
	},
    	"status": 200
	}
