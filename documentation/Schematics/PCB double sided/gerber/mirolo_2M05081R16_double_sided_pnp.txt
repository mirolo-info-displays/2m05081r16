*Pick And Place List
*Company=
*Author=
*eMail=
*
*Project=mirolo_2M05081R16_double_sided
*Date=11:32:19
*CreatedBy=Fritzing 0.9.3b.04.19.5c895d327c44a3114e5fcc9d8260daf0cbb52806
*
*
*Coordinates in mm, always center of component
*Origin 0/0=Lower left corner of PCB
*Rotation in degree (0-360, math. pos.)
*
*No;Value;Package;X;Y;Rotation;Side;Name
1;;;150.393;-28.5371;0;Bottom;Copper Fill337
2;;;94.1961;-31.9363;0;Bottom;Via68
3;;;129.121;-77.775;0;Bottom;Copper Fill284
4;;;32.6702;-79.2776;0;Bottom;Via117
5;;;34.1161;-25.3015;0;Bottom;Via94
6;;;33.1597;-49.6826;0;Bottom;Copper Fill172
7;;;12.7508;-73.1522;0;Bottom;Copper Fill10
8;;;21.1328;-26.2384;0;Bottom;Copper Fill210
9;;;73.8761;-31.9363;0;Bottom;Via78
10;;;12.3444;-14.0972;0;Bottom;Copper Fill157
11;;;61.6585;-40.7926;0;Bottom;Copper Fill282
12;;;11.9126;-48.7555;0;Bottom;Copper Fill233
13;;THT;101.25;-11.9675;0;Bottom;J31
14;;;69.4436;-10.0586;0;Bottom;Copper Fill141
15;;;34.0868;-48.7555;0;Bottom;Copper Fill237
16;;;145.948;-61.5571;0;Bottom;Copper Fill343
17;;;156.337;-67.6658;0;Bottom;Copper Fill5
18;;;28.8036;-10.0586;0;Bottom;Copper Fill139
19;;;12.2936;-31.9915;0;Bottom;Copper Fill243
20;;THT;60.9295;-53.9182;0;Bottom;LDR
21;;;3.2512;-90.6274;0;Bottom;Copper Fill1
22;;;40.2336;-37.6938;0;Bottom;Copper Fill56
23;;;140.487;-27.356;0;Bottom;Copper Fill93
24;;;102.87;-41.3387;0;Bottom;Copper Fill313
25;;;99.3394;-18.3644;0;Bottom;Copper Fill111
26;;;131.712;-15.4688;0;Bottom;Copper Fill346
27;;;53.5561;-31.9363;0;Bottom;Via88
28;;;73.3044;-14.0972;0;Bottom;Copper Fill160
29;;;29.3624;-27.7624;0;Bottom;Copper Fill85
30;;;129.121;-75.8192;0;Bottom;Copper Fill285
31;;;108.153;-26.2384;0;Bottom;Copper Fill218
32;;;8.1534;-12.37;0;Bottom;Copper Fill128
33;;;122.707;-21.8442;0;Bottom;Copper Fill105
34;;;7.9756;-90.475;0;Bottom;Copper Fill2
35;;;98.7806;-12.751;0;Bottom;Copper Fill137
36;;;92.2528;-26.2384;0;Bottom;Copper Fill217
37;;;94.178;-48.7023;0;Bottom;Via69
38;;;49.1236;-10.0586;0;Bottom;Copper Fill140
39;;;10.3378;-74.2063;0;Bottom;Copper Fill306
40;;;131.661;-68.377;0;Bottom;Copper Fill300
41;;cap-pth-small-kit;127.793;-76.7375;0;Bottom;C4
42;100 nF;0805 [SMD, multilayer];96.7299;-77.7946;90;Bottom;C9
43;;;71.826;-35.6768;0;Bottom;Via76
44;;;11.938;-35.23;0;Bottom;Copper Fill62
45;;THT;86.0103;-11.9675;0;Bottom;J32
46;;;27.6005;-72.6238;0;Bottom;TXT1
47;10k;0805 [SMD];44.7965;-36.6797;90;Top;R18
48;;;7.1628;-14.0972;0;Bottom;Copper Fill123
49;;;32.6136;-31.9915;0;Bottom;Copper Fill262
50;;;12.8905;-31.42;0;Bottom;Copper Fill178
51;;;72.0598;-56.4771;0;Bottom;Copper Fill257
52;;;126.65;-31.0174;0;Bottom;Via49
53;;;70.3072;-8.2298;0;Bottom;Copper Fill146
54;;;19.9517;-77.7242;0;Bottom;Copper Fill185
55;;;65.6905;-61.4974;0;Bottom;Via57
56;;;54.4068;-48.7555;0;Bottom;Copper Fill250
57;;;91.0209;-85.7506;0;Bottom;Copper Fill220
58;;;157.308;-3.07485;0;Bottom;Via59
59;;;38.5239;-29.35;0;Bottom;TXT2
60;;;52.9336;-31.9915;0;Bottom;Copper Fill279
61;;;41.9354;-41.3387;0;Bottom;Copper Fill266
62;;;144.43;-71.6576;0;Bottom;Via32
63;;;108.547;-77.775;0;Bottom;Copper Fill184
64;;;92.9386;-58.8266;0;Bottom;Copper Fill23
65;;;77.47;-3.9372;0;Bottom;Copper Fill155
66;;;131.73;-74.1976;0;Bottom;Via29
67;;tqfp44;119.03;-59.6776;-90;Bottom;Atmega 1284p
68;;;31.1722;-37.6959;0;Bottom;Via97
69;;;4.7117;-80.4674;0;Bottom;Copper Fill316
70;;;91.0902;-84.3575;0;Bottom;Via26
71;;;144.43;-13.2376;0;Bottom;Via43
72;;;114.071;-16.3959;0;Bottom;Copper Fill201
73;;;120.371;-36.6524;0;Bottom;Copper Fill33
74;;;81.9785;-40.7926;0;Bottom;Copper Fill295
75;;;108.968;-73.2622;0;Bottom;Via34
76;;;156.466;-56.4176;0;Bottom;Via36
77;;;116.421;-80.95;0;Bottom;Copper Fill177
78;;;108.737;-36.0936;0;Bottom;Copper Fill52
79;;;108.801;-32.0042;0;Bottom;Copper Fill323
80;;;113.95;-38.6374;0;Bottom;Via50
81;;;33.7566;-31.9915;0;Bottom;Copper Fill263
82;;so-24dw;93.6303;-36.7579;-90;Bottom;U6
83;;;73.7997;-31.42;0;Bottom;Copper Fill291
84;;;33.2361;-31.9363;0;Bottom;Via98
85;;;95.0761;-25.3015;0;Bottom;Via63
86;;THT;19.9703;-11.9675;0;Bottom;J40
87;;;21.0693;-41.9102;0;Bottom;Copper Fill245
88;;so-24dw;32.6703;-36.7579;-90;Bottom;U10
89;;;37.8206;-12.751;0;Bottom;Copper Fill134
90;;THT;25.0503;-11.9675;0;Bottom;J37
91;;;107.874;-41.2371;0;Bottom;Copper Fill321
92;;;129.032;-47.5236;0;Bottom;Copper Fill31
93;;;108.547;-77.775;0;Bottom;Copper Fill273
94;;;108.801;-42.1642;0;Bottom;Copper Fill320
95;;;100.772;-60.4497;0;Bottom;Via55
96;;;90.6272;-8.2298;0;Bottom;Copper Fill147
97;;;16.51;-3.9372;0;Bottom;Copper Fill152
98;100µF;200 mil [THT, electrolytic];116.49;-84.3575;-90;Bottom;C3
99;6.8k;THT;90.0918;-51.2867;180;Bottom;R11
100;;;90.3224;-27.7624;0;Bottom;Copper Fill91
101;;;91.0209;-83.109;0;Bottom;Copper Fill221
102;;;115.049;-21.9204;0;Bottom;Copper Fill329
103;;;23.9522;-79.3879;0;Bottom;Copper Fill269
104;;;12.2682;-74.2063;0;Bottom;Copper Fill195
105;;;6.2992;-48.8571;0;Bottom;Copper Fill170
106;;;41.4109;-41.2928;0;Bottom;Via101
107;;;87.8046;-22.9486;0;Bottom;Via60
108;;;57.15;-3.9372;0;Bottom;Copper Fill154
109;;diode-1n4148;94.6568;-72.9378;-90;Bottom;D26
110;;;53.4797;-49.6826;0;Bottom;Copper Fill248
111;;;116.538;-48.7849;0;Bottom;Via112
112;;;53.4416;-28.9562;0;Bottom;Copper Fill79
113;;;155.905;-57.8614;0;Bottom;Copper Fill24
114;;;89.7128;-84.4171;0;Bottom;Copper Fill222
115;;THT;148.367;-14.5075;0;Bottom;Serial
116;;;132.461;-69.1771;0;Bottom;Copper Fill302
117;;THT;126.65;-39.9075;0;Bottom;LAN1
118;;;124.11;-3.07739;0;Bottom;Via58
119;;;101.194;-37.6938;0;Bottom;Copper Fill59
120;;;13.7961;-25.3015;0;Bottom;Via104
121;;so-24dw;12.3503;-36.7579;-90;Bottom;U11
122;;;89.4353;-27.1676;0;Bottom;Via64
123;;;113.95;-41.1774;0;Bottom;Via53
124;;;93.5482;-31.9915;0;Bottom;Copper Fill309
125;;;20.2184;-40.5386;0;Bottom;Copper Fill42
126;;;127.508;-31.0771;0;Bottom;Copper Fill327
127;;;41.3893;-40.7926;0;Bottom;Copper Fill265
128;;;52.5526;-48.7555;0;Bottom;Copper Fill249
129;10k;0805 [SMD];85.4365;-36.6797;90;Top;R16
130;;;8.1534;-48.8571;0;Bottom;Copper Fill231
131;100µF;200 mil [THT, electrolytic];131.752;-13.8011;90;Bottom;C2
132;;THT;4.73033;-11.9675;0;Bottom;J39
133;;;133.147;-41.1736;0;Bottom;Copper Fill68
134;;;113.741;-35.1284;0;Bottom;Copper Fill61
135;;;106.33;-38.6374;0;Bottom;Via51
136;;;149.441;-27.61;0;Bottom;Copper Fill336
137;;;73.7997;-49.6826;0;Bottom;Copper Fill270
138;;;41.4528;-26.2384;0;Bottom;Copper Fill212
139;;;32.6644;-14.0972;0;Bottom;Copper Fill158
140;;;125.146;-35.23;0;Bottom;Copper Fill53
141;;;126.187;-54.9912;0;Bottom;Copper Fill27
142;;;103.721;-82.9058;0;Bottom;Copper Fill238
143;;;97.8916;-39.9671;0;Bottom;Copper Fill199
144;;;46.6344;-84.4171;0;Bottom;Copper Fill227
145;;;112.99;-92.8026;0;Bottom;Hole1
146;;;58.9494;-22.8834;0;Bottom;Via83
147;;;114.998;-15.4942;0;Bottom;Copper Fill333
148;;;68.8086;-36.6524;0;Bottom;Copper Fill208
149;;;103.721;-82.9058;0;Bottom;Copper Fill173
150;;;7.2517;-47.93;0;Bottom;Copper Fill229
151;;;108.87;-41.1774;0;Bottom;Via52
152;;;19.9517;-75.87;0;Bottom;Copper Fill186
153;;;38.3794;-18.3644;0;Bottom;Copper Fill108
154;;;69.1134;-12.37;0;Bottom;Copper Fill131
155;;;60.0202;-23.5968;0;Bottom;Copper Fill96
156;;;12.3444;-43.4596;0;Bottom;Copper Fill39
157;;THT;60.6103;-11.9675;0;Bottom;J36
158;;;107.569;-76.7971;0;Bottom;Copper Fill274
159;;;88.4428;-14.0972;0;Bottom;Copper Fill127
160;;;89.4588;-58.0011;0;Bottom;Copper Fill304
161;;;71.8122;-37.6959;0;Bottom;Via77
162;;;113.386;-64.6178;0;Bottom;Copper Fill16
163;;;5.6896;-20.5234;0;Bottom;Copper Fill100
164;;;9.0424;-27.7624;0;Bottom;Copper Fill83
165;;;132.182;-92.0244;0;Bottom;Copper Fill3
166;;;19.9517;-77.7242;0;Bottom;Copper Fill275
167;;;29.6672;-8.2298;0;Bottom;Copper Fill144
168;;;72.898;-35.23;0;Bottom;Copper Fill65
169;;;32.6644;-43.4596;0;Bottom;Copper Fill40
170;;;113.157;-74.4222;0;Bottom;Copper Fill7
171;;;134.201;-54.915;0;Bottom;Copper Fill340
172;;;28.4753;-27.1676;0;Bottom;Via95
173;;;33.218;-48.7023;0;Bottom;Via99
174;;;25.0317;-78.3338;0;Bottom;Copper Fill268
175;;;19.9517;-75.87;0;Bottom;Copper Fill276
176;;;108.962;-58.8885;0;Bottom;Via33
177;;;54.4361;-25.3015;0;Bottom;Via84
178;10k;THT;98.7103;-71.6575;-90;Bottom;R15
179;;;116.053;-22.9491;0;Bottom;Copper Fill331
180;;;31.2928;-26.2384;0;Bottom;Copper Fill211
181;;;99.1616;-15.4942;0;Bottom;Copper Fill121
182;;;55.5305;-61.4974;0;Bottom;Via56
183;;;26.8446;-22.9486;0;Bottom;Via92
184;;;65.1032;-45.562;0;Bottom;Via80
185;;;130.861;-69.1771;0;Bottom;Copper Fill301
186;;;22.4917;-77.7242;0;Bottom;Copper Fill258
187;;;33.1597;-49.6826;0;Bottom;Copper Fill235
188;;;94.2086;-51.3463;0;Bottom;Copper Fill298
189;;;44.7832;-45.562;0;Bottom;Via90
190;;;66.6496;-20.5234;0;Bottom;Copper Fill103
191;;;111.735;-22.403;0;Bottom;Copper Fill106
192;;;21.0693;-40.7926;0;Bottom;Copper Fill246
193;;;97.79;-3.9372;0;Bottom;Copper Fill156
194;;cap-pth-small-kit;110.013;-76.7375;0;Bottom;C5
195;;;52.578;-35.23;0;Bottom;Copper Fill64
196;;;144.577;-34.8236;0;Bottom;Copper Fill67
197;100 nF;0805 [SMD, multilayer];101.079;-77.7493;90;Bottom;C10
198;;;10.866;-35.6768;0;Bottom;Via106
199;;;78.613;-2.3116;0;Bottom;Copper Fill166
200;;;18.0594;-18.3644;0;Bottom;Copper Fill107
201;;;79.9846;-50.0128;0;Bottom;Copper Fill219
202;;;19.9136;-37.6938;0;Bottom;Copper Fill55
203;;THT;146.97;-65.3075;0;Bottom;I²C
204;;;125.857;-24.3842;0;Bottom;Copper Fill99
205;;;27.4828;-14.0972;0;Bottom;Copper Fill124
206;;;32.258;-35.23;0;Bottom;Copper Fill63
207;;;102.298;-40.7926;0;Bottom;Copper Fill312
208;;;7.8486;-36.6524;0;Bottom;Copper Fill205
209;;diode-1n4148;153.32;-58.9575;180;Bottom;D25
210;;THT;148.367;-31.0175;0;Bottom;ICSP
211;33;THT;67.2431;-50.6687;90;Bottom;R9
212;;;73.2921;-79.073;0;Bottom;IMG1
213;;;61.6585;-41.9102;0;Bottom;Copper Fill281
214;;;95.0468;-48.7555;0;Bottom;Copper Fill288
215;;;23.2845;-57.184;0;Bottom;TXT1
216;;THT;88.5503;-57.9669;0;Bottom;LED_Ring
217;;;96.0374;-51.3463;0;Bottom;Copper Fill299
218;;1206 [SMD];31.6804;-91.9775;0;Bottom;LED1
219;;;73.2282;-31.9915;0;Bottom;Copper Fill292
220;100 nF;0805 [SMD, multilayer];105.994;-74.3193;90;Bottom;C12
221;;diode-1n4148;133;-56.4175;180;Bottom;D27
222;;;81.1784;-40.5386;0;Bottom;Copper Fill45
223;;;72.8726;-48.7555;0;Bottom;Copper Fill271
224;;;144.43;-74.1976;0;Bottom;Via30
225;;;19.3802;-23.5968;0;Bottom;Copper Fill94
226;;;15.7734;-28.5752;0;Bottom;Copper Fill84
227;;;6.096;-63.9828;0;Bottom;Copper Fill15
228;;;58.1406;-12.751;0;Bottom;Copper Fill135
229;;;114.021;-22.9491;0;Bottom;Copper Fill330
230;;;45.3009;-83.109;0;Bottom;Copper Fill225
231;;;75.8698;-32.055;0;Bottom;Copper Fill75
232;;;114.097;-37.135;0;Bottom;Copper Fill60
233;;;113.881;-42.1642;0;Bottom;Copper Fill322
234;;;81.7372;-3.6832;0;Bottom;Copper Fill151
235;;;10.8522;-37.6959;0;Bottom;Via107
236;;;101.498;-40.5386;0;Bottom;Copper Fill46
237;;;52.9844;-43.5612;0;Bottom;Copper Fill37
238;;;8.15528;-27.1676;0;Bottom;Via105
239;;;102.298;-41.9102;0;Bottom;Copper Fill311
240;;;93.6244;-14.0972;0;Bottom;Copper Fill161
241;;;144.43;-56.4176;0;Bottom;Via35
242;;;38.6294;-22.8834;0;Bottom;Via93
243;;;26.0096;-20.5234;0;Bottom;Copper Fill101
244;;;123.11;-25.299;0;Bottom;Via62
245;;;70.4596;-56.4771;0;Bottom;Copper Fill256
246;;;135.179;-53.9371;0;Bottom;Copper Fill341
247;;;82.0509;-41.2928;0;Bottom;Via81
248;;;150.393;-18.3771;0;Bottom;Copper Fill203
249;;;93.1926;-48.7555;0;Bottom;Copper Fill287
250;;;17.653;-2.3116;0;Bottom;Copper Fill163
251;;;91.0902;-64.0376;0;Bottom;Via25
252;;;41.3893;-41.9102;0;Bottom;Copper Fill264
253;;;117.881;-45.5678;0;Bottom;Copper Fill34
254;;;113.995;-29.9214;0;Bottom;Copper Fill69
255;;THT;118.945;-76.7383;180;Bottom;XTAL1
256;;;60.8584;-40.5386;0;Bottom;Copper Fill44
257;;;146.901;-60.6046;0;Bottom;Copper Fill342
258;;;81.9785;-41.9102;0;Bottom;Copper Fill294
259;;;74.7268;-48.7555;0;Bottom;Copper Fill272
260;10k;0805 [SMD];65.1165;-36.6797;90;Top;R17
261;;;78.4606;-12.751;0;Bottom;Copper Fill136
262;;;57.2516;-39.9671;0;Bottom;Copper Fill196
263;;;9.3472;-8.2298;0;Bottom;Copper Fill143
264;;;73.7616;-28.9562;0;Bottom;Copper Fill80
265;;;145.872;-13.2336;0;Bottom;Copper Fill116
266;;;41.0972;-3.6832;0;Bottom;Copper Fill149
267;;;94.1197;-31.42;0;Bottom;Copper Fill308
268;;;21.6495;-60.6494;0;Bottom;TXT1
269;;THT;78.3903;-53.8775;-90;Bottom;IR
270;;;126.581;-30.15;0;Bottom;Copper Fill326
271;;;17.5006;-12.751;0;Bottom;Copper Fill133
272;;;80.3402;-23.5968;0;Bottom;Copper Fill97
273;;;51.506;-35.6768;0;Bottom;Via86
274;;;49.6824;-27.7624;0;Bottom;Copper Fill87
275;;cap-pth-5mm;129.23;-69.1175;0;Bottom;C13
276;;;112.547;-59.7283;0;Bottom;Copper Fill200
277;;;134.201;-6.4772;0;Bottom;Copper Fill348
278;;;12.2682;-74.2063;0;Bottom;Copper Fill307
279;;;71.2597;-57.2772;0;Bottom;Copper Fill255
280;;THT;131.73;-86.8975;90;Bottom;LAN3
281;;;119.05;-59.6902;0;Bottom;Copper Fill18
282;;;95.1357;-52.2734;0;Bottom;Copper Fill297
283;;tactile-pth-12mm;31.3003;-81.8575;0;Bottom;S1
284;;;118.554;-15.4688;0;Bottom;Copper Fill335
285;;;110.287;-57.9376;0;Bottom;Copper Fill25
286;;;8.4836;-10.0586;0;Bottom;Copper Fill138
287;;;3.429;-24.0286;0;Bottom;Copper Fill35
288;;;108.801;-30.15;0;Bottom;Copper Fill324
289;;;3.1496;-81.5342;0;Bottom;Copper Fill4
290;;;22.4917;-77.7242;0;Bottom;Copper Fill179
291;;;124.46;-25.6796;0;Bottom;Copper Fill32
292;;;78.3209;-54.915;0;Bottom;Copper Fill289
293;;;114.071;-43.8914;0;Bottom;Copper Fill36
294;;;92.1322;-37.6959;0;Bottom;Via67
295;;;100.776;-48.7474;0;Bottom;Via113
296;;;108.87;-31.0174;0;Bottom;Via48
297;;;144.577;-35.865;0;Bottom;Copper Fill54
298;100 nF;0805 [SMD, multilayer];103.859;-74.3225;90;Bottom;C11
299;;;57.9672;-92.747;0;Bottom;Hole2
300;;;31.186;-35.6768;0;Bottom;Via96
301;;;73.7997;-31.42;0;Bottom;Copper Fill194
302;;;122.851;-15.0163;0;Bottom;Hole3
303;;;133.401;-5.6771;0;Bottom;Copper Fill349
304;;;16.637;-39.9671;0;Bottom;Copper Fill188
305;;THT;40.2903;-11.9675;0;Bottom;J38
306;;;74.7561;-25.3015;0;Bottom;Via74
307;;;127.375;-16.3503;0;Bottom;Via115
308;;;38.2016;-15.4942;0;Bottom;Copper Fill118
309;;;150.565;-36.0581;0;Bottom;Via45
310;;;109.093;-61.6206;0;Bottom;Copper Fill21
311;;;69.1153;-27.1676;0;Bottom;Via75
312;;;45.3702;-64.0376;0;Bottom;Via28
313;;;143.84;-29.7944;0;Bottom;Copper Fill82
314;100;THT;105.681;-56.4175;180;Bottom;R10
315;;1206 [SMD];11.3604;-91.9775;0;Bottom;LED2
316;;;70.0024;-27.7624;0;Bottom;Copper Fill89
317;;;22.4917;-75.87;0;Bottom;Copper Fill259
318;;;56.4134;-28.5752;0;Bottom;Copper Fill88
319;;;60.5536;-37.6938;0;Bottom;Copper Fill57
320;;;116.421;-82.8042;0;Bottom;Copper Fill251
321;;;94.6912;-31.9915;0;Bottom;Copper Fill310
322;;;141.89;-28.4776;0;Bottom;Via39
323;;;58.5216;-15.4942;0;Bottom;Copper Fill119
324;;;79.2694;-22.8834;0;Bottom;Via73
325;;;139.35;-33.5576;0;Bottom;Via37
326;;;85.4232;-45.562;0;Bottom;Via71
327;;;18.9992;-76.7971;0;Bottom;Copper Fill277
328;;so-24dw;73.3103;-36.7579;-90;Bottom;U8
329;;;145.796;-11.9636;0;Bottom;Copper Fill115
330;;;55.5498;-32.055;0;Bottom;Copper Fill74
331;;;140.589;-68.1992;0;Bottom;Copper Fill6
332;;THT;65.6903;-11.9675;0;Bottom;J33
333;100µF;300 mil [THT, electrolytic];103.79;-85.6275;-90;Bottom;C8
334;680;THT;19.9703;-81.8175;-90;Bottom;R12
335;;;119.482;-16.3959;0;Bottom;Copper Fill202
336;;;115.494;-81.8771;0;Bottom;Copper Fill253
337;;;117.348;-81.8771;0;Bottom;Copper Fill254
338;;;6.096;-66.4212;0;Bottom;Copper Fill13
339;;;37.973;-2.3116;0;Bottom;Copper Fill164
340;;;89.4334;-12.37;0;Bottom;Copper Fill132
341;;;46.3296;-20.5234;0;Bottom;Copper Fill102
342;;power_jack_pth;150.832;-88.4062;-90;Bottom;PWR
343;;;39.7002;-23.5968;0;Bottom;Copper Fill95
344;;;4.7117;-78.3338;0;Bottom;Copper Fill317
345;;;106.33;-16.3678;0;Bottom;Via42
346;;;139.281;-88.57;0;Bottom;Copper Fill314
347;;;120.294;-17.1706;0;Bottom;Copper Fill113
348;;;61.7309;-41.2928;0;Bottom;Via91
349;;;64.389;-69.9518;0;Bottom;Copper Fill29
350;;;33.2105;-31.42;0;Bottom;Copper Fill183
351;;;7.2517;-49.7842;0;Bottom;Copper Fill228
352;;THT;80.9303;-11.9675;0;Bottom;J34
353;100µF;200 mil [THT, electrolytic];7.27033;-51.3375;-90;Bottom;C1
354;;THT;140.993;-8.13411;-90;Bottom;JMP0
355;;;97.0534;-28.5752;0;Bottom;Copper Fill92
356;;;146.97;-36.0976;0;Bottom;Via47
357;;;98.933;-2.3116;0;Bottom;Copper Fill167
358;;;78.3209;-52.9846;0;Bottom;Copper Fill290
359;;;78.8416;-15.4942;0;Bottom;Copper Fill120
360;;so-24dw;52.9903;-36.7579;-90;Bottom;U9
361;;;76.7334;-28.5752;0;Bottom;Copper Fill90
362;;;117.399;-41.2371;0;Bottom;Copper Fill328
363;;;51.4922;-37.6959;0;Bottom;Via87
364;;;102.718;-81.8771;0;Bottom;Copper Fill240
365;;;12.8905;-31.42;0;Bottom;Copper Fill242
366;;;152.05;-20.8576;0;Bottom;Via44
367;;;49.9872;-8.2298;0;Bottom;Copper Fill145
368;;;73.2731;-79.0535;0;Bottom;blkr1
369;;;82.0928;-26.2384;0;Bottom;Copper Fill216
370;;;45.3009;-85.7506;0;Bottom;Copper Fill168
371;;;93.218;-35.23;0;Bottom;Copper Fill66
372;;;47.1646;-22.9486;0;Bottom;Via82
373;;;21.6154;-41.3387;0;Bottom;Copper Fill247
374;;;71.9328;-26.2384;0;Bottom;Copper Fill215
375;;;48.7934;-12.37;0;Bottom;Copper Fill130
376;;THT;116.49;-39.9075;0;Bottom;LAN2
377;;;114.071;-16.3959;0;Bottom;Copper Fill334
378;;;116.421;-82.8042;0;Bottom;Copper Fill176
379;;;102.371;-41.2928;0;Bottom;Via70
380;;;126.39;-16.3959;0;Bottom;Copper Fill204
381;;;28.4734;-12.37;0;Bottom;Copper Fill129
382;;;151.689;-73.9142;0;Bottom;Copper Fill9
383;;;132.613;-16.3959;0;Bottom;Copper Fill347
384;;;61.7728;-26.2384;0;Bottom;Copper Fill214
385;;;12.898;-48.7023;0;Bottom;Via109
386;;;73.858;-48.7023;0;Bottom;Via79
387;;;147.879;-61.5571;0;Bottom;Copper Fill344
388;;cpol-radial-10uf-25v;135.54;-5.61747;0;Bottom;C6
389;;;96.0374;-78.8799;0;Bottom;Copper Fill174
390;;;102.26;-75.0318;0;Bottom;Copper Fill8
391;;;96.1898;-32.055;0;Bottom;Copper Fill76
392;;;14.9098;-32.055;0;Bottom;Copper Fill72
393;;;139.35;-20.8576;0;Bottom;Via38
394;;;32.2326;-48.7555;0;Bottom;Copper Fill236
395;;;89.7636;-10.0586;0;Bottom;Copper Fill142
396;;;130.099;-76.7971;0;Bottom;Copper Fill187
397;;THT;45.3703;-11.9675;0;Bottom;J35
398;;;45.3702;-84.3575;0;Bottom;Via27
399;;;54.0766;-31.9915;0;Bottom;Copper Fill280
400;;;48.7953;-27.1676;0;Bottom;Via85
401;;;36.9316;-39.9671;0;Bottom;Copper Fill190
402;;;121.488;-77.1146;0;Bottom;Copper Fill11
403;;;100.66;-23.5968;0;Bottom;Copper Fill98
404;;;6.5246;-22.9486;0;Bottom;Via102
405;;;145.136;-65.3544;0;Bottom;Copper Fill14
406;;;106.821;-60.4768;0;Bottom;Via54
407;150;THT;102.054;-62.7551;180;Bottom;R14
408;;;12.3444;-39.5988;0;Bottom;Copper Fill47
409;;;127.292;-15.4942;0;Bottom;Copper Fill338
410;;;11.3157;-75.1842;0;Bottom;Copper Fill305
411;;;104.75;-81.8771;0;Bottom;Copper Fill241
412;;;130.861;-69.1771;0;Bottom;Copper Fill192
413;;;52.9844;-39.5988;0;Bottom;Copper Fill49
414;;;87.5284;-58.0011;0;Bottom;Copper Fill303
415;;;68.1228;-14.0972;0;Bottom;Copper Fill126
416;;;51.6128;-26.2384;0;Bottom;Copper Fill213
417;;;33.2105;-31.42;0;Bottom;Copper Fill261
418;;;92.146;-35.6768;0;Bottom;Via66
419;;;48.4886;-36.6524;0;Bottom;Copper Fill207
420;;;79.0194;-18.3644;0;Bottom;Copper Fill110
421;;tactile-pth-12mm;10.9803;-81.8575;0;Bottom;S2
422;;THT;120.182;-22.8749;-90;Bottom;DSP2
423;;;129.159;-53.264;0;Bottom;Copper Fill26
424;;;67.4846;-22.9486;0;Bottom;Via72
425;;;94.1197;-31.42;0;Bottom;Copper Fill197
426;;;17.8816;-15.4942;0;Bottom;Copper Fill117
427;;;73.3044;-43.485;0;Bottom;Copper Fill38
428;3A;d2pack_a;117.41;-8.15756;-90;Bottom;U7
429;;;89.1286;-36.6524;0;Bottom;Copper Fill209
430;;;61.4172;-3.6832;0;Bottom;Copper Fill150
431;;;99.5894;-22.8834;0;Bottom;Via61
432;;;36.0934;-28.5752;0;Bottom;Copper Fill86
433;;;52.9844;-14.0972;0;Bottom;Copper Fill159
434;;THT;11.3721;-69.0686;0;Bottom;Switch
435;;;80.8736;-37.6938;0;Bottom;Copper Fill58
436;;;82.55;-41.3387;0;Bottom;Copper Fill296
437;;;107.874;-31.0771;0;Bottom;Copper Fill325
438;;;12.8397;-49.6826;0;Bottom;Copper Fill171
439;;;32.6644;-39.5988;0;Bottom;Copper Fill48
440;;;141.89;-43.7176;0;Bottom;Via40
441;;;4.14317;-45.562;0;Bottom;Via110
442;;;109.804;-67.8182;0;Bottom;Copper Fill12
443;;;150.393;-18.3771;0;Bottom;Copper Fill339
444;;;89.4588;-58.0011;0;Bottom;Copper Fill193
445;;;3.6322;-79.3879;0;Bottom;Copper Fill318
446;;;131.877;-41.3768;0;Bottom;Copper Fill41
447;;;13.4366;-31.9915;0;Bottom;Copper Fill244
448;;;6.2992;-48.8571;0;Bottom;Copper Fill230
449;;;24.4632;-45.562;0;Bottom;Via100
450;;;125.527;-16.9166;0;Bottom;Copper Fill114
451;;;40.5384;-40.5386;0;Bottom;Copper Fill43
452;;;7.2517;-49.7842;0;Bottom;Copper Fill169
453;;THT;133;-48.7975;0;Bottom;LAN
454;;;20.7772;-3.6832;0;Bottom;Copper Fill148
455;;;77.5716;-39.9671;0;Bottom;Copper Fill198
456;;;86.9696;-20.5234;0;Bottom;Copper Fill104
457;;;130.404;-63.805;0;Bottom;Copper Fill17
458;;;92.3544;-84.4171;0;Bottom;Copper Fill223
459;;;27.7273;-69.4435;0;Bottom;TXT1
460;;;118.623;-16.3181;0;Bottom;Via114
461;;;73.3044;-39.5988;0;Bottom;Copper Fill50
462;;;43.9928;-84.4171;0;Bottom;Copper Fill226
463;;;93.6244;-39.5988;0;Bottom;Copper Fill51
464;;;115.951;-49.0984;0;Bottom;Copper Fill30
465;;cap-pth-large;141.89;-21.2385;-90;Bottom;C3
466;;;112.497;-53.9498;0;Bottom;Copper Fill28
467;;;94.0816;-28.9562;0;Bottom;Copper Fill81
468;;;117.196;-16.2054;0;Bottom;Copper Fill112
469;;;62.23;-41.3387;0;Bottom;Copper Fill283
470;;;107.34;-58.3694;0;Bottom;Copper Fill22
471;;;47.8028;-14.0972;0;Bottom;Copper Fill125
472;;;13.7668;-48.7555;0;Bottom;Copper Fill234
473;;;58.293;-2.3116;0;Bottom;Copper Fill165
474;;;134.798;-26.8988;0;Bottom;Copper Fill71
475;;;25.0317;-80.4674;0;Bottom;Copper Fill267
476;;;12.8397;-49.6826;0;Bottom;Copper Fill232
477;;;33.1216;-28.9562;0;Bottom;Copper Fill78
478;;;26.0604;-79.3879;0;Bottom;Copper Fill182
479;;;12.9161;-31.9363;0;Bottom;Via108
480;;THT;129.19;-94.5175;90;Bottom;Fuse 3.15A_T
481;;;130.099;-76.7971;0;Bottom;Copper Fill286
482;;;21.0909;-41.2928;0;Bottom;Via111
483;;;23.9522;-79.3879;0;Bottom;Copper Fill181
484;;;12.8016;-28.9562;0;Bottom;Copper Fill77
485;10k;0805 [SMD];4.15653;-36.6797;90;Top;R20
486;;;145.085;-6.1216;0;Bottom;Copper Fill162
487;;;131.712;-17.323;0;Bottom;Copper Fill345
488;;;53.4797;-49.6826;0;Bottom;Copper Fill175
489;;cpol-radial-10uf-25v;71.3117;-55.1475;90;Bottom;C5
490;;;22.4917;-75.87;0;Bottom;Copper Fill180
491;;;110.084;-63.1192;0;Bottom;Copper Fill19
492;;;53.5305;-31.42;0;Bottom;Copper Fill278
493;;;53.5305;-31.42;0;Bottom;Copper Fill189
494;;;121.379;-27.0739;0;Bottom;Via65
495;;;74.3712;-31.9915;0;Bottom;Copper Fill293
496;;;18.3094;-22.8834;0;Bottom;Via103
497;;;143.993;-32.309;0;Bottom;Copper Fill70
498;;;114.998;-17.323;0;Bottom;Copper Fill332
499;;;53.538;-48.7023;0;Bottom;Via89
500;;;23.3934;-76.7971;0;Bottom;Copper Fill260
501;;;92.6084;-62.7382;0;Bottom;Copper Fill20
502;;;103.721;-80.8738;0;Bottom;Copper Fill239
503;;;5.7404;-79.3879;0;Bottom;Copper Fill319
504;100µF;200 mil [THT, electrolytic];112.51;-16.3547;180;Bottom;C7
505;;;116.421;-80.95;0;Bottom;Copper Fill252
506;;;131.661;-68.377;0;Bottom;Copper Fill191
507;;;45.3009;-85.7506;0;Bottom;Copper Fill224
508;;;139.281;-85.3442;0;Bottom;Copper Fill315
509;;;58.6994;-18.3644;0;Bottom;Copper Fill109
510;10k;0805 [SMD];24.4765;-36.6797;90;Top;R19
511;;;28.1686;-36.6524;0;Bottom;Copper Fill206
512;680;THT;22.5103;-81.8175;-90;Bottom;R13
513;;;27.5902;-91.9776;0;Bottom;Via116
514;;;36.83;-3.9372;0;Bottom;Copper Fill153
515;;;131.73;-71.6576;0;Bottom;Via31
516;;;35.2298;-32.055;0;Bottom;Copper Fill73
517;;;138.76;-40.4878;0;Bottom;Copper Fill122
