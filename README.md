# mirolo - 2M05081R16

Software and Documentation for mirolo info display 2M05081R16

## Motivation

This project is intended to provide software and hardware documentation for building
small scale digital displays for digital signage and visitor information at conventions
and the like. The displays are very versatile and offer a simple to use web interface
in addition to a more powerful REST/JSON API for remote management via internal LAN networks.

They can also be operated in standalone mode as simple message or event schedule displays
by activating the corresponding modules. This is useful when there is no network available.

## Features
* 2 Lines of dot matrix modules, 1 RGB Ring indicator
* http web interface for simple fallback management
* REST/JSON API for advanced remote management
* automatic brightness control
* IR remote control
* I²C interface connector for external modules (e.g. DS1307 RTC)
* wide range power input: 10-20VAC/10-30VDC
* message board and event schedule modules for network independent manual operation

## License
This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

The hardware schematics are distributed as open hardware under a creative commons license [by-sa 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

## Setup

Install VScode and the PlatformIO extension. PlatformIO will take care of installing libraries and HW platforms.

## Legacy Setup

### Software
* [Arduino IDE](https://www.arduino.cc/en/Main/Software)

### Hardware Libraries
* [MightyCore](https://github.com/MCUdude/MightyCore)

### A Ton of Arduino Libraries
* [UIPEthernet](https://github.com/ntruchsess/arduino_uip)
* [Base64](https://github.com/Densaugeo/base64_arduino)
* [ArduinoJSON](https://github.com/bblanchon/ArduinoJson)
* [IRremote](https://github.com/z3t0/Arduino-IRremote)
* [MD\_MAX72xx](https://github.com/MajicDesigns/MD_MAX72XX)
* [DS1307RTC](https://github.com/PaulStoffregen/DS1307RTC)
* [Time](https://github.com/PaulStoffregen/Time)
* [AdaFruit\_NeoPixel](https://github.com/adafruit/Adafruit_NeoPixel)


### Additional necessary tweaks
To be able to check the ethernet controller for hang states you have to make one slight alteration to the source code
of the UIPEthernet library. Otherwise the code won't compile because the method isn't declared public by default.

Change **utility/Enc28J60Network.h** and move **readReg()** from private to public area.

I copied the code from [ntruchsess/arduino_uip/issues/167](https://github.com/ntruchsess/arduino_uip/issues/167) and didn't have any more problems
with the enc28j60 being unresponsive ever since.

You also have to change one line of the AdaFruit\_NeoPixel library so it supports the CPU running at 20MHz. Change **AdaFruit\_NeoPixel.cpp** and edit the following line

		// 16 MHz(ish) AVR --------------------------------------------------------
		#elif (F_CPU >= 15400000UL) && (F_CPU <= 19000000L)
		
to support a 20MHz Crystal

		// 16 MHz(ish) AVR --------------------------------------------------------
		#elif (F_CPU >= 15400000UL) && (F_CPU <= 20000000L)

### Compiling and upload
Make sure to change settings like MAC and IP addresses or calibration parameters in

		mirolo_2M05081R16_firmware.ino
		global.h
	
to your needs. A quick note about flashing the bootloader via ICSP on the Atmega1284 for the first time:
The IDE will report an error while flashing. This seems to be caused by some fueses not being set properly
the first time. To get rid of the mismatch error burn the bootloader once, ignore the error and completely disconnect
the microcontroller from the power supply. Then reconnect it and burn the bootloader again. It should work now without
errors. After that you can upload your sketch as usual.

Set **LOAD_EEPROM** to **0** when uploading for the first time. Otherwise it will load random values from the EEPROM and 
possibly inhibit proper startup. Please note that this will result in the IP address being set to whatever is specified in **static_ip** with your currently set serial number.

If the upload worked correctly the LED's on the back should light up.

Open the serial monitor (115200 baud) and issue a **system:reboot** command. This will save all values to the EEPROM and overwrite any random values.
Upload the sketch again after that with **LOAD_EEPROM** set to **1**.

## Usage
When started the serial console should output something like this

		mirolo 2M05081R16 led info panel
		Simon Junga | theolyn@is-a-furry.org
		_____________________________________
		Firmware Compile Date: 
		Mar  3 2019 | 15:13:58
		Device Serial Number:
		2019.06.0006
		_____________________________________
		Initializing Ethernet...
		IP-Address: 192.168.130.152
		OK

		System Boot: External Reset
		Boot SUCCESSFULL
		Type "help" for a list of commands
		(Terminate serial commands with \n)
		_____________________________________
		
Use a webbrowser to navigate to the displayed IP Address (The last two blocks of the address will also be shown on the display).
The following webpage should be displayed. Use your defined **HTTP_PASSWORD** and any user name to log in (The user name is only needed to show who is
using the display at the moment).
After logging in you can make changes to the text lines and color wheel. This should be pretty self explanatory.

![mirolo](screenshot_web.png?raw=true "Web Interface")
![mirolo](mirolo_display.jpg?raw=true "mirolo 2M05081R16")

You can change certain settings using serial commands. Send "help" followed by \n to get a list of commands.

The buttons on the back are also used to enable or disable access to the WEB Interface (S1) and the REST API (S2). Long pressing S1 enters display test mode
for visually checking whether all LEDs are operational (press any button to stop). Long pressing both buttons will reboot the display.

For information on how to control the display using the REST API have a look at [/documentation/REST API](../Documentation/REST API). 
There is also a ready made postman collection for testing [www.getpostman.com](https://www.getpostman.com/) as well as a
node.js based server for simulating an API endpoint without having a physical display [/Source/mirolo_2M05081R16_simulator](../Source/mirolo_2M05081R16_simulator).

The IR remote currently only works with the static modules because it isn't really needed in network mode. You can enable compiling these by setting

		LOAD_MODULE 0 //0 none, 1 schedule, 2 messageboard
		
Have a look at **module_schedule.h** and **module_messageboard.h** for configuring your own static messages and timetable. 
		
A remote control cheat sheet for each module can be found in [/Documentation/IR Remote Control](../Documentation/IR Remote Control). These modules are usefull if you
want to use the displays without network connection. Both modules require a DS1307 RTC module to be connected to the I²C bus for time display and management.

The remote control I use is a universal small and cheap remote control for [Android TVs](https://www.aliexpress.com/item/Universal-IR-Remote-Control-For-Android-TV-Box-H96-pro-V88-MXQ-Z28-T95X-T95Z-Plus/32842864477.html?ws_ab_test=searchweb0_0,searchweb201602_5_10065_10068_10547_319_317_10548_10696_10084_453_10083_454_10618_10304_10307_10820_10821_537_10302_536_10843_10059_10884_10887_321_322_10103,searchweb201603_52,ppcSwitch_0&algo_expid=a38941c2-b9f8-4619-a317-2d0655edec9e-7&algo_pvid=a38941c2-b9f8-4619-a317-2d0655edec9e&transAbTest=ae803_3 "aliexpress link")

However by changing the remote control codes in remote.h you can easily use any other remote.


## Hardware Build Instructions
You can find instructions on how to build one of these displays at [instructables.com](https://www.instructables.com/id/Mirolo-Networked-LED-Matrix-Display-for-Digital-Si/). All schematics
and pinout tables as well as PCB layouts are available in [/Documentation/Schematics](../Documentation/Schematics). For displaying
the PCB project files you need to install fritzing [fritzing.org](http://fritzing.org/home/). All diagrams are available in PDF format
but if you'd like to edit the files please install Qelectrotech as well [qelectrotech.org](https://qelectrotech.org/).



## Change log
**Version 1.0 (01-05-2019)**
* first public version


